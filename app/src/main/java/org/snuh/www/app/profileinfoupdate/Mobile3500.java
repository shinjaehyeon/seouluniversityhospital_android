package org.snuh.www.app.profileinfoupdate;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.kakao.auth.KakaoAdapter;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomSelectDialog;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.SUHDialogType2;
import org.snuh.www.app.common.SUHDialogType3;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.login.Mobile2000;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile3500 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    CustomSelectDialog customSelectDialog;

    /**************************************************************/

    int currentSelectGender;
    String[] genderStringArray = {"남자", "여자"};
    int[] genderIntegerArray = {800501, 800502};

    int currentSelectMobileCompanyNumber; // 현재 선택된 통신사 번호
    String[] mobileCompany = {"LG U+", "SKT", "KT", "LG U+ 알뜰폰", "SKT 알뜰폰", "KT 알뜰폰"};
    int mobileCompanyCode[] = {700101, 700102, 700103, 700104, 700105, 700106};

    int currentSelectBloodTypeNumber; // 현재 선택된 혈액형 번호
    String[] bloodType = {"O 형", "A 형", "B 형", "AB 형"};
    int bloodTypeCode[] = {600101, 600102, 600103, 600104};

    /**************************************************************/

    EditText name;
    // EditText gender;
    Button genderSelectButton;
    TextView genderDisplayText;
    EditText signUpMethod;
    Button theMobileCompanySelectButton;
    TextView mobileCompanyDisplayText;
    EditText phoneNumber;
    EditText email;
    EditText password;
    EditText passwordCheck;
    EditText birthdayYear;
    EditText birthdayMonth;
    EditText birthdayDate;
    Button theBloodTypeSelectButton;
    TextView theBloodTypeDisplayText;
    EditText addr;

    /**************************************************************/

    // 불러온 프로필 정보 (원래 값)
    String geted_name;
    int geted_gender;
    int geted_mobile_company;
    String geted_phone_number;

    String geted_password = "";

    int geted_birthday_year;
    int geted_birthday_month;
    int geted_birthday_date;
    int geted_blood_type;
    String geted_addr;


    // 업데이트 보낼 정보 (원래값 or 수정된 값)
    int checked_gender;
    int checked_mobile_company;
    String checked_phone_number;
    String checked_password;
    String checked_birthday;
    int checked_blood_type;
    String checked_addr;

    /**************************************************************/

    Button updateButton;
    Button logout_button;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 3502:
                    mobile3502(msg.obj); // 서버에 프로필 정보 요청 후처리
                    break;
                case 3522:
                    mobile3522(msg.obj); // 서버에 프로필 정보 업데이트 요청 후처리
                    break;
                case 3524: // 서버에 푸시키 삭제 요청하기 후처리
                    mobile3524(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile3500);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기



        mobile3501(); // 서버에 프로필 정보 요청
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }













    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);

        customSelectDialog = new CustomSelectDialog(activity);

        name = (EditText) findViewById(R.id.name);
        // gender = (EditText) findViewById(R.id.gender);
        genderSelectButton = (Button) findViewById(R.id.genderSelectButton);
        genderDisplayText = (TextView) findViewById(R.id.genderDisplayText);
        signUpMethod = (EditText) findViewById(R.id.signUpMethod);
        theMobileCompanySelectButton = (Button) findViewById(R.id.theMobileCompanySelectButton);
        mobileCompanyDisplayText = (TextView) findViewById(R.id.mobileCompanyDisplayText);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        password = (EditText) findViewById(R.id.password);
        passwordCheck = (EditText) findViewById(R.id.passwordCheck);
        email = (EditText) findViewById(R.id.email);
        birthdayYear = (EditText) findViewById(R.id.birthdayYear);
        birthdayMonth = (EditText) findViewById(R.id.birthdayMonth);
        birthdayDate = (EditText) findViewById(R.id.birthdayDate);
        theBloodTypeSelectButton = (Button) findViewById(R.id.theBloodTypeSelectButton);
        theBloodTypeDisplayText = (TextView) findViewById(R.id.theBloodTypeDisplayText);
        addr = (EditText) findViewById(R.id.addr);



        updateButton = (Button) findViewById(R.id.updateButton);
        logout_button = (Button) findViewById(R.id.logout_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        genderSelectButton.setOnClickListener(genderButtonClickEvent);


        theMobileCompanySelectButton.setOnClickListener(theMobileCompanySelectButtonClickEvent);
        theBloodTypeSelectButton.setOnClickListener(theBloodTypeSelectButtonClickEvent);


        updateButton.setOnClickListener(updateButtonClickEvent);
        logout_button.setOnClickListener(logoutButtonClickEvent);

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // customSelectDialog.setFirstValue(2);
        customSelectDialog.setSelectionList(genderStringArray, genderIntegerArray);
    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    /**************************************************************/

    View.OnClickListener genderButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCustomSelectDialog("성별을 선택해주세요.");
        }
    };


    View.OnClickListener theMobileCompanySelectButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callSUHDialogType2Dialog("통신사를 선택해주세요.");
        }
    };

    View.OnClickListener theBloodTypeSelectButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callSUHDialogType3Dialog("혈액형을 선택해주세요.");
        }
    };

    View.OnClickListener updateButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mobile3511()) { // 회원 정보 입력된 값이 수정된 부분이 있는지 체크하기

                if (mobile3512()) { // 회원 정보 입력된 값 유효성 체크하기

                    mobile3521(); // 서버에 프로필 정보 업데이트 요청

                }

            } else {
                callSUHDialogType1Dialog("안내", "수정된 내용이 없습니다.", false);
            }
        }
    };

    View.OnClickListener logoutButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callSUHDialogType1Dialog3("안내", "정말 로그아웃 하시겠습니까?");
        }
    };

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content, final boolean isFinish) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callCustomSelectDialog(String title) {
        customSelectDialog.show();
        customSelectDialog.setTitle(title);
        customSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customSelectDialog.dismiss();
            }
        });
        customSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (customSelectDialog.isOnItemClicked) {
                    int index = customSelectDialog.getCurrentIndex();
                    currentSelectGender = genderIntegerArray[index];

                    genderDisplayText.setText(genderStringArray[index]);
                    genderDisplayText.setTextColor(Color.parseColor("#888888"));

                    customSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    // 로그아웃 하기
    public void goLogout() {
        switch (new ShareInfo(activity).getMemberSingUpMethod()) {
            case 500802: // 페이스북 로그인일 경우
                LoginManager loginManager = LoginManager.getInstance();
                loginManager.logOut();
                new ShareInfo(activity).setInputIntValue("member", 0);
                new ShareInfo(activity).setInputIntValue("member_signupmethod", 0);
                new ShareInfo(activity).setInputIntValue("member_access_token", "");

                finishAffinity();
                Intent intent = new Intent(activity, Mobile2000.class);
                startActivity(intent);
                break;
            case 500803: // 카카오 로그인일 경우
                UserManagement.getInstance().requestLogout(new LogoutResponseCallback() {
                    @Override
                    public void onCompleteLogout() {
                        new ShareInfo(activity).setInputIntValue("member", 0);
                        new ShareInfo(activity).setInputIntValue("member_signupmethod", 0);
                        new ShareInfo(activity).setInputIntValue("member_access_token", "");

                        finishAffinity();
                        Intent intent = new Intent(activity, Mobile2000.class);
                        startActivity(intent);
                    }
                });

                break;
            case 500804: // 네이버 로그인일 경우
                new ShareInfo(activity).setInputIntValue("member", 0);
                new ShareInfo(activity).setInputIntValue("member_signupmethod", 0);
                new ShareInfo(activity).setInputIntValue("member_access_token", "");

                new RequestNaverLogout().execute();

                finishAffinity();
                Intent intent3 = new Intent(activity, Mobile2000.class);
                startActivity(intent3);
                break;
            default:
                new ShareInfo(activity).setInputIntValue("member", 0);
                new ShareInfo(activity).setInputIntValue("member_signupmethod", 0);
                new ShareInfo(activity).setInputIntValue("member_access_token", "");

                finishAffinity();
                Intent intent4 = new Intent(activity, Mobile2000.class);
                startActivity(intent4);
                break;
        }


    }

    private class RequestNaverLogout extends AsyncTask {
        RequestNaverLogout() {

        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // 로그아웃 처리가 완료되면 수행할 로직 작성



        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                final String apiURL = "https://nid.naver.com/oauth2.0/token";
                URL url = new URL(apiURL);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("client_id", "lsYfS68jHtTTeX1n6XOP");
                con.setRequestProperty("client_secret", "jXwQCR3sq2");
                con.setRequestProperty("access_token", new ShareInfo(activity).getMemberAccessToken());
                con.setRequestProperty("grant_type", "delete");
                int responseCode = con.getResponseCode();

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        responseCode == 200 ? con.getInputStream() : con.getErrorStream()));

                String inputLine;
                StringBuffer response = new StringBuffer();
                while((inputLine = br.readLine()) != null) {
                    response.append(inputLine);
                }

                br.close();
                return response;

            } catch(Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }




    public void callSUHDialogType1Dialog2(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.blindNegativeButton();
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
    }

    public void callSUHDialogType1Dialog3(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.blindNegativeButton();
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
                mobile3523(); // 서버에 푸시키 삭제 요청하기
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
    }


    // 통신사 선택 커스텀 다이얼로그
    public void callSUHDialogType2Dialog(String title) {
        final SUHDialogType2 suhDialogType2 = new SUHDialogType2(activity);
        suhDialogType2.show();
        suhDialogType2.setParentSelectedNumber(currentSelectMobileCompanyNumber);
        suhDialogType2.setTitle(title);
        suhDialogType2.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType2.dismiss();
            }
        });
        suhDialogType2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                currentSelectMobileCompanyNumber = suhDialogType2.getCurrentSelectedMobileCompanyNumber();
                Log.i("FinalTags", "currentSelectMobileCompanyNumber = "+currentSelectMobileCompanyNumber);
                if (currentSelectMobileCompanyNumber - 1 >= 0) {
                    mobileCompanyDisplayText.setText(mobileCompany[currentSelectMobileCompanyNumber - 1]);
                    mobileCompanyDisplayText.setTextColor(Color.parseColor("#888888"));
                }
            }
        });
    }

    // 혈액형 선택 커스텀 다이얼로그
    public void callSUHDialogType3Dialog(String title) {
        final SUHDialogType3 suhDialogType3 = new SUHDialogType3(activity);
        suhDialogType3.show();
        suhDialogType3.setParentSelectedNumber(currentSelectBloodTypeNumber);
        suhDialogType3.setTitle(title);
        suhDialogType3.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType3.dismiss();
            }
        });
        suhDialogType3.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                currentSelectBloodTypeNumber = suhDialogType3.getCurrentSelectedMobileCompanyNumber();
                if (currentSelectBloodTypeNumber - 1 >= 0) {
                    theBloodTypeDisplayText.setText(bloodType[currentSelectBloodTypeNumber - 1]);
                    theBloodTypeDisplayText.setTextColor(Color.parseColor("#888888"));
                }
            }
        });
    }

    /**************************************************************/

    // 서버에 프로필 정보 요청
    public void mobile3501() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3501");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3502);
        inLet.start();
    }
    // 서버에 프로필 정보 요청 후처리
    public void mobile3502(Object obj) {
        String data = obj.toString();
        Log.i("FinalTags", "mobile3502 data = "+data);

        UseFul useFul = new UseFul(activity);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                String names = useFul.textClean(jsonObject1.getString("name"));
                int gender_int = jsonObject1.getInt("gender");
                String gender_string = jsonObject1.getString("gender_string");
                int theMobileCompany = jsonObject1.getInt("theMobileCompany");
                String phoneNumbers = jsonObject1.getString("phoneNumber");
                String emails = useFul.textClean(jsonObject1.getString("email"));
                int birthday_year = jsonObject1.getInt("birthday_year");
                int birthday_month = jsonObject1.getInt("birthday_month");
                int birthday_date = jsonObject1.getInt("birthday_date");
                int theBloodType = jsonObject1.getInt("theBloodType");
                String addrs = useFul.textClean(jsonObject1.getString("addr"));
                int signUpMethods = jsonObject1.getInt("signUpMethod");


                // 불러온 값 저장 (값이 바뀌었는지 추후 값이랑 비교하기 위해)
                geted_name = names;
                geted_gender = gender_int;
                currentSelectGender = geted_gender;
                geted_mobile_company = theMobileCompany;
                geted_phone_number = phoneNumbers;

                geted_birthday_year = birthday_year;
                geted_birthday_month = birthday_month;
                geted_birthday_date = birthday_date;
                geted_blood_type = theBloodType;
                geted_addr = addrs;


                // 불러온 값 해당 부분에 보여주기

                // 이름 뿌리기
                name.setText(names);

                // 성별 뿌리기
                genderDisplayText.setText(gender_string);
                if (gender_int != 900900) {
                    genderDisplayText.setTextColor(Color.parseColor("#888888"));
                }
                if (gender_int == 800501) {
                    customSelectDialog.setFirstValue(0);
                } else if (gender_int == 800502) {
                    customSelectDialog.setFirstValue(1);
                }

                // 가입방법 뿌리기
                String signUpMethodString = "";
                switch (signUpMethods) {
                    case 500801:
                        signUpMethodString = "일반 회원가입";
                        break;
                    case 500802:
                        signUpMethodString = "페이스북 로그인";
                        password.setEnabled(false);
                        password.setText("");
                        passwordCheck.setEnabled(false);
                        password.setHint("SNS 계정은 비밀번호 편집 불가");
                        passwordCheck.setHint("SNS 계정은 비밀번호 편집 불가");
                        break;
                    case 500803:
                        signUpMethodString = "카카오 로그인";
                        password.setEnabled(false);
                        password.setText("");
                        passwordCheck.setEnabled(false);
                        password.setHint("SNS 계정은 비밀번호 편집 불가");
                        passwordCheck.setHint("SNS 계정은 비밀번호 편집 불가");

                        break;
                    case 500804:
                        signUpMethodString = "네이버 로그인";
                        password.setEnabled(false);
                        password.setText("");
                        passwordCheck.setEnabled(false);
                        password.setHint("SNS 계정은 비밀번호 편집 불가");
                        passwordCheck.setHint("SNS 계정은 비밀번호 편집 불가");

                        break;
                    default:

                        break;
                }
                signUpMethod.setText(signUpMethodString);

                // 통신사 뿌리기
                if (theMobileCompany < 700101 || theMobileCompany > 700106) {
                    currentSelectMobileCompanyNumber = 0;
                } else {
                    int index = 0;
                    for (int i=0; i<mobileCompanyCode.length; i++) {
                        if (theMobileCompany == mobileCompanyCode[i]) {
                            break;
                        }
                        index++;
                    }
                    mobileCompanyDisplayText.setText(mobileCompany[index]);
                    currentSelectMobileCompanyNumber = index+1;
                    mobileCompanyDisplayText.setTextColor(Color.parseColor("#888888"));
                }


                // 휴대폰 뿌리기
                phoneNumber.setText(phoneNumbers);

                // 이메일 뿌리기
                email.setText(emails);

                // 생년월일 뿌리기
                if (birthday_year == 0 || birthday_month == 0 || birthday_date == 0) {

                } else {
                    birthdayYear.setText(birthday_year+"");
                    birthdayMonth.setText(birthday_month+"");
                    birthdayDate.setText(birthday_date+"");
                }

                // 혈액형 뿌리기
                if (theBloodType < 600101 || theBloodType > 600104) {
                    currentSelectBloodTypeNumber = 0;
                } else {
                    int index = 0;
                    for (int i=0; i<bloodTypeCode.length; i++) {
                        if (theBloodType == bloodTypeCode[i]) {
                            break;
                        }
                        index++;
                    }
                    theBloodTypeDisplayText.setText(bloodType[index]);
                    currentSelectBloodTypeNumber = index+1;
                    theBloodTypeDisplayText.setTextColor(Color.parseColor("#888888"));
                }

                // 주소 뿌리기
                addr.setText(addrs);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 회원 정보 입력된 값이 수정된 부분이 있는지 체크하기
    public boolean mobile3511() {
        // 성별 값이 수정되었는지
        if (geted_gender != currentSelectGender) {
            return true;
        }

        // 통신사 값이 수정 되었는지
        switch (geted_mobile_company) {
            case 900900:
                if (currentSelectMobileCompanyNumber != 0) {
                    Log.i("FinalTags", "439 실행");
                    return true;
                }
                break;
            case 700101:
                if (currentSelectMobileCompanyNumber != 1) {
                    Log.i("FinalTags", "445 실행");
                    return true;
                }
                break;
            case 700102:
                if (currentSelectMobileCompanyNumber != 2) {
                    Log.i("FinalTags", "451 실행");
                    return true;
                }
                break;
            case 700103:
                if (currentSelectMobileCompanyNumber != 3) {
                    Log.i("FinalTags", "457 실행");
                    return true;
                }
                break;
            case 700104:
                if (currentSelectMobileCompanyNumber != 4) {
                    Log.i("FinalTags", "463 실행");
                    return true;
                }
                break;
            case 700105:
                if (currentSelectMobileCompanyNumber != 5) {
                    Log.i("FinalTags", "469 실행");
                    return true;
                }
                break;
            case 700106:
                if (currentSelectMobileCompanyNumber != 6) {
                    Log.i("FinalTags", "475 실행");
                    return true;
                }
                break;
            default:

                break;
        }

        // 휴대폰 번호가 수정 되었는지
        if (!geted_phone_number.equals(phoneNumber.getText().toString())) {
            Log.i("FinalTags", "486 실행");
            return true;
        }

        // 비밀번호가 수정 되었는지
        if (!geted_password.equals(password.getText().toString())) {
            Log.i("FinalTags", "492 실행");
            return true;
        }

        // 생년월일이 수정 되었는지
        if (geted_birthday_year == 0) {
            if (!birthdayYear.getText().toString().equals("")) {
                Log.i("FinalTags", "499 실행");
                return true;
            }
        } else {
            if (geted_birthday_year != Integer.parseInt(birthdayYear.getText().toString())) {
                return true;
            }
        }
        if (geted_birthday_month == 0) {
            if (!birthdayMonth.getText().toString().equals("")) {
                Log.i("FinalTags", "505 실행");
                return true;
            }
        } else {
            if (geted_birthday_month != Integer.parseInt(birthdayMonth.getText().toString())) {
                return true;
            }
        }
        if (geted_birthday_date == 0) {
            if (!birthdayDate.getText().toString().equals("")) {
                Log.i("FinalTags", "511 실행");
                return true;
            }
        } else {
            if (geted_birthday_date != Integer.parseInt(birthdayDate.getText().toString())) {
                return true;
            }
        }

        // 혈액형이 수정 되었는지
        switch (geted_blood_type) {
            case 900900:
                if (currentSelectBloodTypeNumber != 0) {
                    Log.i("FinalTags", "520 실행");
                    return true;
                }
                break;
            case 600101:
                if (currentSelectBloodTypeNumber != 1) {
                    Log.i("FinalTags", "526 실행");
                    return true;
                }
                break;
            case 600102:
                if (currentSelectBloodTypeNumber != 2) {
                    Log.i("FinalTags", "532 실행");
                    return true;
                }
                break;
            case 600103:
                if (currentSelectBloodTypeNumber != 3) {
                    Log.i("FinalTags", "538 실행");
                    return true;
                }
                break;
            case 600104:
                if (currentSelectBloodTypeNumber != 4) {
                    Log.i("FinalTags", "544 실행");
                    return true;
                }
                break;
            default:

                break;
        }


        // 주소가 수정 되었는지
        if (!geted_addr.equals(addr.getText().toString())) {
            Log.i("FinalTags", "556 실행");
            return true;
        }


        return false;
    }

    // 회원 정보 입력된 값 유효성 체크하기
    public boolean mobile3512() {
        UseFul useFul = new UseFul(activity);


        // 성별 체크하기
        if (currentSelectGender == 900900) {
            callSUHDialogType1Dialog("안내", "성별을 선택해주세요.", false);
            return false;
        }
        checked_gender = currentSelectGender;



        // 휴대폰번호 & 통신사 체크하기
        if (phoneNumber.getText().toString().equals("")) {
            callSUHDialogType1Dialog("안내", "휴대폰 번호를 입력 해주세요.", false);
            return false;
        } else {
            if (currentSelectMobileCompanyNumber == 0) {
                callSUHDialogType1Dialog("안내", "통신사를 선택 해주세요.", false);
                return false;
            }
        }
        checked_mobile_company = mobileCompanyCode[currentSelectMobileCompanyNumber-1];
        checked_phone_number = phoneNumber.getText().toString();


        // 비밀번호 체크
        if (password.getText().toString().equals("")) {

        } else {
            if (useFul.isExistEmptySpace(password.getText().toString())) {
                callSUHDialogType1Dialog("안내", "비밀번호에는 공백문자가 들어갈 수 없습니다.", false);
                return false;
            }

            if (password.getText().toString().length() < 6) {
                callSUHDialogType1Dialog("안내", "비밀번호는 6자 이상으로 설정 해주세요.", false);
                return false;
            }

            if (!password.getText().toString().equals(passwordCheck.getText().toString())) {
                callSUHDialogType1Dialog("안내", "비밀번호와 비밀번호확인이 일치하지 않습니다. 다시 확인 해주세요.", false);
                return false;
            }
        }
        checked_password = password.getText().toString();


        // 생년월일 체크
        if (birthdayYear.getText().toString().equals("") || birthdayMonth.getText().toString().equals("") || birthdayDate.getText().toString().equals("")) {
            callSUHDialogType1Dialog("안내", "생년월일을 모두 입력해주세요.", false);
            return false;
        }


        int birthday_year = Integer.parseInt(birthdayYear.getText().toString());
        int birthday_month = Integer.parseInt(birthdayMonth.getText().toString());
        int birthday_date = Integer.parseInt(birthdayDate.getText().toString());
        String birthday_month_str = ""+birthday_month;
        if (birthday_month < 10) {
            birthday_month_str = "0"+birthday_month;
        }
        String birthday_date_str = ""+birthday_date;
        if (birthday_date < 10) {
            birthday_date_str = "0"+birthday_date;
        }
        String fullDate = birthday_year+""+birthday_month_str+""+birthday_date_str;
        if (!useFul.dateCheck(fullDate, "yyyyMMdd")) {
            callSUHDialogType1Dialog("안내", "생년월일의 날짜가 유효하지 않습니다. 다시 확인 해주세요.", false);
            return false;
        }
        checked_birthday = birthday_year+""+birthday_month_str+""+birthday_date_str;



        // 혈액형 체크
        if (currentSelectBloodTypeNumber == 0) {
            callSUHDialogType1Dialog("안내", "혈액형을 선택 해주세요.", false);
            return false;
        }
        checked_blood_type = bloodTypeCode[currentSelectBloodTypeNumber-1];


        // 주소 체크
        checked_addr = addr.getText().toString();


        return true;
    }

    // 서버에 프로필 정보 업데이트 요청
    public void mobile3521() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3521");
        params.put("member", new ShareInfo(activity).getMember());

        params.put("checked_gender", checked_gender);
        params.put("checked_mobile_company", checked_mobile_company);
        params.put("checked_phone_number", checked_phone_number);
        params.put("checked_password", checked_password);
        params.put("checked_birthday", checked_birthday);
        params.put("checked_blood_type", checked_blood_type);
        params.put("checked_addr", checked_addr);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3522);
        inLet.start();
    }

    // 서버에 프로필 정보 업데이트 요청 후처리
    public void mobile3522(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                callSUHDialogType1Dialog2("안내", "프로필 정보가 업데이트 되었습니다.");
            } else {
                callSUHDialogType1Dialog2("안내", "프로필 정보를 업데이트 하는 도중 오류가 발생하였습니다. 다시 시도 해주세요.");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // 서버에 푸시키 삭제 요청하기
    public void mobile3523() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3523");
        params.put("member", new ShareInfo(activity).getMember());

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3524);
        inLet.start();
    }

    // 서버에 푸시키 삭제 요청하기 후처리
    public void mobile3524(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

                goLogout(); // 로그아웃 하기

            } else {

                callSUHDialogType1Dialog2("경고", "로그아웃 중에 문제가 발생하였습니다. 다시 시도해주세요. 이 팝업창이 지속적으로 표시될 경우 문의 바랍니다.");

            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }









    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                finish();
            }
        }
    }

    // 다른 화면으로부터 이 페이지에 다시 왔을 때
    @Override
    protected void onPostResume() {
        super.onPostResume();
        // 정보 갱신하기

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
