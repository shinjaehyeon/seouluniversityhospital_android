package org.snuh.www.app.hospitalnews;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class HospitalDetailDialog extends Dialog {
    Context context;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    TextView news_title;
    TextView news_datetime;
    TextView news_view_index;
    TextView news_content;


    /**************************************************************/

    public HospitalDetailDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.hospital_detail_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        news_title = (TextView) findViewById(R.id.news_title);
        news_datetime = (TextView) findViewById(R.id.news_datetime);
        news_view_index = (TextView) findViewById(R.id.news_view_index);
        news_content = (TextView) findViewById(R.id.news_content);

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }


    public void setNewsTitle(String s) {
        news_title.setText(s);
    }
    public void setNewsDateTime(String s) {
        news_datetime.setText(s);
    }
    public void setNewsViewIndex(int n) {
        news_view_index.setText("조회수 : "+n);
    }
    public void setNewsContent(String s) {
        news_content.setText(s);
    }
}
