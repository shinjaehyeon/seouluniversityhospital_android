package org.snuh.www.app.medicalcertificateitemset;

public class MedicalCertificateItem {
    private int primaryKey;
    private int passedDay;
    private int passedHour;
    private int passedMinute;
    private String dateString;
    private String timeString;
    private int department;
    private String departmentString;
    private int doctor;
    private String doctorName;
    private MedicalCertificateItemViewHolder viewHolder;

    public void setPrimaryKey(int n) {
        this.primaryKey = n;
    }
    public int getPrimaryKey() {
        return this.primaryKey;
    }

    public void setPassedDay(int n) {
        this.passedDay = n;
    }
    public int getPassedDay() {
        return this.passedDay;
    }

    public void setPassedHour(int n) {
        this.passedHour = n;
    }
    public int getPassedHour() {
        return this.passedHour;
    }

    public void setPassedMinute(int n) {
        this.passedMinute = n;
    }
    public int getPassedMinute() {
        return this.passedMinute;
    }

    public void setDateString(String s) {
        this.dateString = s;
    }
    public String getDateString() {
        return this.dateString;
    }

    public void setTimeString(String s) {
        this.timeString = s;
    }
    public String getTimeString() {
        return this.timeString;
    }

    public void setDepartment(int n) {
        this.department = n;
    }
    public int getDepartment() {
        return this.department;
    }

    public void setDepartmentString(String s) {
        this.departmentString = s;
    }
    public String getDepartmentString() {
        return this.departmentString;
    }

    public void setDoctor(int n) {
        this.doctor = n;
    }
    public int getDoctor() {
        return this.doctor;
    }

    public void setDoctorName(String s) {
        this.doctorName = s;
    }
    public String getDoctorName() {
        return this.doctorName;
    }

    public void setViewHolder(MedicalCertificateItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public MedicalCertificateItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
