package org.snuh.www.app.doctordetail;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdetail.Mobile5100ViewPager;

public class Mobile5200ImageAndTab extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    int doctor_pk;

    /**************************************************************/

    String[] tabTitles = new String[]{"기본정보", "감사댓글"};
    TextView tab1;
    TextView tab2;

    Mobile5200ViewPager mobile5200ViewPager;

    ImageView imageView;
    TabLayout tabLayout;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5202: // 의료진 이미지 요청 후처리
                    mobile5202(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    public Mobile5200ImageAndTab(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5200ImageAndTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5200ImageAndTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5200_image_and_tab, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        imageView = (ImageView) v.findViewById(R.id.imageView);
        tabLayout = (TabLayout) v.findViewById(R.id.tabLayout);


        tab1 = (TextView) LayoutInflater.from(activity).inflate(R.layout.tab, null);
        tab2 = (TextView) LayoutInflater.from(activity).inflate(R.layout.tab, null);

    }

    public void setRealInItView() {
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        mobile5201();
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        tab1.setText(tabTitles[0]);
        tab2.setText(tabTitles[1]);

        tabLayout.setupWithViewPager(mobile5200ViewPager.viewPager);
        tabLayout.addTab(tabLayout.newTab().setCustomView(tab1));
        tabLayout.addTab(tabLayout.newTab().setCustomView(tab2));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(useFul.getRcolor(R.color.mobile5100_tab_button_text_normal_color), useFul.getRcolor(R.color.mobile5100_tab_button_text_on_color));
        tabLayout.setSelectedTabIndicatorHeight(useFul.getRdimen(R.dimen.mobile5100_tab_bottom_line_height));
        tabLayout.setSelectedTabIndicatorColor(useFul.getRcolor(R.color.mobile5100_tab_bottom_line_color));
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        tabLayout.addOnTabSelectedListener(tabSelectEventListener);
    }

    TabLayout.OnTabSelectedListener tabSelectEventListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            // viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    /**************************************************************/

    // 의료진 이미지 요청
    public void mobile5201() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5201");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("doctor_pk", doctor_pk);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        inLet.setHandler(handler);
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandlerRequestNumber(5202);
        inLet.start();
    }

    // 의료진 이미지 요청 후처리
    public void mobile5202(Object obj) {
        String data = obj.toString();

        if (data.equals("no")) {

        } else {
            if (!data.equals("")) {
                String base64ImageString = data;
                String encodedString = base64ImageString;
                String pureBase64Encoded = encodedString.substring(encodedString.indexOf(",") + 1);
                byte[] decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT);

                Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
                imageView.setImageBitmap(decodedBitmap);
            }
        }





    }









    public void setMobile5200ViewPager(Mobile5200ViewPager mobile5200ViewPager) {
        this.mobile5200ViewPager = mobile5200ViewPager;
    }

    public void setDoctorPk(int n) {
        this.doctor_pk = n;
    }
}
