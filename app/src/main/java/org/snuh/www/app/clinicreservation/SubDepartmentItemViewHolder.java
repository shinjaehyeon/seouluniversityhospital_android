package org.snuh.www.app.clinicreservation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class SubDepartmentItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public TextView codeTextView;
    public TextView department;
    public Button departmentButton;

    /**************************************************************/

    public SubDepartmentItemViewHolder(View view) {
        super(view);
        this.view = view;

        codeTextView = view.findViewById(R.id.codeTextView);
        department = view.findViewById(R.id.department);
        departmentButton = view.findViewById(R.id.departmentButton);
    }
}
