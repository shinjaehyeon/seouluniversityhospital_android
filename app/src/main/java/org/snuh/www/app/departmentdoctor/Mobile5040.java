package org.snuh.www.app.departmentdoctor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

public class Mobile5040 extends Fragment {
    Activity activity;
    View rootView;
    UseFul useFul;

    /**************************************************************/

    LinearLayout filter_content_area;
    LinearLayout recyclerView_area;

    Mobile5040Filter mobile5040Filter;
    Mobile5040RecyclerView mobile5040RecyclerView;

    /**************************************************************/


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 9999:

                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    public Mobile5040() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5040(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile5040, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        filter_content_area = (LinearLayout) rootView.findViewById(R.id.filter_content_area);
        recyclerView_area = (LinearLayout) rootView.findViewById(R.id.recyclerView_area);


        mobile5040Filter = new Mobile5040Filter(activity);
        mobile5040RecyclerView = new Mobile5040RecyclerView(activity);

        mobile5040Filter.inflateView();
        mobile5040Filter.setConnectViewId();
        mobile5040RecyclerView.inflateView();
        mobile5040RecyclerView.setConnectViewId();

        mobile5040Filter.setMobile5040RecyclerView(mobile5040RecyclerView);
        mobile5040RecyclerView.setMobile5040Filter(mobile5040Filter);

        mobile5040Filter.setRealInItView();
        mobile5040RecyclerView.setRealInItView();


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                filter_content_area.addView(mobile5040Filter);
            }
        });

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView_area.addView(mobile5040RecyclerView);
            }
        });

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

}
