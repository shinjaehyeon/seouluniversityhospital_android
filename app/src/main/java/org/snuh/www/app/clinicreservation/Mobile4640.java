package org.snuh.www.app.clinicreservation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class Mobile4640 extends Fragment {
    Activity activity;
    View rootView;
    NestedScrollView nestedScrollView;
    UseFul useFul;


    Mobile4660 mobile4660;

    /**************************************************************/

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<SubDoctorItem> subDoctorItems = new ArrayList<SubDoctorItem>();
    SubDoctorItemAdapter adapter;

    /**************************************************************/


    int currentSelectedDoctorPrimaryKey;
    String currentSelectedDoctorName;


    /**************************************************************/

    public Mobile4640() {

    }

    @SuppressLint("ValidFragment")
    public Mobile4640(Activity activity) {
        this.activity = activity;
    }

    @SuppressLint("ValidFragment")
    public Mobile4640(Activity activity, NestedScrollView nestedScrollView) {
        this.activity = activity;
        this.nestedScrollView = nestedScrollView;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile4640, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        setRecyclerView(); // 리사이클러뷰 관련 환경 설정하기

        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        adapter = new SubDoctorItemAdapter(recyclerView, subDoctorItems, activity, itemOnClickListener);
        recyclerView.setAdapter(adapter);
    }

    // 부모로부터 의료진 리스트담긴 주소 참조받기
    public void setDoctorArrayList(ArrayList<SubDoctorItem> items) {
        subDoctorItems = items;
    }

    // 부모로부터 호출할 함수인데, 의료진 리스트 갱신하기
    public void setRefreshDoctorList() {
        adapter.notifyDataSetChanged();

        nestedScrollView.fullScroll(View.FOCUS_UP);
        nestedScrollView.scrollTo(0,0);
    }

    /**************************************************************/

    public Mobile4620 mobile4620;
    public void setMobile4620(Mobile4620 mobile4620) {
        this.mobile4620 = mobile4620;
    }


    SubDoctorItem previousSubDoctorItem = null;

    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SubDoctorItem subDoctorItem = (SubDoctorItem) v.getTag();


                currentSelectedDoctorPrimaryKey = subDoctorItem.getDoctorPrimaryKey();
                currentSelectedDoctorName = subDoctorItem.getDoctorName();

                mobile4660.department = mobile4620.currentSelectedDepartmentCode;
                mobile4660.doctor = currentSelectedDoctorPrimaryKey;

                SubDoctorItemViewHolder userViewHolder = (SubDoctorItemViewHolder) subDoctorItem.getSubDoctorItemViewHolder();
                userViewHolder.doctor_checkbox.setImageResource(R.drawable.ic_mobile0000_checkbox_on);
                userViewHolder.border_top.setBackgroundColor(useFul.getRcolor(R.color.mobile4640_doctor_list_border_top_on_color));
                userViewHolder.border_bottom.setBackgroundColor(useFul.getRcolor(R.color.mobile4640_doctor_list_border_bottom_on_color));

                if (previousSubDoctorItem != null) {
                    if (subDoctorItem.getDoctorPrimaryKey() != previousSubDoctorItem.getDoctorPrimaryKey()) {
                        SubDoctorItemViewHolder prevUserViewHolder = (SubDoctorItemViewHolder) previousSubDoctorItem.getSubDoctorItemViewHolder();
                        prevUserViewHolder.doctor_checkbox.setImageResource(R.drawable.ic_mobile0000_checkbox_off);
                        prevUserViewHolder.border_top.setBackgroundColor(useFul.getRcolor(R.color.mobile4640_doctor_list_border_top_normal_color));
                        prevUserViewHolder.border_bottom.setBackgroundColor(useFul.getRcolor(R.color.mobile4640_doctor_list_border_bottom_normal_color));
                    }
                }

                mobile4660.allClear();

                previousSubDoctorItem = subDoctorItem;

        }
    };

    /**************************************************************/

    public void setMobile4660(Mobile4660 mobile4660) {
        this.mobile4660 = mobile4660;
    }




    public void allClear() {
        if (previousSubDoctorItem != null) {
            SubDoctorItemViewHolder prevUserViewHolder = (SubDoctorItemViewHolder) previousSubDoctorItem.getSubDoctorItemViewHolder();
            prevUserViewHolder.doctor_checkbox.setImageResource(R.drawable.ic_mobile0000_checkbox_off);
            prevUserViewHolder.border_top.setBackgroundColor(useFul.getRcolor(R.color.mobile4640_doctor_list_border_top_normal_color));
            prevUserViewHolder.border_bottom.setBackgroundColor(useFul.getRcolor(R.color.mobile4640_doctor_list_border_bottom_normal_color));
        }
        previousSubDoctorItem = null;

        currentSelectedDoctorName = "";
        currentSelectedDoctorPrimaryKey = 0;

        subDoctorItems.clear();
        adapter.notifyDataSetChanged();
    }
}
