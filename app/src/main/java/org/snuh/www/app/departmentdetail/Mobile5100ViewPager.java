package org.snuh.www.app.departmentdetail;

import android.content.Context;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdoctor.Mobile5000ViewPager;

public class Mobile5100ViewPager extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    /**************************************************************/

    int department_pk;

    NestedScrollView nestedScrollView;

    Mobile5100ImageAndTab mobile5100ImageAndTab;

    Mobile5120 mobile5120;
    Mobile5140 mobile5140;
    // Mobile5160 mobile5160;
    ViewPager viewPager;

    /**************************************************************/

    public Mobile5100ViewPager(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5100ViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5100ViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5100_viewpager, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        mobile5120 = new Mobile5120(activity);
        mobile5140 = new Mobile5140(activity);
        // mobile5160 = new Mobile5160(activity);
    }

    public void setRealInItView() {
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    public void setDepartmentPk(int n) {
        this.department_pk = n;
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        mobile5120.setDepartmentPk(department_pk);
        mobile5120.mobile5121();

        mobile5140.setDepartmentPk(department_pk);
        mobile5140.setNestedScrollView(nestedScrollView);

        viewPager.setAdapter(new pagerAdapter(activity.getSupportFragmentManager()));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    private class pagerAdapter extends FragmentStatePagerAdapter {

        public pagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mobile5100ImageAndTab.tabTitles[position];
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return mobile5120;
                case 1:
                    return mobile5140;
//                case 2:
//                    return mobile5160;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    /**************************************************************/

    public void setMobile5100ImageAndTab(Mobile5100ImageAndTab mobile5100ImageAndTab) {
        this.mobile5100ImageAndTab = mobile5100ImageAndTab;
    }

    public void setNestedScrollView(NestedScrollView nestedScrollView) {
        this.nestedScrollView = nestedScrollView;
    }
}
