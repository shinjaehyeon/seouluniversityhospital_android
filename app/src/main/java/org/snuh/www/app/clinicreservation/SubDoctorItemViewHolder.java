package org.snuh.www.app.clinicreservation;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class SubDoctorItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public ImageView doctor_image;
    public TextView doctor_department;
    public TextView doctor_name;
    public ImageView doctor_checkbox;

    public RelativeLayout border_top;
    public RelativeLayout border_bottom;

    /**************************************************************/

    public SubDoctorItemViewHolder(View view) {
        super(view);
        this.view = view;

        doctor_image = (ImageView) view.findViewById(R.id.doctor_image);
        doctor_department = (TextView) view.findViewById(R.id.doctor_department);
        doctor_name = (TextView) view.findViewById(R.id.doctor_name);
        doctor_checkbox = (ImageView) view.findViewById(R.id.doctor_checkbox);

        border_top = (RelativeLayout) view.findViewById(R.id.border_top);
        border_bottom = (RelativeLayout) view.findViewById(R.id.border_bottom);
    }
}
