package org.snuh.www.app.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class CustomSelectDialogSelectionButton extends LinearLayout {
    View v;
    Activity activity;
    Context context;


    int index;
    Button button;

    public CustomSelectDialogSelectionButton(Context context) {
        super(context);
        initView();
    }

    public CustomSelectDialogSelectionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CustomSelectDialogSelectionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.custom_select_dialog_selection_button, this, false);
        addView(v);

        button = (Button) v.findViewById(R.id.button);
        button.setTag(this);
    }




    public Button getButton() {
        return button;
    }

    public int getIndex() {
        return this.index;
    }



    public void setIndex(int n) {
        this.index = n;
    }

    public void setText(String s) {
        button.setText(s+"");
    }

    public void setTextColor(int n) {
        button.setTextColor(n);
    }

    public void setButtonBackgroundResource(int n) {
        button.setBackgroundResource(n);
    }

    public void setActiveEffect() {
        button.setBackgroundResource(R.drawable.ripple_button_type_2);
        button.setTextColor(Color.parseColor("#ffffff"));
    }

    public void setNormalEffect() {
        button.setBackgroundResource(R.drawable.ripple_button_type_6);
        button.setTextColor(Color.parseColor("#666666"));
    }
}
