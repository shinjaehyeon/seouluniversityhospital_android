package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class SubDepartmentSelectDialog extends Dialog {
    Context context;
    UseFul useFul;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    /**************************************************************/

    public int currentSelectedDepartmentCode;
    public String currentSelectedDepartmentString;
    SubDepartmentItemViewHolder prevSelectedViewHolder = null;

    /**************************************************************/

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<SubDepartmentItem> subDepartmentItems = new ArrayList<SubDepartmentItem>();
    SubDepartmentItemAdapter subDepartmentItemAdapter;

    /**************************************************************/

    public SubDepartmentSelectDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.sub_department_selected_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        setRecyclerView(); // 리사이클러뷰 관련 환경 설정하기
    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(context);


        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    /**************************************************************/

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(context,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        subDepartmentItemAdapter = new SubDepartmentItemAdapter(recyclerView, subDepartmentItems, (Activity) context, itemOnClickListener);
        recyclerView.setAdapter(subDepartmentItemAdapter);
    }

    // 진료과 리스트 받아와서 ArrayList에 저장하기
    public void setSubDepartmentItems(ArrayList<SubDepartmentItem> subDepartmentItems) {
        this.subDepartmentItems = subDepartmentItems;
    }

    /**************************************************************/

    public boolean isOnItemClicked = false;

    // 진료과 리스트중 하나를 클릭 했을 시 발생하는 이벤트 설정
    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isOnItemClicked = true;

            if (prevSelectedViewHolder != null) {
                prevSelectedViewHolder.department.setTextColor(useFul.getRcolor(R.color.mobile4620_department_list_text_off_color));
                prevSelectedViewHolder.departmentButton.setBackgroundResource(R.drawable.ripple_button_type_6);
            }

            SubDepartmentItem subDepartmentItem = (SubDepartmentItem) v.getTag();
            subDepartmentItem.getViewHolder().department.setTextColor(useFul.getRcolor(R.color.mobile4620_department_list_text_on_color));
            subDepartmentItem.getViewHolder().departmentButton.setBackgroundResource(R.drawable.ripple_button_type_2);
            currentSelectedDepartmentCode = subDepartmentItem.getCode();
            currentSelectedDepartmentString = subDepartmentItem.getDepartment();

            prevSelectedViewHolder = subDepartmentItem.getViewHolder();

            dismiss();
        }
    };


}
