package org.snuh.www.app.doctordetail;

import android.content.Context;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdetail.Mobile5100ImageAndTab;
import org.snuh.www.app.departmentdetail.Mobile5120;
import org.snuh.www.app.departmentdetail.Mobile5140;
import org.snuh.www.app.departmentdetail.Mobile5160;

public class Mobile5200ViewPager extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    /**************************************************************/

    int doctor_pk;

    NestedScrollView nestedScrollView;

    Mobile5200ImageAndTab mobile5200ImageAndTab;

    Mobile5220 mobile5220;
    Mobile5240 mobile5240;
    ViewPager viewPager;

    /**************************************************************/

    public Mobile5200ViewPager(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5200ViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5200ViewPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5200_viewpager, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        viewPager = (ViewPager) v.findViewById(R.id.viewPager);
        mobile5220 = new Mobile5220(activity);
        mobile5240 = new Mobile5240(activity);
    }

    public void setRealInItView() {
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    public void setDoctorPk(int n) {
        this.doctor_pk = n;
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        mobile5220.setDoctorPk(doctor_pk);
        mobile5220.setNestedScrollView(nestedScrollView);

        mobile5240.setDoctorPk(doctor_pk);
        mobile5240.setNestedScrollView(nestedScrollView);


        viewPager.setAdapter(new pagerAdapter(activity.getSupportFragmentManager()));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    private class pagerAdapter extends FragmentStatePagerAdapter {

        public pagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);


        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mobile5200ImageAndTab.tabTitles[position];
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return mobile5220;
                case 1:
                    return mobile5240;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    /**************************************************************/

    public void setMobile5200ImageAndTab(Mobile5200ImageAndTab mobile5200ImageAndTab) {
        this.mobile5200ImageAndTab = mobile5200ImageAndTab;
    }

    public void setNestedScrollView(NestedScrollView nestedScrollView) {
        this.nestedScrollView = nestedScrollView;
    }
}
