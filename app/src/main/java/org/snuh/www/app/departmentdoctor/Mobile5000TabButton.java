package org.snuh.www.app.departmentdoctor;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.UseFul;

public class Mobile5000TabButton extends LinearLayout {
    Activity activity;
    Context context;
    UseFul useFul;
    View v;
    Mobile5000ViewPager viewPager;

    /**************************************************************/

    Button tab_1_button;
    Button tab_2_button;

    TextView tab_1_button_text_on;
    TextView tab_1_button_text_off;
    TextView tab_2_button_text_on;
    TextView tab_2_button_text_off;

    /**************************************************************/

    public Mobile5000TabButton(Context context) {
        super(context);
        this.context = context;
        this.activity = (Activity) context;
        this.useFul = new UseFul(activity);

    }

    public Mobile5000TabButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (Activity) context;
        this.useFul = new UseFul(activity);

    }

    public Mobile5000TabButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (Activity) context;
        this.useFul = new UseFul(activity);

    }

    /**************************************************************/

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5000_tab_button, this, false);
        addView(v);


        setConnectViewId();
        setObjectEvent();
    }

    /**************************************************************/

    // 부모가 갖고 있는 Mobile5000ViewPager 객체 전달 받기
    public void setViewPager(Mobile5000ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    /**************************************************************/

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        tab_1_button = (Button) v.findViewById(R.id.tab_1_button);
        tab_2_button = (Button) v.findViewById(R.id.tab_2_button);

        tab_1_button.setTag(0);
        tab_2_button.setTag(1);

        tab_1_button_text_on = (TextView) v.findViewById(R.id.tab_1_button_text_on);
        tab_1_button_text_off = (TextView) v.findViewById(R.id.tab_1_button_text_off);
        tab_2_button_text_on = (TextView) v.findViewById(R.id.tab_2_button_text_on);
        tab_2_button_text_off = (TextView) v.findViewById(R.id.tab_2_button_text_off);

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        tab_1_button.setOnClickListener(movePageListener);
        tab_2_button.setOnClickListener(movePageListener);
    }

    /**************************************************************/

    View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            tabButtonEffect(tag);

            viewPager.viewPager.setCurrentItem(tag);
        }
    };

    public void tabButtonEffect(int n) {
        if (n == 0) {
            tab_1_button_text_on.setVisibility(View.VISIBLE);
            tab_1_button_text_off.setVisibility(View.GONE);

            tab_2_button_text_on.setVisibility(View.GONE);
            tab_2_button_text_off.setVisibility(View.VISIBLE);
        } else {
            tab_1_button_text_on.setVisibility(View.GONE);
            tab_1_button_text_off.setVisibility(View.VISIBLE);

            tab_2_button_text_on.setVisibility(View.VISIBLE);
            tab_2_button_text_off.setVisibility(View.GONE);
        }
    }
}
