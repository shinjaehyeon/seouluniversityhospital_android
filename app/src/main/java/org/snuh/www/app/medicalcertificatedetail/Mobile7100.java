package org.snuh.www.app.medicalcertificatedetail;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.medicalcertificateitemset.MedicalCertificateItem;
import org.snuh.www.app.medicalcertificateitemset.MedicalCertificateItemAdapter;
import org.snuh.www.app.prescriptiondrugitemset.PrescriptionDrugItem;
import org.snuh.www.app.prescriptiondrugitemset.PrescriptionDrugItemAdapter;

import java.util.ArrayList;

public class Mobile7100 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    int suhmc; // 진단서 pk

    /**************************************************************/

    TextView certificate_date;
    TextView certificate_time;
    TextView certificate_department;
    TextView certificate_doctor;
    TextView certificate_time_doctor_comment;

    TextView result_none;


    NestedScrollView nestedScrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<PrescriptionDrugItem> items = new ArrayList<PrescriptionDrugItem>();
    PrescriptionDrugItemAdapter itemAdapter;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 7102: // 서버에 진단서 상세정보 요청 후처리
                    mobile7102(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile7100);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기

        setRecyclerView(); // 리사이클러뷰 관련 환경 설정하기

        mobile7101(); // 서버에 진단서 상세정보 요청
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        suhmc = getIntent().getIntExtra("suhmc", 0);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);


        certificate_date = (TextView) findViewById(R.id.certificate_date);
        certificate_time = (TextView) findViewById(R.id.certificate_time);
        certificate_department = (TextView) findViewById(R.id.certificate_department);
        certificate_doctor = (TextView) findViewById(R.id.certificate_doctor);
        certificate_time_doctor_comment = (TextView) findViewById(R.id.certificate_time_doctor_comment);


        result_none = (TextView) findViewById(R.id.result_none);

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new PrescriptionDrugItemAdapter(recyclerView, items, activity, null, nestedScrollView);
        recyclerView.setAdapter(itemAdapter);
    }

    /**************************************************************/

    // 서버에 진단서 상세정보 요청
    public void mobile7101() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile7101");
        params.put("member", new ShareInfo(this).getMember());
        params.put("suhmc", suhmc);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(7102);
        inLet.start();
    }

    // 서버에 진단서 상세정보 요청 후처리
    public void mobile7102(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile7102 data = "+data);


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                // 진단서 기본 정보 가져와 보여주기
                JSONObject jsonObject1 = jsonObject.getJSONObject("data1");
                String medical_certificate_date = jsonObject1.getString("medical_certificate_date");
                String medical_certificate_time = jsonObject1.getString("medical_certificate_time");
                String department_string = jsonObject1.getString("department_string");
                String doctor_name = jsonObject1.getString("doctor_name");
                String doctor_comment = jsonObject1.getString("doctor_comment");

                certificate_date.setText(medical_certificate_date+"");
                certificate_time.setText(medical_certificate_time+"");
                certificate_department.setText(department_string);
                certificate_doctor.setText(doctor_name);
                certificate_time_doctor_comment.setText(doctor_comment);


                // 진단서의 처방약품 리스트 가져와 보여주기
                JSONArray jsonArray = jsonObject.getJSONArray("data2");
                if (jsonArray.length() >= 1) {
                    result_none.setVisibility(View.GONE);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                        int suhpd = jsonObject2.getInt("suhpd"); // 처방약품 pk
                        int drug_code = jsonObject2.getInt("drug_code"); // 약품 pk
                        String drug_name = jsonObject2.getString("drug_name"); // 약품 이름
                        float oneday_dose = (float) jsonObject2.getDouble("oneday_dose"); // 하루 투여량
                        float oneday_number = (float) jsonObject2.getDouble("oneday_number"); // 하루 투여횟수
                        float dose_day_num = (float) jsonObject2.getDouble("dose_day_num"); // 총 투여일수
                        String note = jsonObject2.getString("note"); // 약품 비고

                        PrescriptionDrugItem item = new PrescriptionDrugItem();
                        item.setPrimarykey(suhpd);
                        item.setDrugPrimaryKey(drug_code);
                        item.setDrugName(drug_name + "");
                        item.setOnedayDose(oneday_dose);
                        item.setOnedayNumber(oneday_number);
                        item.setDoseDayNum(dose_day_num);
                        item.setNote(note);
                        items.add(item);
                    }

                    itemAdapter.setLoaded();
                    itemAdapter.notifyDataSetChanged();
                } else {
                    result_none.setVisibility(View.VISIBLE);
                }

            } else {

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/



    /**************************************************************/


}
