package org.snuh.www.app.historyitemset;


import org.snuh.www.app.hospitalnewsitemset.HospitalNewsItemViewHolder;

public class HistoryItem {
    public HistoryItemViewHolder viewHolder;
    public String period;
    public String content;

    public void setPeriod(String s) {
        this.period = s;
    }
    public String getPeriod() {
        return this.period;
    }

    public void setContent(String s) {
        this.content = s;
    }
    public String getContent() {
        return this.content;
    }

    public void setViewHolder(HistoryItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public HistoryItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
