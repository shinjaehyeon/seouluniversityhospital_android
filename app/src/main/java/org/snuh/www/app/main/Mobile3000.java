package org.snuh.www.app.main;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.crhitemset.CRHItem;
import org.snuh.www.app.crhitemset.CRHItemAdapter;
import org.snuh.www.app.crhitemset.CRHItemViewHolder;
import org.snuh.www.app.departmentdoctor.Mobile5000;
import org.snuh.www.app.hospitalnews.Mobile8000;
import org.snuh.www.app.medicalcertificate.Mobile7000;
import org.snuh.www.app.orderwaitingticket.Mobile6500;
import org.snuh.www.app.personalquestion.Mobile5500;
import org.snuh.www.app.profileinfoupdate.Mobile3500;

import java.util.ArrayList;
import java.util.Calendar;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile3000 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    /**************************************************************/

    TextView profile_name;
    TextView profile_recent_clinic_date;
    TextView profile_all_reservation_num;
    TextView profile_all_clinic_num;

    Button myinfo_edit_button;

    /**************************************************************/

    Button major_menu_1;
    Button major_menu_2;
    Button major_menu_3;
    Button major_menu_4;

    Button big_major_button_1;
    Button big_major_button_2;

    /**************************************************************/

    RelativeLayout synchronization_button;

    /**************************************************************/

    int index = 0;
    int view_num = 6;

    NestedScrollView scrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<CRHItem> crhItems = new ArrayList<CRHItem>();
    CRHItemAdapter crhItemAdapter;

    CRHItem cancelWantCrhItem;

    Button moreViewButton;

    /**************************************************************/

    RelativeLayout progressBarLayout;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 3002:
                    mobile3002(msg.obj); // 서버에 프로필정보 요청 후처리
                    break;
                case 3004:
                    mobile3004(msg.obj); // 서버에 프로필정보에 휴대폰 번호 및 인적정보가 업데이트 되어있는지에 대한 여부 요청 후처리
                    break;
                case 3011:
                    mobile3011(msg.obj); // 서버에 최근 진료예약 내역 요청 후처리
                    break;
                case 3021:
                    mobile3021(msg.obj); // 서버에 진료예약 취소 요청 후처리
                    break;

                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile3000);




        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기


        // mobile3001(); // 서버에 프로필정보 요청 (이름, 최근진료날짜, 총 예약횟수, 총 진단 횟수)

        setRecyclerView(); // 리사이클러뷰 관련 환경 설정하기
        // mobile3010(); // 최근 진료예약 내역 요청 보내기
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Log.i("MyTags", "Main onStart 시작");
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.i("MyTags", "Main onPause 시작");
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.i("MyTags", "Main onResume 시작");
        new UseFul(activity).setAppRunning(1);
    }


    /**************************************************************/

    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        profile_name = (TextView) findViewById(R.id.profile_name);
        profile_recent_clinic_date = (TextView) findViewById(R.id.profile_recent_clinic_date);
        profile_all_reservation_num = (TextView) findViewById(R.id.profile_all_reservation_num);
        profile_all_clinic_num = (TextView) findViewById(R.id.profile_all_clinic_num);

        /**************************************************************/

        myinfo_edit_button = (Button) findViewById(R.id.myinfo_edit_button);

        /**************************************************************/

        major_menu_1 = (Button) findViewById(R.id.major_menu_1);
        major_menu_2 = (Button) findViewById(R.id.major_menu_2);
        major_menu_3 = (Button) findViewById(R.id.major_menu_3);
        major_menu_4 = (Button) findViewById(R.id.major_menu_4);

        big_major_button_1 = (Button) findViewById(R.id.big_major_button_1);
        big_major_button_2 = (Button) findViewById(R.id.big_major_button_2);

        /**************************************************************/

        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);

        /**************************************************************/

        synchronization_button = (RelativeLayout) findViewById(R.id.synchronization_button);

        /**************************************************************/

        scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        moreViewButton = (Button) findViewById(R.id.moreViewButton);

        progressBarLayout = (RelativeLayout) findViewById(R.id.progressBarLayout);

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // 내정보 수정 버튼 클릭시 이벤트 연결
        myinfo_edit_button.setOnClickListener(myinfoEditButtonClickEvent);


        // 주요메뉴 클릭시 이벤트 연결
        major_menu_1.setOnClickListener(majorMenuClickEvent);
        major_menu_2.setOnClickListener(majorMenuClickEvent);
        major_menu_3.setOnClickListener(majorMenuClickEvent);
        major_menu_4.setOnClickListener(majorMenuClickEvent);

        // 큰 주요메뉴 클릭시 이벤트 연결
        big_major_button_1.setOnClickListener(bigMajorMenuEvent);
        big_major_button_2.setOnClickListener(bigMajorMenuEvent);


        // 최근진료예약내역 동기화 버튼 이벤트 연결
        synchronization_button.setOnClickListener(synchronizationEvent);



    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    /**************************************************************/



    // 최근진료예약내역 동기화 버튼 이벤트
    View.OnClickListener synchronizationEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            synchronizationCRH();
        }
    };

    public void allClear() {
        index = 0;
        crhItems.clear();
        crhItemAdapter.isMoreInfo = true;
        crhItemAdapter.notifyDataSetChanged();
        recyclerView.removeAllViews();
        cancelWantCrhItem = null;
    }

    // 진료예약내역 동기화
    public void synchronizationCRH() {
        allClear();

        mobile3010();
    }

    // 내정보 수정 버튼 클릭시 이벤트
    View.OnClickListener myinfoEditButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goToActivity(Mobile3500.class);
        }
    };

    // 주요메뉴 클릭시 이벤트
    View.OnClickListener majorMenuClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.major_menu_1: // 진료예약 버튼 클릭시
                    mobile3003(); // 서버에 프로필정보에 휴대폰 번호 및 인적정보가 업데이트 되어있는지에 대한 여부 요청
                    break;
                case R.id.major_menu_2: // 진료과/의료진 버튼 클릭시
                    goToActivity(Mobile5000.class);
                    break;
                case R.id.major_menu_3: // 1:1문의 버튼 클릭시
                    goToActivity(Mobile5500.class);
                    break;
                case R.id.major_menu_4: // 병원뉴스 버튼 클릭시
                    goToActivity(Mobile8000.class);
                    break;
                default:

                    break;
            }
        }
    };

    // 큰 주요메뉴 클릭시 이벤트
    View.OnClickListener bigMajorMenuEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.big_major_button_1: // 순번대기표 버튼 클릭시 발생할 이벤트
                    goToActivity(Mobile6500.class);
                    break;
                case R.id.big_major_button_2: // 진단서 버튼 클릭시 발생할 이벤트
                    goToActivity(Mobile7000.class);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        crhItemAdapter = new CRHItemAdapter(recyclerView, crhItems, activity, null, scrollView);
        recyclerView.setAdapter(crhItemAdapter);

        crhItemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                crhItems.add(null);
                crhItemAdapter.notifyItemInserted(crhItems.size() - 1);

                mobile3010();
            }
        });

        // 스크롤로 더보기 구현하고 싶으면
//        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                int scrollViewPos = scrollView.getScrollY();
//                int TextView_lines = scrollView.getChildAt(0).getBottom() - scrollView.getHeight();
//
//                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//
//
//                    if (TextView_lines == scrollViewPos) {
//                        if (crhItemAdapter.isMoreInfo == true) {
//                            crhItemAdapter.totalItemCount = linearLayoutManager.getItemCount();
//                            crhItemAdapter.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//
//                            if (!crhItemAdapter.isLoading && crhItemAdapter.totalItemCount <= (crhItemAdapter.lastVisibleItem + crhItemAdapter.visibleThreshold)) {
//                                crhItemAdapter.isLoading = true;
//
//                                if (crhItemAdapter.onLoadMoreListener != null) {
//                                    crhItemAdapter.onLoadMoreListener.onLoadMore();
//                                }
//
//                            }
//                        } else {
//                            // new UseFul(activity).showToast("불러올 데이터가 없습니다. - 스크롤");
//                        }
//                    }
//
//
//            }
//        });

        // 버튼으로 더보기 구현하고 싶으면
        moreViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (crhItemAdapter.isMoreInfo == true) {
                    if (!crhItemAdapter.isLoading) {
                        crhItemAdapter.isLoading = true;
                        if (crhItemAdapter.onLoadMoreListener != null) {
                            crhItemAdapter.onLoadMoreListener.onLoadMore();
                        }
                    }
                } else {
                    callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.", false);
                }
            }
        });
    }

    /**************************************************************/

    // 서버에 프로필정보 요청 (이름, 최근진료날짜, 총 예약횟수, 총 진단 횟수)
    public void mobile3001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3001");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3002);
        inLet.start();
    }
    // 서버에 프로필정보 요청 후처리
    public void mobile3002(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                String name = jsonObject1.getString("name"); // 이름
                String recent_clinic_full_date = jsonObject1.getString("recent_clinic_full_date"); // 최근 진료 날짜
                int all_reservation_num = jsonObject1.getInt("all_reservation_num"); // 총 예약 횟수
                int all_clinic_num = jsonObject1.getInt("all_clinic_num"); // 총 진료 횟수

                profile_name.setText(name);
                profile_recent_clinic_date.setText(recent_clinic_full_date);
                profile_all_reservation_num.setText(all_reservation_num+" 회");
                profile_all_clinic_num.setText(all_clinic_num+" 회");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 서버에 프로필정보에 휴대폰 번호 및 인적정보가 업데이트 되어있는지에 대한 여부 요청
    public void mobile3003() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3003");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3004);
        inLet.start();
    }

    // 서버에 프로필정보에 휴대폰 번호 및 인적정보가 업데이트 되어있는지에 대한 여부 요청 후처리
    public void mobile3004(Object obj) {
        String data = obj.toString();
        
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                // 회원정보에 필요한 정보가 등록되어 있으면
                goToActivity(Mobile4500.class); // 진료예약 페이지로 이동
            } else {
                // 회원정보에 필요한 정보가 없으면
                // 프로필 업데이트 권유 팝업창 띄우기
                callSUHDialogType1Dialog("안내", "회원정보 프로필 업데이트 후 이용 가능합니다.", false);
            }


        } catch (JSONException e) {
            
        }


    }

    // 서버에 최근 진료예약 내역 요청
    public void mobile3010() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3010");
        params.put("member", new ShareInfo(this).getMember());
        params.put("index", index);
        params.put("view_num", view_num);
        params.put("period_size", 0);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3011);
        inLet.start();
    }
    // 서버에 최근 진료예약 내역 요청 후처리
    public void mobile3011(Object obj) {
        String data = obj.toString();

        Log.i("Final3000", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                String more_info = jsonObject.getString("more_info");

                if (crhItems.size() >= 1) {
                    crhItems.remove(crhItems.size() - 1);
                    crhItemAdapter.notifyItemRemoved(crhItems.size());
                }

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int pk = jsonObject1.getInt("suhcrh");
                    String doctor_name = jsonObject1.getString("doctor_name");
                    String department_string = jsonObject1.getString("department_string");
                    String clinic_reservation_date = jsonObject1.getString("clinic_reservation_date");
                    String clinic_reservation_time = jsonObject1.getString("clinic_reservation_time");
                    int status = jsonObject1.getInt("status");
                    String status_string = jsonObject1.getString("status_string");

                    int year = jsonObject1.getInt("year");
                    String month = useFul.attachZero(jsonObject1.getInt("month"));
                    String date = useFul.attachZero(jsonObject1.getInt("date"));


                    CRHItem crhItem = new CRHItem();
                    crhItem.setPrimaryKey(pk);
                    crhItem.setDoctorName(doctor_name+" 의료진");
                    crhItem.setDeparment(department_string+" 진료");
                    crhItem.setClinicReservationDate(clinic_reservation_date);
                    crhItem.setClinicReservationTime(clinic_reservation_time);
                    crhItem.setStatus(status);
                    crhItem.setStatusString(status_string);
                    crhItem.setCancelButtonClickEvent(cancelButtonClickEvent);
                    crhItem.setYear(year+"");
                    crhItem.setMonth(month);
                    crhItem.setDate(date);

                    crhItems.add(crhItem);
                }
                crhItemAdapter.setLoaded();
                crhItemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    crhItemAdapter.isMoreInfo = true;
                } else {
                    crhItemAdapter.isMoreInfo = false;
                }
                index = index + view_num;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener cancelButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cancelWantCrhItem = (CRHItem) v.getTag();

            int year = Integer.parseInt(cancelWantCrhItem.getYear());
            int month = Integer.parseInt(cancelWantCrhItem.getMonth());
            int date = Integer.parseInt(cancelWantCrhItem.getDate());

            Log.i("MyTags", "year = "+year);
            Log.i("MyTags", "month = "+month);
            Log.i("MyTags", "date = "+date);

            Calendar big = Calendar.getInstance();
            Calendar small = Calendar.getInstance();
            small.set(small.YEAR, year);
            small.set(small.MONTH, month-1);
            small.set(small.DATE, date);



            int dday = useFul.differDatetime(big, small);
            Log.i("MyTags", "날짜 차이 = "+dday);
            if (dday > -2) {
                callSUHDialogType1Dialog("안내", "진료예약일이 내일인 경우에는 취소할 수 없습니다.", false);
            } else {
                // new UseFul(activity).showToast("취소 버튼 눌른 진료예약 pk는 "+crhItem.getPk());
                callSUHDialogType1Dialog2("안내", "정말 해당 진료예약을 취소하시겠습니까?");
            }
        }
    };

    // 서버에 진료예약 취소 요청
    public void mobile3020() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3020");
        params.put("member", new ShareInfo(this).getMember());
        params.put("crh_pk", cancelWantCrhItem.getPk());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3021);
        inLet.start();
    }
    // 서버에 진료예약 취소 요청 후처리
    public void mobile3021(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                // 취소 성공했으면
                CRHItemViewHolder crhItemViewHolder = cancelWantCrhItem.getViewHolder();

                // 글자 취소처리됨 으로 문자열 바꾸고 색 회색으로 바꾸기
                crhItemViewHolder.current_status.setText("취소됨");
                crhItemViewHolder.current_status.setTextColor(new UseFul(activity).getRcolor(R.color.clinic_reservation_list_current_status_text_off_color));
                // 취소 버튼 없애기
                crhItemViewHolder.cancel_button.setVisibility(View.GONE);
            } else {
                // 취소 실패했으면
                callSUHDialogType1Dialog("안내", "취소 과정에 오류가 발생하였습니다. 다시 시도해 주세요. \n\n(같은 오류 메세지가 계속 보이면 아래 메일로 문의를 보내주시면 답변 드리겠습니다.)\njwisedom@naver.com", false);
            }

            progressBarLayout.setVisibility(View.GONE); // 로딩 레이아웃 숨기기
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content, final boolean isFinish) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
                if (isFinish) {
                    finishAffinity();
                }
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callSUHDialogType1Dialog2(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss(); // 팝업창 닫고
                progressBarLayout.setVisibility(View.VISIBLE); // 로딩 레이아웃 띄우고

                // 서버에 진료예약 취소 요청
                mobile3020();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void goToActivity(Class c) {
        Intent intent = new Intent(context, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        ((Activity) context).startActivity(intent);
        // Bungee.slideUp(context);
    }

    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                callSUHDialogType1Dialog("안내", "정말 서울대학교병원 앱을 종료하시겠습니까?", true);
            }
        }
    }

    // 다른 화면으로부터 이 페이지에 다시 왔을 때
    @Override
    protected void onPostResume() {
        super.onPostResume();
        // 정보 갱신하기

        Log.i("MyTags", "Main onPostResume 실행");
        mobile3001(); // 서버에 프로필정보 요청 (이름, 최근진료날짜, 총 예약횟수, 총 진단 횟수)
        synchronizationCRH(); // 진료예약내역 동기화
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
