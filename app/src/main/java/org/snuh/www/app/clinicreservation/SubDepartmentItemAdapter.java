package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;

import java.util.List;

public class SubDepartmentItemAdapter extends RecyclerView.Adapter {
    private Activity activity;

    private List<SubDepartmentItem> subDepartmentItems;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public final LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    /**************************************************************/

    public SubDepartmentItemAdapter(RecyclerView recyclerView, List<SubDepartmentItem> subDepartmentItems, final Activity activity, View.OnClickListener itemsOnClickListener) {
        this.subDepartmentItems = subDepartmentItems;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return subDepartmentItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.sub_department_item, parent, false);
            return new SubDepartmentItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SubDepartmentItemViewHolder) {
            SubDepartmentItem subDepartmentItem = subDepartmentItems.get(position);
            SubDepartmentItemViewHolder userViewHolder = (SubDepartmentItemViewHolder) holder;

            subDepartmentItem.setViewHolder(userViewHolder);

            userViewHolder.codeTextView.setText(subDepartmentItem.getCode()+"");
            userViewHolder.department.setText(subDepartmentItem.getDepartment());
            userViewHolder.departmentButton.setTag(subDepartmentItem);
            userViewHolder.departmentButton.setOnClickListener(itemsOnClickListener);

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return subDepartmentItems == null ? 0 : subDepartmentItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/

}
