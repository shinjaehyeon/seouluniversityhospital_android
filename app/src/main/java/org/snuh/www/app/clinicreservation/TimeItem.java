package org.snuh.www.app.clinicreservation;

public class TimeItem {
    public int year;
    public int month;
    public int date;

    public int hour;
    public int minute;
    public int already;

    public TimeItemViewHolder viewHolder;

    public void setYear(int n) {
        this.year = n;
    }
    public int getYear() {
        return this.year;
    }

    public void setMonth(int n) {
        this.month = n;
    }
    public int getMonth() {
        return this.month;
    }

    public void setDate(int n) {
        this.date = n;
    }
    public int getDate() {
        return this.date;
    }

    public void setHour(int n) {
        this.hour = n;
    }
    public int getHour() {
        return this.hour;
    }

    public void setMinute(int n) {
        this.minute = n;
    }
    public int getMinute() {
        return this.minute;
    }

    public void setAlready(int n) {
        this.already = n;
    }
    public int getAlready() {
        return this.already;
    }


    public void setViewHolder(TimeItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public TimeItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
