package org.snuh.www.app.hospitalnewsitemset;


import org.snuh.www.app.doctoritemset.DoctorItemViewHolder;

public class HospitalNewsItem {
    public int pk;
    public String title;
    public String content;
    public int viewIndex;
    public String datetime;
    public HospitalNewsItemViewHolder viewHolder;

    public void setPk(int n) {
        this.pk = n;
    }
    public int getPk() {
        return this.pk;
    }

    public void setTitle(String s) {
        this.title = s;
    }
    public String getTitle() {
        return this.title;
    }

    public void setContent(String s) {
        this.content = s;
    }
    public String getContent() {
        return this.content;
    }

    public void setViewIndex(int n) {
        this.viewIndex = n;
    }
    public int getViewIndex() {
        return this.viewIndex;
    }

    public void setDatetime(String s) {
        this.datetime = s;
    }
    public String getDatetime() {
        return this.datetime;
    }

    public void setViewHolder(HospitalNewsItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public HospitalNewsItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
