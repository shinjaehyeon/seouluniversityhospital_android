package org.snuh.www.app.common;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class UninstallIntentReceiver extends BroadcastReceiver {
    public static final String PACKAGE_NAME = "org.snuh.www.app";

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                default:

                    break;
            }
        }
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent != null){

            if(Intent.ACTION_PACKAGE_REMOVED.equals(intent.getAction())){
                Log.i("MyTags", "앱 삭제 이벤트 발동!!!");
                ContentValues params = new ContentValues();
                params.put("act", "mobile3523");
                params.put("member", new ShareInfo(context).getMember());

                InLet inLet = new InLet(context);
                inLet.setInLetAct("basic");
                inLet.setProtocolType("http");
                // inLet.setUrl("");
                inLet.setParams(params);
                inLet.setHandler(handler);
                inLet.setHandlerRequestNumber(3524);
                inLet.start();


                Uri uri = intent.getData();
                if(uri == null){
                    return;
                }

                if(PACKAGE_NAME.equals(uri.getSchemeSpecificPart())){
                    // App 초기화 및 재설치 관련 처리
                    Toast.makeText(context, "ACTION_PACKAGE_REMOVED", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

}

