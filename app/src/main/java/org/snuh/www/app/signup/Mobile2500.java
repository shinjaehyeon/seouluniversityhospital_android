package org.snuh.www.app.signup;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.login.Mobile2000;
import org.snuh.www.app.login.Mobile2000SnsLoginButton;

public class Mobile2500 extends AppCompatActivity {
    Activity activity;
    Context context;
    CustomToolbar topbar;

    /**************************************************************/

    FrameLayout fragment;
    FrameLayout fragment2;
    FrameLayout fragment3;

    Mobile2510 mobile2510;
    Mobile2520 mobile2520;
    Mobile2530 mobile2530;
    int currentPage = 1;

    /**************************************************************/

    Button negativeButton;
    Button positiveButton;

    /**************************************************************/

    RelativeLayout progressBarLayout;
    ProgressBar progressBar;

    /**************************************************************/

    String checkedName; // 체크된 이름
    int checkedGender; // 체크된 성별
    int checkedMobileCompanyNumber; // 체크된 통신사 번호
    String checkedPhoneNumber; // 체크된 휴대폰 번호
    String checkedEmail; // 체크된 이메일
    String checkedPassword; // 체크된 비밀번호
    String checkedBirthday; // 체크된 생년월일
    int checkedBloodTypeNumber; // 체크된 혈액형 번호
    String checkedAddress; // 체크된 주소

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 2525:
                    mobile2525(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile2500);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        setFragmentPage(); // 페이지 (프래그먼트) 셋팅하기
        showPage(1); // 첫페이지(STEP 01) 페이지 보여주기
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }






    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);

        fragment = (FrameLayout) findViewById(R.id.fragment);
        fragment2 = (FrameLayout) findViewById(R.id.fragment2);
        fragment3 = (FrameLayout) findViewById(R.id.fragment3);

        mobile2510 = new Mobile2510(activity);
        mobile2520 = new Mobile2520(activity);
        mobile2530 = new Mobile2530(activity);

        negativeButton = (Button) findViewById(R.id.negativeButton);
        positiveButton = (Button) findViewById(R.id.positiveButton);

        progressBarLayout = (RelativeLayout) findViewById(R.id.progressBarLayout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        negativeButton.setOnClickListener(negativeButtonClickEvent); // 취소 버튼 클릭했을 시
        positiveButton.setOnClickListener(positiveButtonClickEvent); // 다음 버튼 클릭했을 시
    }





    /**************************************************************/



    // 페이지 (프래그먼트) 셋팅하기
    public void setFragmentPage() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, mobile2510);
        fragmentTransaction.commit();

        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager.beginTransaction();
        fragmentTransaction2.replace(R.id.fragment2, mobile2520);
        fragmentTransaction2.commit();

        FragmentManager fragmentManager3 = getFragmentManager();
        FragmentTransaction fragmentTransaction3 = fragmentManager.beginTransaction();
        fragmentTransaction3.replace(R.id.fragment3, mobile2530);
        fragmentTransaction3.commit();
    }



    // 페이지 (프래그먼트) 보여주기
    public void showPage(int pageNum) {
        currentPage = pageNum;

        switch (pageNum) {
            case 1:
                fragment.setVisibility(View.VISIBLE);
                fragment2.setVisibility(View.GONE);
                fragment3.setVisibility(View.GONE);
                break;
            case 2:
                fragment.setVisibility(View.GONE);
                fragment2.setVisibility(View.VISIBLE);
                fragment3.setVisibility(View.GONE);
                break;
            case 3:
                fragment.setVisibility(View.GONE);
                fragment2.setVisibility(View.GONE);
                fragment3.setVisibility(View.VISIBLE);
                negativeButton.setVisibility(View.GONE);
                positiveButton.setText("로그인 하러 가기");
                break;
            default:

                break;
        }
    }





    /**************************************************************/


    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
    public void mobile2522() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile2522");
        params.put("member", new ShareInfo(this).getMember());
        params.put("checkedName", checkedName); // 이름
        params.put("checkedGender", checkedGender); // 성별
        params.put("checkedMobileCompanyNumber", checkedMobileCompanyNumber); // 통신사 번호
        params.put("checkedPhoneNumber", checkedPhoneNumber); // 휴대폰 번호
        params.put("checkedEmail", checkedEmail); // 이메일
        params.put("checkedPassword", checkedPassword); // 비밀번호
        params.put("checkedBirthday", checkedBirthday); // 생년월일
        params.put("checkedBloodTypeNumber", checkedBloodTypeNumber); // 혈액형 번호
        params.put("checkedAddress", checkedAddress); // 주소

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2525);
        inLet.start();
    }

    // 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청 후처리
    public void mobile2525(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                progressBarLayout.setVisibility(View.GONE); // 로딩 표시 없애기

                // 다음단계로 넘어가기
                showPage(3);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/



    // 취소 버튼 클릭했을 시
    View.OnClickListener negativeButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    // 다음 버튼 클릭했을 시
    View.OnClickListener positiveButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (currentPage) {
                case 1: // 현재 보여지는 페이지가 STEP 01 일 때
                    // 모든 약관 동의에 체크 했는지 확인하기
                    if (!mobile2510.isServiceTermsAcceptChecked) {
                        callSUHDialogType1Dialog("안내", "서비스 이용약관에 동의해 주세요.");
                    } else if (!mobile2510.isPersonalTermsAcceptChecked) {
                        callSUHDialogType1Dialog("안내", "개인정보취급방침에 동의해 주세요.");
                    } else {
                        showPage(2);
                    }
                    break;
                case 2: // 현재 보여지는 페이지가 STEP 02 일 때
                    if (mobile2520.mobile2521()) {
                        // 회원가입 입력 폼 체크가 모두 정상적일 경우

                        // 로딩 표시
                        progressBarLayout.setVisibility(View.VISIBLE);

                        // 입력받아야 할 정보가 모두 입력되었으면 DB에 회원정보 저장 요청
                        checkedName = mobile2520.checkedName;
                        checkedGender = mobile2520.checkedGender;
                        checkedMobileCompanyNumber = mobile2520.checkedMobileCompanyNumber;
                        checkedPhoneNumber = mobile2520.checkedPhoneNumber;
                        checkedEmail = mobile2520.checkedEmail;
                        checkedPassword = mobile2520.checkedPassword;
                        checkedBirthday = mobile2520.checkedBirthday;
                        checkedBloodTypeNumber = mobile2520.checkedBloodTypeNumber;
                        checkedAddress = mobile2520.checkedAddress;
                        mobile2522();
                    }
                    break;
                case 3: // 현재 보여지는 페이지가 STEP 03 일 때
                    finishAffinity();
                    Intent intent = new Intent(activity, Mobile2000.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
            }
        }
    };


}
