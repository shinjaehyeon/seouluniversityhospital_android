package org.snuh.www.app.prescriptiondrugitemset;

public class PrescriptionDrugItem {
    public PrescriptionDrugItemViewHolder viewHolder;

    public int primarykey; // 처방약 pk
    public int drugPrimaryKey; // 약품 pk
    public String drugName; // 약품 이름
    public float onedayDose; // 하루 투여량
    public float onedayNumber; // 하루 투여횟수
    public float doseDayNum; // 총 투약일수
    public String note; // 비고





    public void setViewHolder(PrescriptionDrugItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public PrescriptionDrugItemViewHolder getViewHolder() {
        return this.viewHolder;
    }


    public void setPrimarykey(int n) {
        this.primarykey = n;
    }
    public int getPrimarykey() {
        return this.primarykey;
    }



    public void setDrugPrimaryKey(int n) {
        this.drugPrimaryKey = n;
    }
    public int getDrugPrimaryKey() {
        return this.drugPrimaryKey;
    }

    public void setDrugName(String s) {
        this.drugName = s;
    }
    public String getDrugName() {
        return this.drugName;
    }




    public void setOnedayDose(float f) {
        this.onedayDose = f;
    }
    public float getOnedayDose() {
        return this.onedayDose;
    }

    public void setOnedayNumber(float f) {
        this.onedayNumber = f;
    }
    public float getOnedayNumber() {
        return this.onedayNumber;
    }

    public void setDoseDayNum(float f) {
        this.doseDayNum = f;
    }
    public float getDoseDayNum() {
        return this.doseDayNum;
    }



    public void setNote(String s) {
        this.note = s;
    }
    public String getNote() {
        return this.note;
    }
}
