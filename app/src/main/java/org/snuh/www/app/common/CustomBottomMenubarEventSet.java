package org.snuh.www.app.common;

import android.app.Activity;
import android.app.ActivityOptions;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AnimationUtils;

import org.snuh.www.app.R;

public class CustomBottomMenubarEventSet {
    Activity activity;
    CustomBottomMenubar customBottomMenubar;
    UseFul useFul;

    int measuredwidth;
    int measuredheight;
    int onlyBottomMenubarHeight;
    int visiblehiddenButtonHeight;
    int fullBottomMenubarHeight;


    int visibleEndY;
    int hiddenEndY;


    public CustomBottomMenubarEventSet() {

    }

    public CustomBottomMenubarEventSet(Activity activity, CustomBottomMenubar customBottomMenubar) {
        this.activity = activity;
        this.customBottomMenubar = customBottomMenubar;
        useFul = new UseFul(activity);
        initView();
    }


    public void initView() {
        getSizes();
        setObjectEvent();
    }

    public void getSizes() {
        DisplayMetrics metrics = new DisplayMetrics();   //for all android versions
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        measuredwidth  = metrics.widthPixels;
        measuredheight = metrics.heightPixels;
        onlyBottomMenubarHeight = useFul.getRdimen(R.dimen.custombottommenubar_only_height);
        visiblehiddenButtonHeight = useFul.getRdimen(R.dimen.custombottommenubar_visiblehidden_button_height);
        fullBottomMenubarHeight = useFul.getRdimen(R.dimen.custombottommenubar_height2);

        visibleEndY = -onlyBottomMenubarHeight;
        hiddenEndY = 0;
    }

    public void setObjectEvent() {
        customBottomMenubar.setTheDisplayAndHiddenButtonEvent(bottomMenubarVisibleHiddenButtonEvent);
    }

    View.OnClickListener bottomMenubarVisibleHiddenButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (customBottomMenubar.getIsVisible()) {
                // 하단 메뉴바가 보일 때
                // 숨기는 code...
                customBottomMenubar.animate().translationY(hiddenEndY).setDuration(CustomBottomMenubar.DURATION_VALUE).setInterpolator(
                        AnimationUtils.loadInterpolator(activity, CustomBottomMenubar.INTERPOLATOR_VALUE)
                ).start(); // move away
                customBottomMenubar.setVisibleButton();
                customBottomMenubar.setIsVisible(false);
            } else {
                // 하단 메뉴바가 안보일때
                // 보이는 code...
                customBottomMenubar.animate().translationY(visibleEndY).setDuration(CustomBottomMenubar.DURATION_VALUE).setInterpolator(
                        AnimationUtils.loadInterpolator(activity, CustomBottomMenubar.INTERPOLATOR_VALUE)
                ).start(); // move away
                customBottomMenubar.setHiddenButton();
                customBottomMenubar.setIsVisible(true);

            }
        }
    };
}
