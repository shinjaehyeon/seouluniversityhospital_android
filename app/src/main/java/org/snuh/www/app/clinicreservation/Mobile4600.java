package org.snuh.www.app.clinicreservation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile4600 extends Fragment {
    Activity activity;
    View rootView;
    UseFul useFul;

    /**************************************************************/

    NestedScrollView nestedScrollView;

    Mobile4620 mobile4620;
    Mobile4640 mobile4640;
    Mobile4660 mobile4660;

    ArrayList<FrameLayout> fragments = new ArrayList<FrameLayout>();

    int previousPosition = 900900;
    int currentPosition;

    /**************************************************************/

    ArrayList<Button> stepButton = new ArrayList<Button>();

    /**************************************************************/

    // STEP 01 에 해당하는 변수


    // STEP 02 에 해당하는 변수
    ArrayList<SubDoctorItem> subDoctorItems = new ArrayList<SubDoctorItem>();

    // STEP 03 에 해당하는 변수

    /**************************************************************/

    // STEP 01 에 해당하는 내용 저장
    int step01_department;
    String step01_department_string;
    String step01_symptom;

    // STEP 02 에 해당하는 내용 저장
    int step02_doctor_primarykey;
    String step02_doctor_name;

    // STEP 03 에 해당하는 내용 저장
    int step03_select_year;
    int step03_select_month;
    int step03_select_date;
    int step03_select_hour;
    int step03_select_minute;

    /**************************************************************/

    Button prevButton;
    Button nextButton;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 4602: // 서버에 입력받은 예약정보를 DB에 저장 요청 후처리
                    mobile4602(msg.obj);
                    break;
                case 4624: // 서버에 해당 진료과에 맞는 의료진 리스트 요청 후처리
                    mobile4624(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    public Mobile4600() {

    }

    @SuppressLint("ValidFragment")
    public Mobile4600(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile4600, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        setFragment(); // 프래그먼트 적용하기
        setViewPageFragment(0); // index 프래그먼트 표시하기 (나머지 다 숨김)

        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        nestedScrollView = (NestedScrollView) rootView.findViewById(R.id.nestedScrollView);
        nestedScrollView.setSmoothScrollingEnabled(true);

        mobile4620 = new Mobile4620(activity);
        mobile4620.setHandler(handler);
        mobile4640 = new Mobile4640(activity, nestedScrollView);
        mobile4640.setDoctorArrayList(subDoctorItems);
        mobile4660 = new Mobile4660(activity);
        mobile4660.setNestedScrollView(nestedScrollView);

        mobile4640.setMobile4620(mobile4620);
        mobile4640.setMobile4660(mobile4660);


        fragments.add((FrameLayout) rootView.findViewById(R.id.fragment_1));
        fragments.add((FrameLayout) rootView.findViewById(R.id.fragment_2));
        fragments.add((FrameLayout) rootView.findViewById(R.id.fragment_3));

        /**************************************************************/



        /**************************************************************/

        stepButton.add((Button) rootView.findViewById(R.id.step_01_button));
        stepButton.add((Button) rootView.findViewById(R.id.step_02_button));
        stepButton.add((Button) rootView.findViewById(R.id.step_03_button));

        /**************************************************************/



        /**************************************************************/

        prevButton = (Button) rootView.findViewById(R.id.prevButton);
        nextButton = (Button) rootView.findViewById(R.id.nextButton);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // STEP 버튼 클릭시 이벤트 연결
        for (int i=0; i<stepButton.size(); i++) {
            stepButton.get(i).setOnClickListener(stepButtonClickEvent);
        }

        /**************************************************************/

        // 이전, 다음단계 버튼 클릭시 이벤트 연결
        prevButton.setOnClickListener(thePrevNextButtonClickEvent);
        nextButton.setOnClickListener(thePrevNextButtonClickEvent);

        /**************************************************************/
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    /**************************************************************/

    // 프래그먼트 적용하기
    public void setFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_1, mobile4620);
        fragmentTransaction.commit();

        FragmentManager fragmentManager2 = getFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
        fragmentTransaction2.replace(R.id.fragment_2, mobile4640);
        fragmentTransaction2.commit();

        FragmentManager fragmentManager3 = getFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction3 = fragmentManager3.beginTransaction();
        fragmentTransaction3.replace(R.id.fragment_3, mobile4660);
        fragmentTransaction3.commit();
    }

    public void setViewPageFragment(int position) {
        currentPosition = position;

        fragments.get(position).setVisibility(View.VISIBLE);
        stepButton.get(position).setBackgroundResource(R.drawable.ripple_button_type_9);

        if (previousPosition != 900900) {
            fragments.get(previousPosition).setVisibility(View.GONE);
            stepButton.get(previousPosition).setBackgroundResource(R.drawable.ripple_button_type_10);
        }
        previousPosition = currentPosition;
    }

    /**************************************************************/

    // 서버에 해당 진료과에 맞는 의료진 리스트 요청 후처리
    public void mobile4624(Object obj) {
        String data = obj.toString();

        Log.i("FinalTags", "mobile4624 data = "+data);

        // STEP 02, 03 내용 및 입력 받은 값 초기화
        subDoctorItems.clear(); // 의료진 리스트 초기화
        clearSTEP02();
        clearSTEP03();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int doctor_pk = jsonObject1.getInt("doctor");
                    String department = jsonObject1.getString("department_string");
                    String name = jsonObject1.getString("name");

                    SubDoctorItem subDoctorItem = new SubDoctorItem();
                    subDoctorItem.setDoctorPrimaryKey(doctor_pk);
                    subDoctorItem.setDoctorDepartment(department);
                    subDoctorItem.setDoctorName(name);
                    subDoctorItems.add(subDoctorItem);
                }

                Log.i("FinalTags", "의료진 리스트 갱신 호출");
                mobile4640.setRefreshDoctorList(); // 의료진 리스트 갱신

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content, final boolean isFinish) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
                if (isFinish) {
                    activity.finish();
                }
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callSUHDialogType1Dialog3(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                activity.finish();
            }
        });
    }


    // 최종 예약 정보 확인 다이얼로그 창
    public void callSUHDialogType1Dialog2(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();

                mobile4601(); // 서버에 입력받은 예약정보를 DB에 저장 요청
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/

    // 서버에 입력받은 예약정보를 DB에 저장 요청
    public void mobile4601() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile4601");
        params.put("member", new ShareInfo(activity).getMember());

        params.put("step01_department", step01_department); // 진료과
        params.put("step01_symptom", step01_symptom); // 증상

        params.put("step02_doctor_primarykey", step02_doctor_primarykey); // 의료진

        params.put("step03_select_year", step03_select_year); // 년
        params.put("step03_select_month", useFul.attachZero(step03_select_month)); // 월
        params.put("step03_select_date", useFul.attachZero(step03_select_date)); // 일
        params.put("step03_select_hour", useFul.attachZero(step03_select_hour)); // 시
        params.put("step03_select_minute", useFul.attachZero(step03_select_minute)); // 분

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(4602);
        inLet.start();
    }

    // 서버에 입력받은 예약정보를 DB에 저장 요청 후처리
    public void mobile4602(Object obj) {
        String data = obj.toString();

        // Log.i("FinalMtags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("already")) {
                callSUHDialogType1Dialog3("안내", "이미 다른 사람이 해당 날짜/시간에 예약을 진행하였습니다. 다른 날짜 및 시간을 선택해주세요.");
            } else if (result.equals("ok")) {
                callSUHDialogType1Dialog3("안내", "진료예약이 접수되었습니다.");
            } else  {
                callSUHDialogType1Dialog3("안내", "진료예약 중에 오류가 발생하였습니다.");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/

    // 이전, 다음단계 버튼 클릭시 이벤트
    View.OnClickListener thePrevNextButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.prevButton: // 이전단계 버튼 클릭 시
                    switch (currentPosition) {
                        case 0: // 현재 보여지는 페이지가 STEP 01 - 진료과/증상 선택 페이지일 경우
                            useFul.showToast("이전 단계가 없습니다.");
                            break;
                        case 1: // 현재 보여지는 페이지가 STEP 02 - 의료진 선택 페이지일 경우
                            setViewPageFragment(0);
                            scrollTop();
                            break;
                        case 2: // 현재 보여지는 페이지가 STEP 03 - 날짜/시간 선택 페이지일 경우
                            setViewPageFragment(1);
                            scrollTop();
                            break;
                        default:

                            break;
                    }
                    break;
                case R.id.nextButton: // 다음단계 버튼 클릭 시
                    switch (currentPosition) {
                        case 0: // 현재 보여지는 페이지가 STEP 01 - 진료과/증상 선택 페이지일 경우
                            if (isSTEP01Checked()) {
                                setViewPageFragment(1);
                                scrollTop();
                            }
                            break;
                        case 1: // 현재 보여지는 페이지가 STEP 02 - 의료진 선택 페이지일 경우
                            if (isSTEP02Checked()) {
                                mobile4660.department = mobile4620.currentSelectedDepartmentCode;
                                mobile4660.doctor = mobile4640.currentSelectedDoctorPrimaryKey;
                                setViewPageFragment(2);
                                scrollTop();
                            }
                            break;
                        case 2: // 현재 보여지는 페이지가 STEP 03 - 날짜/시간 선택 페이지일 경우
                            if (isSTEP03Checked()) {
                                callSUHDialogType1Dialog2(
                                        "안내",
                                        step01_department_string+
                                                "\n"+
                                                step02_doctor_name+
                                                " 의료진 \n"+
                                                step03_select_year+
                                                "년 "+
                                                step03_select_month+
                                                "월 "+
                                                step03_select_date+
                                                "일 "+
                                                useFul.attachZero(step03_select_hour)+
                                                "시 "+
                                                useFul.attachZero(step03_select_minute)+
                                                "분 \n\n\n 위에 표시된 예약정보가 맞으시면 확인을 눌러주세요.");
                            }
                            break;
                        default:

                            break;
                    }
                    break;
                default:

                    break;
            }

        }
    };

    // STEP 버튼 클릭시 이벤트
    View.OnClickListener stepButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.step_01_button: // STEP 01 - 진료과/증상 선택 버튼 클릭 했을 때
                    switch (currentPosition) {
                        case 0: // 현재 보여지는 페이지가 STEP 01 - 진료과/증상 선택 페이지일 경우
                            // 아무것도 안함.
                            break;
                        case 1: // 현재 보여지는 페이지가 STEP 02 - 의료진 선택 페이지일 경우
                            setViewPageFragment(0);
                            scrollTop();
                            break;
                        case 2: // 현재 보여지는 페이지가 STEP 03 - 날짜/시간 선택 페이지일 경우
                            setViewPageFragment(0);
                            scrollTop();
                            break;
                        default:

                            break;
                    }
                    break;
                case R.id.step_02_button: // STEP 02 - 의료진 선택 버튼 클릭 했을 때
                    switch (currentPosition) {
                        case 0: // 현재 보여지는 페이지가 STEP 01 - 진료과/증상 선택 페이지일 경우
                            if (isSTEP01Checked()) {
                                setViewPageFragment(1);
                                scrollTop();
                            }
                            break;
                        case 1: // 현재 보여지는 페이지가 STEP 02 - 의료진 선택 페이지일 경우
                            // 아무것도 안함.
                            break;
                        case 2: // 현재 보여지는 페이지가 STEP 03 - 날짜/시간 선택 페이지일 경우
                            setViewPageFragment(1);
                            scrollTop();
                            break;
                        default:

                            break;
                    }
                    break;
                case R.id.step_03_button: // STEP 03 - 날짜/시간 선택 버튼 클릭 했을 때
                    switch (currentPosition) {
                        case 0: // 현재 보여지는 페이지가 STEP 01 - 진료과/증상 선택 페이지일 경우
                            if (isSTEP01Checked()) {
                                if (isSTEP02Checked()) {
                                    setViewPageFragment(2);
                                    scrollTop();
                                }
                            }
                            break;
                        case 1: // 현재 보여지는 페이지가 STEP 02 - 의료진 선택 페이지일 경우
                            if (isSTEP02Checked()) {
                                mobile4660.department = mobile4620.currentSelectedDepartmentCode;
                                mobile4660.doctor = mobile4640.currentSelectedDoctorPrimaryKey;
                                setViewPageFragment(2);
                                scrollTop();
                            }
                            break;
                        case 2: // 현재 보여지는 페이지가 STEP 03 - 날짜/시간 선택 페이지일 경우

                            break;
                        default:

                            break;
                    }
                    break;
                default:

                    break;
            }

        }
    };

    public void scrollTop() {
        nestedScrollView.post(new Runnable() {
            @Override
            public void run() {
                nestedScrollView.fullScroll(View.FOCUS_UP);
                nestedScrollView.scrollTo(0,0);
            }
        });
    }

    /**************************************************************/

    // STEP 01 - 진료과/증상 선택 단계에서 입력 받아야 할 정보를 모두 입력 받았는지 체크하기 (모두 다 입력 받았으면 ture 반환, 그렇지 않으면 false 반환)
    public boolean isSTEP01Checked() {
        // 진료과 체크
        if (mobile4620.currentSelectedDepartmentCode == 0) {
            callSUHDialogType1Dialog("안내", "진료과를 선택 해주세요.", false);
            return false;
        }

        step01_department = mobile4620.getCurrentSelectedDepartmentCode();
        step01_department_string = mobile4620.getCurrentSelectedDepartmentString();
        step01_symptom = mobile4620.getCurrentSymptomString();

        return true;
    }

    // STEP 02 - 의료진 선택 단계에서 입력 받아야 할 정보를 모두 입력 받았는지 체크하기 (모두 다 입력 받았으면 ture 반환, 그렇지 않으면 false 반환)
    public boolean isSTEP02Checked() {

        // 의료진 체크
        if (mobile4640.currentSelectedDoctorPrimaryKey == 0) {
            callSUHDialogType1Dialog("안내", "의료진을 선택 해주세요.", false);
            return false;
        }

        step02_doctor_primarykey = mobile4640.currentSelectedDoctorPrimaryKey;
        step02_doctor_name = mobile4640.currentSelectedDoctorName;

        return true;
    }

    // STEP 03 - 날짜/시간 선택 단계에서 입력 받아야 할 정보를 모두 입력 받았는지 체크하기 (모두 다 입력 받았으면 ture 반환, 그렇지 않으면 false 반환)
    public boolean isSTEP03Checked() {

        if (mobile4660.currentSelectedYear == 0) {
            callSUHDialogType1Dialog("안내", "날짜 혹은 시간을 선택 해주세요.", false);
            return false;
        }

        step03_select_year = mobile4660.currentSelectedYear;
        step03_select_month = mobile4660.currentSelectedMonth;
        step03_select_date = mobile4660.currentSelectedDate;
        step03_select_hour = mobile4660.currentSelectedHour;
        step03_select_minute = mobile4660.currentSelectedMinute;

        return true;
    }

    /**************************************************************/

    // STEP 02 - 의료진 선택에 선택되어 있던 값 모두 초기화
    public void clearSTEP02() {
        mobile4640.allClear();
    }

    // STEP 03 - 날짜/시간 선택에 선택되어 있던 값 모두 초기화
    public void clearSTEP03() {
        mobile4660.allClear();
    }
}
