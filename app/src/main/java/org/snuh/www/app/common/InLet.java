package org.snuh.www.app.common;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class InLet extends Thread {
    Context context;

    /**************************************************************/

    String inLetAct;
    String protocolType;
    private String url_string = "http://jwisedom.dothome.co.kr/seouluniversityhospital_mobile/outlet.php";
    ContentValues params;
    Handler handler;
    int handlerRequestNumber;

    /**************************************************************/



    /**************************************************************/

    // 필수 (생성자)
    public InLet(Context context) {
        this.context = context;
    }

    // 필수 (InLet의 act 타입 설정)
    public void setInLetAct(String inLetAct) {
        this.inLetAct = inLetAct;
    }

    // 필수 (http인지, https인지)
    public void setProtocolType(String protocolType) {
        this.protocolType = protocolType;
    }

    // 선택 (요청 보낼 주소 설정)
    public void setUrl(String url_string) {
        this.url_string = url_string;
    }

    // 선택 (인자값 설정)
    public void setParams(ContentValues params) {
        this.params = params;
    }

    // 선택 (handler 설정)
    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    // 선택 (handler 요청 번호 설정)
    public void setHandlerRequestNumber(int handlerRequestNumber) {
        this.handlerRequestNumber = handlerRequestNumber;
    }

    /**************************************************************/

    @Override
    public void run() {
        super.run();

        switch (inLetAct) {
            case "basic":
                if (protocolType.equals("http")) {
                    sendHttpServerRequire();
                } else if (protocolType.equals("https")) {
                    sendHttpsServerRequire();
                }
                break;
            case "base64ImageGet":
                if (protocolType.equals("http")) {
                    sendHttpServerBase64ImageRequire();
                } else if (protocolType.equals("https")) {
                    sendHttpsServerBase64ImageRequire();
                }
                break;
            default:

                break;
        }
    }

    /**************************************************************/

    public void sendHttpServerRequire() {
        // HttpURLConnection 참조 변수.
        HttpURLConnection urlConn = null;
        // URL 뒤에 붙여서 보낼 파라미터.
        StringBuffer sbParams = new StringBuffer();


        // 보낼 데이터가 없으면 파라미터를 비운다.
        if (params == null) {
            sbParams.append("");
            // 보낼 데이터가 있으면 파라미터를 채운다.
        } else {
            // 파라미터가 2개 이상이면 파라미터 연결에 &가 필요하므로 스위칭할 변수 생성.
            boolean isAnd = false;
            // 파라미터 키와 값.
            String key;
            String value;

            for (Map.Entry<String, Object> parameter : params.valueSet()) {
                key = parameter.getKey();
                value = parameter.getValue().toString();

                // 파라미터가 두개 이상일때, 파라미터 사이에 &를 붙인다.
                if (isAnd)
                    sbParams.append("&");

                sbParams.append(key).append("=").append(value);

                // 파라미터가 2개 이상이면 isAnd를 true로 바꾸고 다음 루프부터 &를 붙인다.
                if (!isAnd) {
                    if (params.size() >= 2) {
                        isAnd = true;
                    }
                }
            }
        }

        /**
         * 2. HttpURLConnection을 통해 web의 데이터를 가져온다.
         * */

        try {
            URL url = new URL(url_string);
            urlConn = (HttpURLConnection) url.openConnection();

            // [2-1]. urlConn 설정.
            urlConn.setRequestMethod("POST"); // URL 요청에 대한 메소드 설정 : POST.
            urlConn.setRequestProperty("Accept-Charset", "UTF-8"); // Accept-Charset 설정.
            urlConn.setRequestProperty("Context_Type", "application/x-www-form-urlencoded;cahrset=UTF-8");

            // [2-2]. parameter 전달 및 데이터 읽어오기.
            String strParams = sbParams.toString(); //sbParams에 정리한 파라미터들을 스트링으로 저장. 예)id=id1&pw=123;
            OutputStream os = urlConn.getOutputStream();
            os.write(strParams.getBytes("UTF-8")); // 출력 스트림에 출력.
            os.flush(); // 출력 스트림을 플러시(비운다)하고 버퍼링 된 모든 출력 바이트를 강제 실행.
            os.close(); // 출력 스트림을 닫고 모든 시스템 자원을 해제.

            // [2-3]. 연결 요청 확인.
            // 실패 시 null을 리턴하고 메서드를 종료.
            if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // [2-4]. 읽어온 결과물 리턴.
                // 요청한 URL의 출력물을 BufferedReader로 받는다.
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "UTF-8"));

                // 출력물의 라인과 그 합에 대한 변수.
                String line;
                String responseStringBuilder = "";

                // 라인을 받아와 합친다.
                while ((line = reader.readLine()) != null){
                    responseStringBuilder += line;
                }

                Message msg = handler.obtainMessage();
                msg.obj = responseStringBuilder.toString();
                msg.what = handlerRequestNumber;

                handler.sendMessage(msg);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendHttpsServerRequire() {
        HttpsURLConnection urlConn = null;
        StringBuffer sbParams = new StringBuffer();


        if (params == null)
            sbParams.append("");
            // 보낼 데이터가 있으면 파라미터를 채운다.
        else {
            // 파라미터가 2개 이상이면 파라미터 연결에 &가 필요하므로 스위칭할 변수 생성.
            boolean isAnd = false;
            // 파라미터 키와 값.
            String key;
            String value;

            for (Map.Entry<String, Object> parameter : params.valueSet()) {
                key = parameter.getKey();
                value = parameter.getValue().toString();

                // 파라미터가 두개 이상일때, 파라미터 사이에 &를 붙인다.
                if (isAnd) {
                    sbParams.append("&");
                }
                sbParams.append(key).append("=").append(value);

                // 파라미터가 2개 이상이면 isAnd를 true로 바꾸고 다음 루프부터 &를 붙인다.
                if (!isAnd) {
                    if (params.size() >= 2) {
                        isAnd = true;
                    }
                }
            }
        }

        /**
         * 2. HttpURLConnection을 통해 web의 데이터를 가져온다.
         * */

        try {
            URL url = new URL(url_string);

            trustAllHosts();

            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });

            HttpsURLConnection connection = httpsURLConnection;

            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String strParams = sbParams.toString();
            OutputStream outputStream = connection.getOutputStream();
            //BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            // bufferedWriter.write(String.valueOf(strParams.getBytes("UTF-8")));
            // bufferedWriter.flush();
            // bufferedWriter.close();
            outputStream.write(strParams.getBytes("UTF-8"));
            outputStream.flush();
            outputStream.close();

            connection.connect();


            StringBuilder responseStringBuilder = new StringBuilder();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                for (; ; ) {
                    String stringLine = bufferedReader.readLine();
                    if (stringLine == null) break;
                    responseStringBuilder.append(stringLine + '\n');
                }
                bufferedReader.close();
            }
            connection.disconnect();

            // Log.d("MyTags", "결과 = "+responseStringBuilder.toString());

            Message msg2 = handler.obtainMessage();
            msg2.obj = responseStringBuilder.toString();
            msg2.what = handlerRequestNumber;

            handler.sendMessage(msg2);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void sendHttpServerBase64ImageRequire() {
        // HttpURLConnection 참조 변수.
        HttpURLConnection urlConn = null;
        // URL 뒤에 붙여서 보낼 파라미터.
        StringBuffer sbParams = new StringBuffer();


        // 보낼 데이터가 없으면 파라미터를 비운다.
        if (params == null) {
            sbParams.append("");
            // 보낼 데이터가 있으면 파라미터를 채운다.
        } else {
            // 파라미터가 2개 이상이면 파라미터 연결에 &가 필요하므로 스위칭할 변수 생성.
            boolean isAnd = false;
            // 파라미터 키와 값.
            String key;
            String value;

            for (Map.Entry<String, Object> parameter : params.valueSet()) {
                key = parameter.getKey();
                value = parameter.getValue().toString();

                // 파라미터가 두개 이상일때, 파라미터 사이에 &를 붙인다.
                if (isAnd)
                    sbParams.append("&");

                sbParams.append(key).append("=").append(value);

                // 파라미터가 2개 이상이면 isAnd를 true로 바꾸고 다음 루프부터 &를 붙인다.
                if (!isAnd) {
                    if (params.size() >= 2) {
                        isAnd = true;
                    }
                }
            }
        }

        /**
         * 2. HttpURLConnection을 통해 web의 데이터를 가져온다.
         * */

        try {
            URL url = new URL(url_string);
            urlConn = (HttpURLConnection) url.openConnection();

            // [2-1]. urlConn 설정.
            urlConn.setRequestMethod("POST"); // URL 요청에 대한 메소드 설정 : POST.
            urlConn.setRequestProperty("Accept-Charset", "UTF-8"); // Accept-Charset 설정.
            urlConn.setRequestProperty("Context_Type", "application/x-www-form-urlencoded;cahrset=UTF-8");

            // [2-2]. parameter 전달 및 데이터 읽어오기.
            String strParams = sbParams.toString(); //sbParams에 정리한 파라미터들을 스트링으로 저장. 예)id=id1&pw=123;
            OutputStream os = urlConn.getOutputStream();
            os.write(strParams.getBytes("UTF-8")); // 출력 스트림에 출력.
            os.flush(); // 출력 스트림을 플러시(비운다)하고 버퍼링 된 모든 출력 바이트를 강제 실행.
            os.close(); // 출력 스트림을 닫고 모든 시스템 자원을 해제.

            // [2-3]. 연결 요청 확인.
            // 실패 시 null을 리턴하고 메서드를 종료.
            if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // [2-4]. 읽어온 결과물 리턴.
                // 요청한 URL의 출력물을 BufferedReader로 받는다.
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "UTF-8"));

                // 출력물의 라인과 그 합에 대한 변수.
                String line;
                String responseStringBuilder = "";

                // 라인을 받아와 합친다.
                while ((line = reader.readLine()) != null){
                    responseStringBuilder += line;
                }

                Message msg = handler.obtainMessage();
                msg.obj = responseStringBuilder.toString();
                msg.what = handlerRequestNumber;

                handler.sendMessage(msg);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendHttpsServerBase64ImageRequire() {
        HttpsURLConnection urlConn = null;
        StringBuffer sbParams = new StringBuffer();


        if (params == null)
            sbParams.append("");
            // 보낼 데이터가 있으면 파라미터를 채운다.
        else {
            // 파라미터가 2개 이상이면 파라미터 연결에 &가 필요하므로 스위칭할 변수 생성.
            boolean isAnd = false;
            // 파라미터 키와 값.
            String key;
            String value;

            for (Map.Entry<String, Object> parameter : params.valueSet()) {
                key = parameter.getKey();
                value = parameter.getValue().toString();

                // 파라미터가 두개 이상일때, 파라미터 사이에 &를 붙인다.
                if (isAnd) {
                    sbParams.append("&");
                }
                sbParams.append(key).append("=").append(value);

                // 파라미터가 2개 이상이면 isAnd를 true로 바꾸고 다음 루프부터 &를 붙인다.
                if (!isAnd) {
                    if (params.size() >= 2) {
                        isAnd = true;
                    }
                }
            }
        }

        /**
         * 2. HttpURLConnection을 통해 web의 데이터를 가져온다.
         * */

        try {
            URL url = new URL(url_string);

            trustAllHosts();

            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });

            HttpsURLConnection connection = httpsURLConnection;

            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            String strParams = sbParams.toString();
            OutputStream outputStream = connection.getOutputStream();
            //BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            // bufferedWriter.write(String.valueOf(strParams.getBytes("UTF-8")));
            // bufferedWriter.flush();
            // bufferedWriter.close();
            outputStream.write(strParams.getBytes("UTF-8"));
            outputStream.flush();
            outputStream.close();

            connection.connect();


            StringBuilder responseStringBuilder = new StringBuilder();
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                for (; ; ) {
                    String stringLine = bufferedReader.readLine();
                    if (stringLine == null) break;
                    responseStringBuilder.append(stringLine + '\n');
                }
                bufferedReader.close();
            }
            connection.disconnect();

            // Log.d("MyTags", "결과 = "+responseStringBuilder.toString());

            Message msg2 = handler.obtainMessage();
            msg2.obj = responseStringBuilder.toString();
            msg2.what = handlerRequestNumber;

            handler.sendMessage(msg2);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private static void trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }

            @Override
            public void checkClientTrusted(
                    java.security.cert.X509Certificate[] chain,
                    String authType)
                    throws java.security.cert.CertificateException {
                // TODO Auto-generated method stub

            }

            @Override
            public void checkServerTrusted(
                    java.security.cert.X509Certificate[] chain,
                    String authType)
                    throws java.security.cert.CertificateException {
                // TODO Auto-generated method stub

            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection
                    .setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
