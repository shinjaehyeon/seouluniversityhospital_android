package org.snuh.www.app.noticeitemset;

import android.graphics.Color;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.w3c.dom.Text;

public class NoticeItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public LinearLayout parent_layout;

    public Button button;
    public RelativeLayout passed_day_textview_parentlayout;
    public TextView passed_day_textview;
    public TextView datetime_textview;
    public TextView datetime_time_textview;
    public TextView title_textview;
    public TextView content_textview;

    public ImageView right_icon;

    /**************************************************************/

    public NoticeItemViewHolder(View view) {
        super(view);
        this.view = view;

        parent_layout = (LinearLayout) view.findViewById(R.id.parent_layout);

        button = (Button) view.findViewById(R.id.button);

        passed_day_textview_parentlayout = (RelativeLayout) view.findViewById(R.id.passed_day_textview_parentlayout);
        passed_day_textview = (TextView) view.findViewById(R.id.passed_day_textview);
        datetime_textview = (TextView) view.findViewById(R.id.datetime_textview);
        datetime_time_textview = (TextView) view.findViewById(R.id.datetime_time_textview);
        title_textview = (TextView) view.findViewById(R.id.title_textview);
        content_textview = (TextView) view.findViewById(R.id.content_textview);

        right_icon = (ImageView) view.findViewById(R.id.right_icon);

    }


    public void setUnActiveEffect() {
        button.setBackgroundResource(R.drawable.ripple_button_type_10);
        passed_day_textview.setTextColor(Color.parseColor("#a7a7a7"));
        passed_day_textview_parentlayout.setBackgroundResource(R.drawable.border_rounded_4);

        datetime_textview.setTextColor(Color.parseColor("#b5b5b5"));
        datetime_time_textview.setTextColor(Color.parseColor("#b5b5b5"));

        title_textview.setTextColor(Color.parseColor("#999999"));
        content_textview.setTextColor(Color.parseColor("#b0afaf"));

        right_icon.setImageResource(R.drawable.ic_mobile0000_right_arrow_icon_4);
    }

    public void setActiveEffect() {
        button.setBackgroundResource(R.drawable.ripple_button_type_9);
        passed_day_textview.setTextColor(Color.parseColor("#008ace"));
        passed_day_textview_parentlayout.setBackgroundResource(R.drawable.border_rounded_3);

        datetime_textview.setTextColor(Color.parseColor("#888888"));
        datetime_time_textview.setTextColor(Color.parseColor("#888888"));

        title_textview.setTextColor(Color.parseColor("#444444"));
        content_textview.setTextColor(Color.parseColor("#888888"));

        right_icon.setImageResource(R.drawable.ic_mobile0000_right_arrow_icon_3);
    }

}
