package org.snuh.www.app.firebase;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FCMInstanceIDListenerService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIDService";

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 33:

                    break;
                default:

                    break;
            }
        }
    };

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + token);
    }

    public String getToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        return token;
    }
}
