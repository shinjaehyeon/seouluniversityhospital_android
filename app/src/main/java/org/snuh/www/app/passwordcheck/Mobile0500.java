package org.snuh.www.app.passwordcheck;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.splash.Mobile1000;

public class Mobile0500 extends AppCompatActivity {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;

    ConstraintLayout password_input_window;
    EditText password_edittext;
    Button password_commit_button;
    String original_password;
    int password_flag = 0;

    int linkPageNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile0500);
        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        original_password = new ShareInfo(activity).getPassword();
        password_flag = new ShareInfo(activity).getPsswordSettingFlag();

        if (password_flag == 0) {
            Intent intent = new Intent(activity, Mobile1000.class);
            intent.putExtra("linkPageNumber", linkPageNumber);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        } else {
            password_input_window.setVisibility(View.VISIBLE);
        }

        password_commit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputedPassword = password_edittext.getText().toString();

                if (inputedPassword.equals(original_password)) {
                    Intent intent = new Intent(activity, Mobile1000.class);
                    intent.putExtra("linkPageNumber", linkPageNumber);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    useFul.showToast("패스워드가 일치하지 않습니다.");
                }
            }
        });
    }

    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        linkPageNumber = getIntent().getIntExtra("linkPageNumber", 0);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        password_input_window = (ConstraintLayout) findViewById(R.id.password_input_window);
        password_edittext = (EditText) findViewById(R.id.password_edittext);
        password_commit_button = (Button) findViewById(R.id.password_commit_button);
    }
}
