package org.snuh.www.app.clinicreservation;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SubDepartmentItem {
    public int code;
    public String department;
    public SubDepartmentItemViewHolder viewHolder;

    public void setViewHolder(SubDepartmentItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public SubDepartmentItemViewHolder getViewHolder() {
        return this.viewHolder;
    }

    public void setCode(int n) {
        this.code = n;
    }
    public int getCode() {
        return this.code;
    }

    public void setDepartment(String s) {
        this.department = s;
    }
    public String getDepartment() {
        return this.department;
    }
}
