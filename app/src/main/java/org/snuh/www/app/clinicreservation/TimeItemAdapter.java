package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.UseFul;

import java.util.List;

public class TimeItemAdapter extends RecyclerView.Adapter { 
    private Activity activity;

    private List<TimeItem> timeItems;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public final LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    /**************************************************************/

    public TimeItemAdapter(RecyclerView recyclerView, List<TimeItem> timeItems, final Activity activity, View.OnClickListener itemsOnClickListener) {
        this.timeItems = timeItems;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return timeItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.time_item, parent, false);
            return new TimeItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TimeItemViewHolder) {
            TimeItem timeItem = timeItems.get(position);
            TimeItemViewHolder userViewHolder = (TimeItemViewHolder) holder;

            timeItem.setViewHolder(userViewHolder);

            if (timeItem.getAlready() == 0) {
                userViewHolder.time_button.setTag(timeItem);
                userViewHolder.time_button.setOnClickListener(itemsOnClickListener);
                userViewHolder.time_button.setBackgroundResource(R.drawable.ripple_button_type_6);
            } else {
                userViewHolder.time_button.setOnClickListener(null);
                userViewHolder.time_button.setBackgroundResource(R.drawable.border_2);
            }
            String zero1 = "";
            if (timeItem.getHour() == 0) {
                zero1 = "0";
            }
            String zero2 = "";
            if (timeItem.getMinute() == 0) {
                zero2 = "0";
            }
            userViewHolder.time_textview.setText(zero1+timeItem.getHour()+":"+timeItem.getMinute()+zero2);

            if (position % 2 == 0) {
                userViewHolder.setMargin(activity);
            }


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return timeItems == null ? 0 : timeItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/

}
