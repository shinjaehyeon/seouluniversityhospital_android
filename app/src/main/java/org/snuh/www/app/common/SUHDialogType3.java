package org.snuh.www.app.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class SUHDialogType3 extends Dialog {
    Context context;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    Button select1;
    Button select2;
    Button select3;
    Button select4;

    int currentSelectNumber;

    /**************************************************************/

    public SUHDialogType3(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.suh_dialog_type_3);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        select1 = (Button) findViewById(R.id.select1);
        select2 = (Button) findViewById(R.id.select2);
        select3 = (Button) findViewById(R.id.select3);
        select4 = (Button) findViewById(R.id.select4);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        select1.setOnClickListener(bloodTypeButtonClickEvent);
        select2.setOnClickListener(bloodTypeButtonClickEvent);
        select3.setOnClickListener(bloodTypeButtonClickEvent);
        select4.setOnClickListener(bloodTypeButtonClickEvent);
    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public int getCurrentSelectedMobileCompanyNumber() {
        return this.currentSelectNumber;
    }

    public void setParentSelectedNumber(int n) {
        currentSelectNumber = n;
        switch (n) {
            case 1:
                select1.setBackgroundResource(R.drawable.ripple_button_type_2);
                select1.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 2:
                select2.setBackgroundResource(R.drawable.ripple_button_type_2);
                select2.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 3:
                select3.setBackgroundResource(R.drawable.ripple_button_type_2);
                select3.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 4:
                select4.setBackgroundResource(R.drawable.ripple_button_type_2);
                select4.setTextColor(Color.parseColor("#ffffff"));
                break;
            default:

                break;
        }
    }

    /**************************************************************/

    View.OnClickListener bloodTypeButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.select1:
                    currentSelectNumber = 1;
                    dismiss();
                    break;
                case R.id.select2:
                    currentSelectNumber = 2;
                    dismiss();
                    break;
                case R.id.select3:
                    currentSelectNumber = 3;
                    dismiss();
                    break;
                case R.id.select4:
                    currentSelectNumber = 4;
                    dismiss();
                    break;
                default:

                    break;
            }
        }
    };
}
