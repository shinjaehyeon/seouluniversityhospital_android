package org.snuh.www.app.departmentdoctor;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.signup.Mobile2520;

public class Mobile5000ViewPager extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;
    Mobile5000TabButton tabButton;

    ViewPager viewPager;

    Mobile5020 mobile5020;
    Mobile5040 mobile5040;

    /**************************************************************/

    public Mobile5000ViewPager(@NonNull Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        this.useFul = new UseFul(activity);


    }

    public Mobile5000ViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        this.useFul = new UseFul(activity);


    }

    /**************************************************************/

    int intent_page_index;
    public void getIntentInfo() {
        intent_page_index = activity.getIntent().getIntExtra("pageIndex", 0);
    }



    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5000_viewpager, this, false);
        addView(v);

        getIntentInfo();

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setViewPager(); // viewpager 셋팅하기

    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        viewPager = (ViewPager) v.findViewById(R.id.viewPager);

        mobile5020 = new Mobile5020(activity);
        mobile5040 = new Mobile5040(activity);

    }

    // viewpager 셋팅하기
    public void setViewPager() {
        viewPager.setAdapter(new pagerAdapter(activity.getSupportFragmentManager()));
        viewPager.setCurrentItem(intent_page_index);
        viewPager.setOffscreenPageLimit(2);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /**************************************************************/

    // 부모가 갖고 있는 탭 버튼 받아오기
    public void setTabButton(Mobile5000TabButton tabButton) {
        this.tabButton = tabButton;
    }

    /**************************************************************/


    private class pagerAdapter extends FragmentStatePagerAdapter {
        public pagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);

            tabButton.tabButtonEffect(position);
        }


        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return mobile5020;
                case 1:
                    return mobile5040;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
