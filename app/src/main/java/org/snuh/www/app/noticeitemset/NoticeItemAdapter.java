package org.snuh.www.app.noticeitemset;

import android.app.Activity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;

import java.util.List;

public class NoticeItemAdapter extends RecyclerView.Adapter {
    private Activity activity;

    private List<NoticeItem> items;
    private NestedScrollView scrollviews;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;
    public String getMoreInfoType = "scroll"; // or button

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public NoticeItemAdapter(RecyclerView recyclerView, List<NoticeItem> items, final Activity activity, View.OnClickListener itemsOnClickListener, NestedScrollView scrollview) {
        this.items = items;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;
        this.scrollviews = scrollview;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.notice_item, parent, false);
            return new NoticeItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NoticeItemViewHolder) {
            NoticeItem item = items.get(position);
            NoticeItemViewHolder userViewHolder = (NoticeItemViewHolder) holder;

            // 버튼에 리스너 연결해주고 태그 달기
            item.setViewHolder(userViewHolder);
            userViewHolder.button.setTag(item);
            userViewHolder.button.setOnClickListener(itemsOnClickListener);

            // 버튼 높이 부모 높이에 맞추기
            final LinearLayout parent_layout = (LinearLayout) userViewHolder.parent_layout;
            final Button button = (Button) userViewHolder.button;
            parent_layout.post(new Runnable() {
                @Override
                public void run() {
                    int height = parent_layout.getHeight();
                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) button.getLayoutParams();
                    lp.height = height;
                    button.setLayoutParams(lp);
                }
            });

            // 몇 일, 몇 시간, 몇 분 전인지 여부 판단해서 보여주기
            String passDateTime = "";
            if (item.getPassedDate() == 0 && item.getPassedHour() == 0) {
                passDateTime = item.getPassedMinute()+"분 전";
            } else if (item.getPassedDate() == 0) {
                passDateTime = item.getPassedHour()+"시간 전";
            } else {
                passDateTime = item.getPassedDate()+"일 전";
            }
            userViewHolder.passed_day_textview.setText(passDateTime);

            userViewHolder.datetime_textview.setText(item.getGetDate());
            userViewHolder.datetime_time_textview.setText(item.getGetTime()+" 수신");
            userViewHolder.title_textview.setText(item.getTitle());
            userViewHolder.content_textview.setText(item.getContent());


            // 푸쉬가 읽은 상태이면 비활성화 하기
            if (item.getStatus() == 600201) {
                userViewHolder.setUnActiveEffect();
            } else {
                userViewHolder.setActiveEffect();
            }

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/
}
