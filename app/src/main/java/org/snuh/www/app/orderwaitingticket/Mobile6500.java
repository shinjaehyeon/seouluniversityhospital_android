package org.snuh.www.app.orderwaitingticket;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.MainActivity;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomSelectDialog;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile6500 extends AppCompatActivity {
    private Messenger mServiceMessenger = null;
    private boolean mIsBound;

    /**************************************************************/

    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    /**************************************************************/

    String[] menuStrings = {"서울대학교병원 <본원> 접수창구", "서울대학교병원 <어린이병원> 접수창구", "서울대학교병원 <암병원> 접수창구"};
    int[] menuIntegers = {700201, 700202, 700203};
    int currentIndex = 0;
    int prevIndex = 0;

    Button register_counter_select_button;
    CustomSelectDialog customSelectDialog;
    TextView register_counter_display_textview;



    ArrayList<TextView> hospital_type_my_numbers = new ArrayList<TextView>();





    ArrayList<TextView> hospital700201_counters = new ArrayList<TextView>();
    ArrayList<TextView> hospital700202_counters = new ArrayList<TextView>();
    ArrayList<TextView> hospital700203_counters = new ArrayList<TextView>();




    Button ticket_cancel_button;
    Button ticket_get_button;

    RelativeLayout progressBarLayout;

    /**************************************************************/


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
//                case 6506: // 서버에 현재 대기중인 내 번호표가 있는지 여부 요청 후처리
//                    mobile6506(msg.obj);
//                    break;
                case 6508: // 서버에 순번대기표 뽑기 요청 후처리
                    mobile6508(msg.obj);
                    break;
                case 6510: // 서버에 순번대기표 취소 요청 후처리
                    mobile6510(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    /**************************************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile6500);



        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기




        setStartService(); // 서비스 시작
        sendMessageToService("from main"); // 메시지를 서비스로 전달
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }









    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);


        register_counter_select_button = (Button) findViewById(R.id.register_counter_select_button);
        customSelectDialog = new CustomSelectDialog(activity);
        register_counter_display_textview = (TextView) findViewById(R.id.register_counter_display_textview);


        hospital_type_my_numbers.add((TextView) findViewById(R.id.hospital_type_1_my_numbers));
        hospital_type_my_numbers.add((TextView) findViewById(R.id.hospital_type_2_my_numbers));
        hospital_type_my_numbers.add((TextView) findViewById(R.id.hospital_type_3_my_numbers));




        hospital700201_counters.add((TextView) findViewById(R.id.hospital_type_1_counter_1_current_numbers));
        hospital700201_counters.add((TextView) findViewById(R.id.hospital_type_1_counter_2_current_numbers));
        hospital700201_counters.add((TextView) findViewById(R.id.hospital_type_1_counter_3_current_numbers));

        hospital700202_counters.add((TextView) findViewById(R.id.hospital_type_2_counter_1_current_numbers));
        hospital700202_counters.add((TextView) findViewById(R.id.hospital_type_2_counter_2_current_numbers));
        hospital700202_counters.add((TextView) findViewById(R.id.hospital_type_2_counter_3_current_numbers));

        hospital700203_counters.add((TextView) findViewById(R.id.hospital_type_3_counter_1_current_numbers));
        hospital700203_counters.add((TextView) findViewById(R.id.hospital_type_3_counter_2_current_numbers));
        hospital700203_counters.add((TextView) findViewById(R.id.hospital_type_3_counter_3_current_numbers));


        ticket_cancel_button = (Button) findViewById(R.id.ticket_cancel_button);
        ticket_get_button = (Button) findViewById(R.id.ticket_get_button);


        progressBarLayout = (RelativeLayout) findViewById(R.id.progressBarLayout);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        customSelectDialog.setSelectionList(menuStrings, menuIntegers);
        customSelectDialog.setFirstValue(0);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        register_counter_select_button.setOnClickListener(registerCounterSelectButtonEvent);

        ticket_cancel_button.setOnClickListener(ticketButtonEvent);
        ticket_get_button.setOnClickListener(ticketButtonEvent);
    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    // 접수 창구 선택 박스 클릭 시 이벤트
    View.OnClickListener registerCounterSelectButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCustomSelectDialog("창구 선택");
        }
    };

    // 번호표 취소/뽑기 버튼 이벤트
    View.OnClickListener ticketButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ticket_cancel_button: // 번호표 취소 버튼 클릭시 이벤트
                    // 정말 취소할 건지 팝업창 띄우기
                    callReallyTicKetCancelDialog("경고", "정말 현재 대기중인 순번대기표를 취소하시겠습니까?");
                    break;
                case R.id.ticket_get_button: // 번호표 뽑기 버튼 클릭시 이벤트
                    showLoadingDisplay();
                    mobile6507(); // 서버에 순번대기표 뽑기 요청

                    break;
                default:

                    break;
            }
        }
    };

    public void callCustomSelectDialog(String title) {
        customSelectDialog.show();
        customSelectDialog.setTitle(title);
        customSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customSelectDialog.dismiss();
            }
        });
        customSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (customSelectDialog.isOnItemClicked) {
                    currentIndex = customSelectDialog.getCurrentIndex();

                    register_counter_display_textview.setText(menuStrings[currentIndex]);
                    showCounter(currentIndex);


                    customSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    // 해당 인덱스의 창구 보여주기
    public void showCounter(int n) {
        switch (n) {
            case 0:
                hospital700201_counters.get(0).setVisibility(View.VISIBLE);
                hospital700201_counters.get(1).setVisibility(View.VISIBLE);
                hospital700201_counters.get(2).setVisibility(View.VISIBLE);
                break;
            case 1:
                hospital700202_counters.get(0).setVisibility(View.VISIBLE);
                hospital700202_counters.get(1).setVisibility(View.VISIBLE);
                hospital700202_counters.get(2).setVisibility(View.VISIBLE);
                break;
            case 2:
                hospital700203_counters.get(0).setVisibility(View.VISIBLE);
                hospital700203_counters.get(1).setVisibility(View.VISIBLE);
                hospital700203_counters.get(2).setVisibility(View.VISIBLE);
                break;
            default:

                break;
        }
        hospital_type_my_numbers.get(n).setVisibility(View.VISIBLE);

        switch (prevIndex) {
            case 0:
                hospital700201_counters.get(0).setVisibility(View.GONE);
                hospital700201_counters.get(1).setVisibility(View.GONE);
                hospital700201_counters.get(2).setVisibility(View.GONE);
                break;
            case 1:
                hospital700202_counters.get(0).setVisibility(View.GONE);
                hospital700202_counters.get(1).setVisibility(View.GONE);
                hospital700202_counters.get(2).setVisibility(View.GONE);
                break;
            case 2:
                hospital700203_counters.get(0).setVisibility(View.GONE);
                hospital700203_counters.get(1).setVisibility(View.GONE);
                hospital700203_counters.get(2).setVisibility(View.GONE);
                break;
            default:

                break;
        }
        hospital_type_my_numbers.get(prevIndex).setVisibility(View.GONE);

        prevIndex = n;
    }


    //    // 서버에 현재 대기중인 내 번호표가 있는지 여부 요청
//    public void mobile6505() {
//        ContentValues params = new ContentValues();
//        params.put("act", "mobile6505");
//        params.put("member", new ShareInfo(this).getMember());
//        InLet inLet = new InLet(activity);
//        inLet.setInLetAct("basic");
//        inLet.setProtocolType("http");
//        // inLet.setUrl("");
//        inLet.setParams(params);
//        inLet.setHandler(handler);
//        inLet.setHandlerRequestNumber(6506);
//        inLet.start();
//    }
//
//    // 서버에 현재 대기중인 내 번호표가 있는지 여부 요청 후처리
//    public void mobile6506(Object obj) {
//        hideLoadingDisplay();
//
//        String data = obj.toString();
//        try {
//            JSONObject jsonObject = new JSONObject(data);
//            String result = jsonObject.getString("result");
//
//            if (result.equals("ok")) {
//                // 현재 내가 뽑아놓은 순번대기표가 있을 때
//
//                // 정말 취소할 건지 팝업창 띄우기
//                callReallyTicKetCancelDialog("경고", "정말 현재 대기중인 순번대기표를 취소하시겠습니까?");
//
//            } else {
//                // 현재 내가 뽑아놓은 순번대기표가 없을 때
//
//                // 없다는 팝업창 띄우기
//                callSUHDialogType1Dialog("안내", "현재 대기중인 순번대기표가 없습니다.");
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//    }


    // 서버에 순번대기표 뽑기 요청
    public void mobile6507() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile6507");
        params.put("member", new ShareInfo(this).getMember());
        params.put("hospital_type", menuIntegers[currentIndex]);

        Log.i("MyTags", "순번대기표 뽑기 요청");
        Log.i("MyTags", "hospital_type = "+menuIntegers[currentIndex]);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(6508);
        inLet.start();
    }

    // 서버에 순번대기표 뽑기 요청 후처리
    public void mobile6508(Object obj) {
        hideLoadingDisplay();

        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("already")) {
                callSUHDialogType1Dialog("안내", "이미 유효한 순번대기표가 존재합니다.");
            } else if (result.equals("ok")) {
                callSUHDialogType1Dialog("안내", "순번대기표가 정상적으로 생성되었습니다.");
            } else {
                callSUHDialogType1Dialog("안내", "순번대기표 생성중 오류가 발생하였습니다. 다시 시도해주세요.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    // 서버에 순번대기표 취소 요청
    public void mobile6509() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile6509");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(6510);
        inLet.start();
    }

    // 서버에 순번대기표 취소 요청 후처리
    public void mobile6510(Object obj) {
        hideLoadingDisplay();

        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                callSUHDialogType1Dialog("안내", "순번대기표가 취소되었습니다.");
            } else {
                callSUHDialogType1Dialog("안내", "순번대기표가 취소중에 오류가 발생하였습니다. 다시 시도해주세요.");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }





    public void callReallyTicKetCancelDialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();

                showLoadingDisplay();
                mobile6509(); // 서버에 순번대기표 취소 요청
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }


    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }







    /**************************************************************/

    /** 서비스 시작 및 Messenger 전달 */
    private void setStartService() {
//        startService(new Intent(this, ReceptionTicketService.class));
        bindService(new Intent(this, ReceptionTicketService.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    /** 서비스 정지 */
    private void setStopService() {
        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
        stopService(new Intent(this, ReceptionTicketService.class));
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d("test","onServiceConnected");
            mServiceMessenger = new Messenger(iBinder);
            try {
                Message msg = Message.obtain(null, 6500);
                msg.replyTo = mMessenger;
                msg.obj = "아이고 연결됬어요 서비스님!";
                Log.i("MyTags", "아이고 연결됬어요 서비스님! 보냄.");
                mServiceMessenger.send(msg);
            }
            catch (RemoteException e) {
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };

    /** Service 로 부터 message를 받음 */
    private final Messenger mMessenger = new Messenger(new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.i("test","act : what "+msg.what);
            switch (msg.what) {
                case 6500:
                    int h_700201_counter_1_number = msg.getData().getInt("h_700201_counter_1_number");
                    int h_700201_counter_2_number = msg.getData().getInt("h_700201_counter_2_number");
                    int h_700201_counter_3_number = msg.getData().getInt("h_700201_counter_3_number");

                    int h_700202_counter_1_number = msg.getData().getInt("h_700202_counter_1_number");
                    int h_700202_counter_2_number = msg.getData().getInt("h_700202_counter_2_number");
                    int h_700202_counter_3_number = msg.getData().getInt("h_700202_counter_3_number");

                    int h_700203_counter_1_number = msg.getData().getInt("h_700203_counter_1_number");
                    int h_700203_counter_2_number = msg.getData().getInt("h_700203_counter_2_number");
                    int h_700203_counter_3_number = msg.getData().getInt("h_700203_counter_3_number");

                    int my_number_h_type = msg.getData().getInt("my_number_h_type");
                    int my_number = msg.getData().getInt("my_number");
                    int my_number_status = msg.getData().getInt("my_number_status");


                    if (h_700201_counter_1_number == 0) {
                        hospital700201_counters.get(0).setText("-");
                    } else {
                        hospital700201_counters.get(0).setText(h_700201_counter_1_number+"");
                    }
                    if (h_700201_counter_2_number == 0) {
                        hospital700201_counters.get(1).setText("-");
                    } else {
                        hospital700201_counters.get(1).setText(h_700201_counter_2_number+"");
                    }
                    if (h_700201_counter_3_number == 0) {
                        hospital700201_counters.get(2).setText("-");
                    } else {
                        hospital700201_counters.get(2).setText(h_700201_counter_3_number+"");
                    }

                    if (h_700202_counter_1_number == 0) {
                        hospital700202_counters.get(0).setText("-");
                    } else {
                        hospital700202_counters.get(0).setText(h_700202_counter_1_number+"");
                    }
                    if (h_700202_counter_2_number == 0) {
                        hospital700202_counters.get(1).setText("-");
                    } else {
                        hospital700202_counters.get(1).setText(h_700202_counter_2_number+"");
                    }
                    if (h_700202_counter_3_number == 0) {
                        hospital700202_counters.get(2).setText("-");
                    } else {
                        hospital700202_counters.get(2).setText(h_700202_counter_3_number+"");
                    }

                    if (h_700203_counter_1_number == 0) {
                        hospital700203_counters.get(0).setText("-");
                    } else {
                        hospital700203_counters.get(0).setText(h_700203_counter_1_number+"");
                    }
                    if (h_700203_counter_2_number == 0) {
                        hospital700203_counters.get(1).setText("-");
                    } else {
                        hospital700203_counters.get(1).setText(h_700203_counter_2_number+"");
                    }
                    if (h_700203_counter_3_number == 0) {
                        hospital700203_counters.get(2).setText("-");
                    } else {
                        hospital700203_counters.get(2).setText(h_700203_counter_3_number+"");
                    }

                    switch (my_number_h_type) {
                        case 700201: // 본원 접수창구
                            hospital_type_my_numbers.get(0).setText(my_number+"");
                            hospital_type_my_numbers.get(1).setText("-");
                            hospital_type_my_numbers.get(2).setText("-");
                            break;
                        case 700202: // 어린이병원 접수창구
                            hospital_type_my_numbers.get(0).setText("-");
                            hospital_type_my_numbers.get(1).setText(my_number+"");
                            hospital_type_my_numbers.get(2).setText("-");
                            break;
                        case 700203: // 본원 접수창구
                            hospital_type_my_numbers.get(0).setText("-");
                            hospital_type_my_numbers.get(1).setText("-");
                            hospital_type_my_numbers.get(2).setText(my_number+"");
                            break;
                        default:
                            hospital_type_my_numbers.get(0).setText("-");
                            hospital_type_my_numbers.get(1).setText("-");
                            hospital_type_my_numbers.get(2).setText("-");
                            break;
                    }


                    break;
            }
            return false;
        }
    }));

    /** Service 로 메시지를 보냄 */
    private void sendMessageToService(String str) {
        if (mIsBound) {
            if (mServiceMessenger != null) {
                try {
                    Message msg = Message.obtain(null, 6500);
                    msg.replyTo = mMessenger;
                    msg.obj = str;
                    mServiceMessenger.send(msg);
                } catch (RemoteException e) {

                }
            }
        }
    }

    /**************************************************************/







    public void showLoadingDisplay() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    public void hideLoadingDisplay() {
        progressBarLayout.setVisibility(View.GONE);
    }







    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                finish();
            }
        }
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.i("MyTags113344", "onStop() 호출됨");
        setStopService();
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
        setStartService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setStopService();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
