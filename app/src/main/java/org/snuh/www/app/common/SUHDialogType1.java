package org.snuh.www.app.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.signup.Mobile2510;
import org.snuh.www.app.signup.Mobile2520;
import org.snuh.www.app.signup.Mobile2530;

public class SUHDialogType1 extends Dialog {
    Context context;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    TextView content;

    Button negativeButton;
    Button positiveButton;

    /**************************************************************/

    public SUHDialogType1(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.suh_dialog_type_1);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        content = (TextView) findViewById(R.id.content);

        negativeButton = (Button) findViewById(R.id.negativeButton);
        positiveButton = (Button) findViewById(R.id.positiveButton);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public void setContent(String s) {
        content.setText(s);
    }

    public void setNegativeButtonOnClickListener(View.OnClickListener e) {
        negativeButton.setOnClickListener(e);
    }

    public void setPositiveButtonOnClickListener(View.OnClickListener e) {
        positiveButton.setOnClickListener(e);
    }

    public void blindNegativeButton() {
        negativeButton.setVisibility(View.INVISIBLE);
    }
}
