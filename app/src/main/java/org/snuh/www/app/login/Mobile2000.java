package org.snuh.www.app.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Share;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kakao.auth.ErrorCode;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.ui.view.OAuthLoginButton;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.main.Mobile3000;
import org.snuh.www.app.signup.Mobile2500;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HandshakeCompletedListener;

public class Mobile2000 extends AppCompatActivity {
    Activity activity;
    Context context;

    /**************************************************************/

    // SNS 로그인 버튼
    Mobile2000SnsLoginButton facebookLoginButton;
    Mobile2000SnsLoginButton kakaotalkLoginButton;
    Mobile2000SnsLoginButton naverLoginButton;

    private CallbackManager callbackManager;
    com.facebook.login.widget.LoginButton facebookLoginButtonReal;

    // 아이디, 비밀번호 EditText
    EditText idEdittext;
    EditText pwEdittext;

    // 아이디/비밀번호 찾기 버튼
    Button findIdPwButton;

    // 회원가입, 로그인 버튼
    Button joinButton;
    Button loginButton;

    /**************************************************************/

    // 로딩 레이아웃
    RelativeLayout progressBarLayout;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 2011: // 페이스북로그인 계정 DB에 있는지 확인 요청 결과값 받기
                    mobile2011(msg.obj);
                    break;
                case 2021: // 카카오톡로그인 계정 DB에 있는지 확인 요청 결과값 받기
                    mobile2021(msg.obj);
                    break;
                case 2031: // 네이버로그인 계정 DB에 있는지 확인 요청 결과값 받기
                    mobile2031(msg.obj);
                    break;
                case 2041: // 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 결과값 받기
                    mobile2041(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile2000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        setFacebookLogin(); // 페이스북 로그인 셋팅
        setKakaoLogin(); // 카카오 로그인 셋팅
        setNaverLogin(); // 네이버 로그인 셋팅
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }









    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        // SNS 로그인 버튼
        facebookLoginButton = (Mobile2000SnsLoginButton) findViewById(R.id.facebookLoginButton);
        kakaotalkLoginButton = (Mobile2000SnsLoginButton) findViewById(R.id.kakaotalkLoginButton);
        naverLoginButton = (Mobile2000SnsLoginButton) findViewById(R.id.naverLoginButton);

        facebookLoginButtonReal = (com.facebook.login.widget.LoginButton) findViewById(R.id.facebookLoginButtonReal);

        // 아이디, 비밀번호 EditText
        idEdittext = (EditText) findViewById(R.id.idEdittext);
        pwEdittext = (EditText) findViewById(R.id.pwEdittext);

        // 아이디/비밀번호 찾기 버튼
        findIdPwButton = (Button) findViewById(R.id.findIdPwButton);

        // 회원가입, 로그인 버튼
        joinButton = (Button) findViewById(R.id.joinButton);
        loginButton = (Button) findViewById(R.id.loginButton);

        // 로딩 레이아웃
        progressBarLayout = (RelativeLayout) findViewById(R.id.progressBarLayout);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // SNS 로그인 버튼 클릭 이벤트 설정
        facebookLoginButton.setButtonEvent(snsButtonEventObj);
        facebookLoginButton.setLoginButtonTag(1);
        kakaotalkLoginButton.setButtonEvent(snsButtonEventObj);
        kakaotalkLoginButton.setLoginButtonTag(2);
        naverLoginButton.setButtonEvent(snsButtonEventObj);
        naverLoginButton.setLoginButtonTag(3);

        // 아이디/비밀번호 찾기 버튼 클릭 이벤트 설정
        findIdPwButton.setOnClickListener(findIdPwButtonClickEvent);

        // 회원가입, 로그인 버튼 클릭 이벤트 설정
        joinButton.setOnClickListener(joinLoginButtonClickEvent);
        loginButton.setOnClickListener(joinLoginButtonClickEvent);
    }

    // 페이스북으로 로그인 셋팅
    public void setFacebookLogin() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();
        // facebookLoginButtonReal.setReadPermissions("email");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("MyTags","onSucces LoginResult="+loginResult);
                String id = Profile.getCurrentProfile().getId();
                String name = Profile.getCurrentProfile().getName();

                Log.d("MyTags","id = "+id+", name = "+name);

                mobile2010(id, name, 500802);
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("MyTags","onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("MyTags","onError = "+exception.toString());
            }
        });



    }

    // 카카오 로그인 셋팅
    SessionCallback callback;
    com.kakao.usermgmt.LoginButton kakaoLoginButton;

    public void setKakaoLogin() {
        callback = new SessionCallback();
        Session.getCurrentSession().addCallback(callback);
        kakaoLoginButton = (com.kakao.usermgmt.LoginButton) findViewById(R.id.com_kakao_login);


    }

    // 카카오 로그인 구현 부분
    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            Log.i("MyTags", "kakao 로그인 onSessionOpened 실행됨..!");
            UserManagement.getInstance().me(new MeV2ResponseCallback() {
                @Override
                public void onFailure(ErrorResult errorResult) {
                    super.onFailure(errorResult);
                    Log.i("MyTags", "kakao 로그인 onFailure 실행됨..!");
                }

                @Override
                public void onFailureForUiThread(ErrorResult errorResult) {
                    super.onFailureForUiThread(errorResult);
                    Log.i("MyTags", "kakao 로그인 onFailureForUiThread 실행됨..!");
                }

                @Override
                public void onDidStart() {
                    super.onDidStart();
                    Log.i("MyTags", "kakao 로그인 onDidStart 실행됨..!");
                }

                @Override
                public void onDidEnd() {
                    super.onDidEnd();
                    Log.i("MyTags", "kakao 로그인 onDidEnd 실행됨..!");
                }

                @Override
                public void onSuccessForUiThread(MeV2Response result) {
                    super.onSuccessForUiThread(result);
                    Log.i("MyTags", "onSuccessForUiThread 로그인 onDidEnd 실행됨..!");
                    Log.i("MyTags", "result.getId() = "+result.getId());
                    Log.i("MyTags", "result.getNickname() = "+result.getNickname());

                    String kakaoUserPk = result.getId()+"";
                    String kakaoUserName = result.getNickname()+"";

                    mobile2020(kakaoUserPk, kakaoUserName, 500803);
                }

                @Override
                public void onSuccess(MeV2Response result) {
                    Log.i("MyTags", "kakao 로그인 onSuccess(MeV2Response result) 실행됨..!");

                }

                @Override
                public void onSessionClosed(ErrorResult errorResult) {
                    Log.i("MyTags", "onSessionClosed 로그인 onDidEnd 실행됨..!");
                }
            });

        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            Log.i("MyTags", "kakao 로그인 onSessionOpenFailed 실행됨..!");
            Log.i("MyTags", exception.toString());
        }
    }

    // 카카오톡로그인 계정 DB에 있는지 확인 요청 보내기
    public void mobile2020(String id, String name, int signUpMethod) {
        String token = FirebaseInstanceId.getInstance().getToken();

        ContentValues params = new ContentValues();
        params.put("act", "mobile2020");
        params.put("member", new ShareInfo(this).getMember());
        params.put("id", id);
        params.put("name", name);
        params.put("signUpMethod", signUpMethod);
        params.put("pushkey", token);


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2021);
        inLet.start();
    }

    // 카카오톡로그인 계정 DB에 있는지 확인 요청 결과값 받기
    public void mobile2021(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);

            int member_pk = jsonObject.getInt("member_pk");
            new ShareInfo(activity).setInputIntValue("member", member_pk);
            new ShareInfo(activity).setInputIntValue("member_signupmethod", 500803);

            // 메인페이지로 이동
            goToMain();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    com.nhn.android.naverlogin.ui.view.OAuthLoginButton mOAuthLoginButton;
    private static final String TAG = "OAuthSampleActivity";

    /**
     * client 정보를 넣어준다.
     */
    private static String OAUTH_CLIENT_ID = "lsYfS68jHtTTeX1n6XOP";
    private static String OAUTH_CLIENT_SECRET = "jXwQCR3sq2";
    private static String OAUTH_CLIENT_NAME = "네이버 아이디로 로그인";

    private static OAuthLogin mOAuthLoginInstance;
    private static Context mContext;

    /**
     * UI 요소들
     */
    private TextView mApiResultText;
    private static TextView mOauthAT;
    private static TextView mOauthRT;
    private static TextView mOauthExpires;
    private static TextView mOauthTokenType;
    private static TextView mOAuthState;

    public void setNaverLogin() {
        mOAuthLoginButton = (OAuthLoginButton) findViewById(R.id.buttonOAuthLoginImg);
        mOAuthLoginButton.setOAuthLoginHandler(mOAuthLoginHandler);
        mContext = this;

        initData();


    }

    private void initData() {
        mOAuthLoginInstance = OAuthLogin.getInstance();

        mOAuthLoginInstance.showDevelopersLog(true);
        mOAuthLoginInstance.init(mContext, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET, OAUTH_CLIENT_NAME);
    }

    @Override
    protected void onResume() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }

    /**
     * startOAuthLoginActivity() 호출시 인자로 넘기거나, OAuthLoginButton 에 등록해주면 인증이 종료되는 걸 알 수 있다.
     */

    String kakao_access_token;
    private OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run (boolean success) {
            if (success) {
                String accessToken = mOAuthLoginInstance.getAccessToken(mContext);
                String refreshToken = mOAuthLoginInstance.getRefreshToken(mContext);
                long expiresAt = mOAuthLoginInstance.getExpiresAt(mContext);
                String tokenType = mOAuthLoginInstance.getTokenType(mContext);
                //mOauthAT.setText(accessToken);
                //mOauthRT.setText(refreshToken);
                //mOauthExpires.setText(String.valueOf(expiresAt));
                //mOauthTokenType.setText(tokenType);
                //mOAuthState.setText(mOAuthLoginInstance.getState(mContext).toString());

                kakao_access_token = accessToken;
                Log.i("MyTags", "accessToken = "+accessToken);
                Log.i("MyTags", "refreshToken = "+refreshToken);
                Log.i("MyTags", "expiresAt = "+expiresAt);
                Log.i("MyTags", "tokenType = "+tokenType);


                // mobile2034(tokenType, accessToken);
                new RequestApiTask(accessToken).execute();


            } else {
                String errorCode = mOAuthLoginInstance.getLastErrorCode(mContext).getCode();
                String errorDesc = mOAuthLoginInstance.getLastErrorDesc(mContext);
                Toast.makeText(mContext, "errorCode:" + errorCode + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
            }
        }

    };

    private class RequestApiTask extends AsyncTask {
        private String token;

        RequestApiTask(String token) {
            this.token = token;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // 로그인 처리가 완료되면 수행할 로직 작성
            processAuthResult((StringBuffer) o);

        }

        @Override
        protected Object doInBackground(Object[] objects) {
            String header = "Bearer " + token;
            try {
                final String apiURL = "https://openapi.naver.com/v1/nid/me";
                URL url = new URL(apiURL);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Authorization", header);
                int responseCode = con.getResponseCode();

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        responseCode == 200 ? con.getInputStream() : con.getErrorStream()));

                String inputLine;
                StringBuffer response = new StringBuffer();
                while((inputLine = br.readLine()) != null) {
                    response.append(inputLine);
                }

                br.close();
                return response;

            } catch(Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private void processAuthResult(StringBuffer response) {
        Log.i("MyTags", "response = "+response);

        try {
            // response는 json encoded된 상태이기 때문에 json 형식으로 decode 해줘야 한다.
            JSONObject object = new JSONObject(response.toString());
            JSONObject innerJson = new JSONObject(object.get("response").toString());

            String id = innerJson.getString("id");
            String email = innerJson.getString("email");
            String name = innerJson.getString("name");
            // String profileImgUrl = innerJson.getString("profile_image");
            Log.i("MyTags", "email = "+email);
            Log.i("MyTags", "name = "+name);

            mobile2030(id, name, 500804); // 네이버로그인 계정 DB에 있는지 확인 요청 보내기
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    // 네이버로그인 계정 DB에 있는지 확인 요청 보내기
    public void mobile2030(String id, String name, int signUpMethod) {
        String token = FirebaseInstanceId.getInstance().getToken();

        ContentValues params = new ContentValues();
        params.put("act", "mobile2030");
        params.put("member", new ShareInfo(this).getMember());
        params.put("id", id);
        params.put("name", name);
        params.put("signUpMethod", signUpMethod);
        params.put("pushkey", token);


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2031);
        inLet.start();
    }

    // 네이버로그인 계정 DB에 있는지 확인 요청 결과값 받기
    public void mobile2031(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);

            int member_pk = jsonObject.getInt("member_pk");
            new ShareInfo(activity).setInputIntValue("member", member_pk);
            new ShareInfo(activity).setInputIntValue("member_signupmethod", 500804);
            new ShareInfo(activity).setInputIntValue("member_access_token", kakao_access_token);

            // 메인페이지로 이동
            goToMain();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }










    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/

    // 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 보내기
    public void mobile2040(String id, String pw) {
        String token = FirebaseInstanceId.getInstance().getToken();

        ContentValues params = new ContentValues();
        params.put("act", "mobile2040");
        params.put("member", new ShareInfo(this).getMember());
        params.put("id", id);
        params.put("pw", pw);
        params.put("pushkey", token);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2041);
        inLet.start();
    }

    // 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 결과값 받기
    public void mobile2041(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            progressBarLayout.setVisibility(View.GONE);

            if (result.equals("ok")) {
                // 일치하는 계정이 있으면 member pk 저장하고 메인으로 이동
                int suhmember = jsonObject.getInt("suhmember");
                new ShareInfo(activity).setInputIntValue("member", suhmember);
                new ShareInfo(activity).setInputIntValue("member_signupmethod", 500801);

                // 메인페이지로 이동
                goToMain();

            } else {
                // 일치하는 계정이 없으면 없다는 다이얼로그 창 띄우기
                callSUHDialogType1Dialog("안내", "일치하는 계정이 없습니다.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    // 페이스북로그인 계정 DB에 있는지 확인 요청 보내기
    public void mobile2010(String id, String name, int signUpMethod) {
        String token = FirebaseInstanceId.getInstance().getToken();

        ContentValues params = new ContentValues();
        params.put("act", "mobile2010");
        params.put("member", new ShareInfo(this).getMember());
        params.put("id", id);
        params.put("name", name);
        params.put("signUpMethod", signUpMethod);
        params.put("pushkey", token);


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2011);
        inLet.start();
    }

    // 페이스북로그인 계정 DB에 있는지 확인 요청 결과값 받기
    public void mobile2011(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);

            int member_pk = jsonObject.getInt("member_pk");
            new ShareInfo(activity).setInputIntValue("member", member_pk);
            new ShareInfo(activity).setInputIntValue("member_signupmethod", 500802);

            // 메인페이지로 이동
            goToMain();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

















    // 메인 페이지로 이동하기
    public void goToMain() {
        Intent intent = new Intent(activity, Mobile3000.class);
        startActivity(intent);
        finish();
    }

    /**************************************************************/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**************************************************************/

    // SNS 로그인 버튼 클릭 이벤트
    View.OnClickListener snsButtonEventObj = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch ((int) v.getTag()) {
                case 1:
                    facebookLoginButtonReal.callOnClick();
                    break;
                case 2:
                    kakaoLoginButton.callOnClick();
                    // new UseFul(activity).showToast("카카오 로그인 버튼 클릭");
                    break;
                case 3:
                    // new UseFul(activity).showToast("네이버 로그인 버튼 클릭");
                    mOAuthLoginButton.callOnClick();
                    break;
                default:

                    break;
            }
        }
    };

    // 아이디/비밀번호 찾기 버튼 클릭 이벤트
    View.OnClickListener findIdPwButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new UseFul(activity).showToast("아이디/비밀번호 찾기 버튼 클릭");
        }
    };

    // 회원가입, 로그인 버튼 클릭 이벤트
    View.OnClickListener joinLoginButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.joinButton: // 회원가입
                    Intent intent = new Intent(activity, Mobile2500.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    break;
                case R.id.loginButton: // 로그인
                    progressBarLayout.setVisibility(View.VISIBLE);

                    String id = idEdittext.getText().toString();
                    String pw = pwEdittext.getText().toString();

                    mobile2040(id, pw); // 내부 계정 아이디, 비밀번호 값을 통해 DB에 있는지 확인 요청 보내기

                    break;
                default:

                    break;
            }
        }
    };
}
