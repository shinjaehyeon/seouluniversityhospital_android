package org.snuh.www.app.questionitemset;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

public class QuestionAnswerDialog extends Dialog {
    Context context;
    UseFul useFul;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    /**************************************************************/

    TextView question_type_textview;
    TextView question_title;
    TextView question_datetime;
    TextView question_content;
    TextView answer_title;
    TextView answer_datetime;
    TextView answer_content;


    String questionType;
    String questionTitle;
    String questionDatetime;
    String questionContent;
    String answerTitle;
    String answerDatetime;
    String answerContent;

    /**************************************************************/


    public QuestionAnswerDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.question_answer_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

    }




    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(context);


        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);


        question_type_textview = (TextView) findViewById(R.id.question_type_textview);
        question_title = (TextView) findViewById(R.id.question_title);
        question_datetime = (TextView) findViewById(R.id.question_datetime);
        question_content = (TextView) findViewById(R.id.question_content);
        answer_title = (TextView) findViewById(R.id.answer_title);
        answer_datetime = (TextView) findViewById(R.id.answer_datetime);
        answer_content = (TextView) findViewById(R.id.answer_content);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public void setQuestionType(String s) {
        this.questionType = s;
        // question_type_textview.setText(s);
    }
    public void setQuestionTitle(String s) {
        this.questionTitle = s;
        // question_title.setText(s);
    }
    public void setQuestionDatetime(String s) {
        this.questionDatetime = s;
        // question_datetime.setText(s);
    }
    public void setQuestionContent(String s) {
        this.questionContent = s;
        // question_content.setText(s);
    }

    public void setAnswerTitle(String s) {
        this.answerTitle = s;
        // answer_title.setText(s);
    }
    public void setAnswerDatetime(String s) {
        this.answerDatetime = s;
        // answer_datetime.setText(s);
    }
    public void setAnswerContent(String s) {
        this.answerContent = s;
        // answer_content.setText(s);
    }


    /**************************************************************/

    public void setApplyContents() {
        question_type_textview.setText(questionType);
        question_title.setText(questionTitle);
        question_datetime.setText(questionDatetime);
        question_content.setText(questionContent);

        answer_title.setText(answerTitle);
        answer_datetime.setText(answerDatetime);
        answer_content.setText(answerContent);
    }

    /**************************************************************/


}
