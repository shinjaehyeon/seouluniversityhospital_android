package org.snuh.www.app.common;

/**
 * Created by shinjaehyeon on 2018-02-10.
 */

public interface OnLoadMoreListener {
     void onLoadMore();
}
