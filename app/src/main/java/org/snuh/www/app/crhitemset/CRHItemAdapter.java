package org.snuh.www.app.crhitemset;

import android.app.Activity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.UseFul;

import java.util.List;

public class CRHItemAdapter extends RecyclerView.Adapter {
    private Activity activity;

    private List<CRHItem> crhItems;
    private NestedScrollView scrollviews;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public final LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;
    public String getMoreInfoType = "scroll"; // or button

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    /**************************************************************/

    public CRHItemAdapter(RecyclerView recyclerView, List<CRHItem> crhItems, final Activity activity, View.OnClickListener itemsOnClickListener, NestedScrollView scrollview) {
        this.crhItems = crhItems;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;
        this.scrollviews = scrollview;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return crhItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.clinic_reservation_list_item, parent, false);
            return new CRHItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CRHItemViewHolder) {
            CRHItem crhItem = crhItems.get(position);
            CRHItemViewHolder userViewHolder = (CRHItemViewHolder) holder;

            // userViewHolder.view.setOnClickListener(itemsOnClickListener);
            crhItem.setViewHolder(userViewHolder); // 뷰를 Item에 저장. 나중에 이 Item을 태그로 붙이는 용도.

            userViewHolder.doctor.setText(crhItem.getDoctorName());
            userViewHolder.department.setText(crhItem.getDeparment());
            userViewHolder.reservation_date.setText(crhItem.getClinicReservationDate());
            userViewHolder.reservation_time.setText(crhItem.getClinicReservationTime());
            userViewHolder.current_status.setText(crhItem.getStatusString());

            if (crhItem.getStatus() != 500201) {
                // 예약중이 아니면 (진료완료, 취소됨, 미방문 중 하나라면)

                // 글씨 색 회색으로 바꾸기
                userViewHolder.current_status.setTextColor(new UseFul(activity).getRcolor(R.color.clinic_reservation_list_current_status_text_off_color));

                // 취소 버튼 없애기
                userViewHolder.cancel_button.setVisibility(View.GONE);
            } else {
                // 예약중이면

                // 글씨 색 파랑색으로 바꾸기
                userViewHolder.current_status.setTextColor(new UseFul(activity).getRcolor(R.color.clinic_reservation_list_current_status_text_on_color));

                // 취소 버튼 보이기
                userViewHolder.cancel_button.setVisibility(View.VISIBLE);
                // 취소버튼에 item 태그로 달고 이벤트 리스너 연결하기
                userViewHolder.cancel_button.setTag(crhItem);
                userViewHolder.cancel_button.setOnClickListener(crhItem.getCancelButtonClickEvent());
            }


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return crhItems == null ? 0 : crhItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/

}
