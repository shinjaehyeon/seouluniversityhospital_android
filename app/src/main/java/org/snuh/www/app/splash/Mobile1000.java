package org.snuh.www.app.splash;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdoctor.Mobile5000;
import org.snuh.www.app.hospitalnews.Mobile8000;
import org.snuh.www.app.login.Mobile2000;
import org.snuh.www.app.main.Mobile3000;
import org.snuh.www.app.permission.Mobile1500;
import org.snuh.www.app.profileinfoupdate.Mobile3500;

public class Mobile1000 extends AppCompatActivity {
    Activity activity;
    Context context;

    /**************************************************************/

    private static final int REQUEST_PERMISSION_CODE = 1;
    private static String[] PERMISSION_ALL = {
            Manifest.permission.INTERNET,
            Manifest.permission.CHANGE_WIFI_MULTICAST_STATE,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.VIBRATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static String[] PERMISSION_ALL_CHECK = {
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.WRITE_CALL_LOG,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 1:
                    mobile1002(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile1000);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기


        getIntentInfo();
        new UseFul(activity).setAppRunning(1);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mobile1001(); // 앱 구동시 필요한 데이터 요청하기
            }
        }, 1000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }







    // 인텐트 정보 가져오기
    int linkPageNumber;
    public void getIntentInfo() {
        linkPageNumber = getIntent().getIntExtra("linkPageNumber", 0);
    }

    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 앱 구동시 필요한 데이터 요청하기
    public void mobile1001() {
        Log.i("MyTags", "mobile1001 = 앱 구동시 필요한 데이터 요청하기");

        ContentValues params = new ContentValues();
        params.put("act", "mobile1001");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(1);
        inLet.start();
    }

    // 앱 구동시 필요한 데이터를 받아 저장하기
    public void mobile1002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");
            if (result.equals("ok")) {
                // code..



                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    // 단말기 OS 버전이 마쉬멜로우 버전보다 낮을 때
                    if (new ShareInfo(activity).getMember() == 0) {
                        // 로그인 페이지로 이동
                        Intent intent = new Intent(activity, Mobile2000.class);
                        startActivity(intent);
                    } else {
                        // 메인 페이지로 이동
                        goMainPage();
                    }
                    finish();
                } else{
                    // 단말기 OS 버전이 마쉬멜로우 버전 이상일 때
                    try {
                        if (!checkPermissions(activity, PERMISSION_ALL)) {
                            // 하나라도 권한이 허용 되지 않은게 있다면
                            // 접근권한 페이지로 이동
                            Intent intent = new Intent(activity, Mobile1500.class);
                            startActivity(intent);
                            finish();
                        } else {
                            // 모든 권한이 허용되었다면
                            if (new ShareInfo(activity).getMember() == 0) {
                                // 로그인 페이지로 이동
                                Intent intent = new Intent(activity, Mobile2000.class);
                                startActivity(intent);
                            } else {
                                // 메인 페이지로 이동
                                goMainPage();
                            }

                            finish();
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }

                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void goMainPage() {
        Log.i("MyTags", "linkPageNumber = "+linkPageNumber);
        switch (linkPageNumber) {
            case 0:
                // 메인 페이지로 이동
                Intent intent2 = new Intent(activity, Mobile3000.class);
                startActivity(intent2);
                break;
            case 3500:
                // 프로필정보업데이트 페이지로 이동
                Intent intent5 = new Intent(activity, Mobile3000.class);
                startActivity(intent5);

                Intent intent6 = new Intent(activity, Mobile3500.class);
                startActivity(intent6);
                break;
            case 4600:
                // 진료예약신청 페이지로 이동
                Intent intent7 = new Intent(activity, Mobile3000.class);
                startActivity(intent7);

                Intent intent8 = new Intent(activity, Mobile4500.class);
                intent8.putExtra("pageIndex", 0);
                startActivity(intent8);
                break;
            case 4700:
                // 진료예약내역 페이지로 이동
                Intent intent9 = new Intent(activity, Mobile3000.class);
                startActivity(intent9);

                Intent intent10 = new Intent(activity, Mobile4500.class);
                intent10.putExtra("pageIndex", 1);
                startActivity(intent10);
                break;
            case 5020:
                // 진료과 페이지로 이동
                Intent intent11 = new Intent(activity, Mobile3000.class);
                startActivity(intent11);

                Intent intent12 = new Intent(activity, Mobile5000.class);
                intent12.putExtra("pageIndex", 0);
                startActivity(intent12);
                break;
            case 5040:
                // 의료진 페이지로 이동
                Intent intent13 = new Intent(activity, Mobile3000.class);
                startActivity(intent13);

                Intent intent14 = new Intent(activity, Mobile5000.class);
                intent14.putExtra("pageIndex", 1);
                startActivity(intent14);
                break;
            case 8000:
                // 병원뉴스 페이지로 이동
                Intent intent4 = new Intent(activity, Mobile3000.class);
                startActivity(intent4);

                Intent intent3 = new Intent(activity, Mobile8000.class);
                startActivity(intent3);
                break;


            default:

                break;
        }


    }


    public boolean checkPermissions(Activity activity, String[] permission) throws IllegalAccessException {
        int[] grantlist = new int[permission.length];
        Log.i("MyTag", "permission.length = "+permission.length);
        for (int i=0; i<permission.length; i++) {
            grantlist[i] = ContextCompat.checkSelfPermission(activity, permission[i]);
        }
        for (int i=0; i<grantlist.length; i++) {
            Log.i("MyTag", "grantlist["+i+"] = "+grantlist[i]);
            if (grantlist[i] == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }
}
