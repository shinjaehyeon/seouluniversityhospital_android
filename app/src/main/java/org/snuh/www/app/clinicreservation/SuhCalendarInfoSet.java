package org.snuh.www.app.clinicreservation;

import android.widget.Button;
import android.widget.TextView;

public class SuhCalendarInfoSet {
    public int index;

    public int year;
    public int month;
    public int date;
    public int day;
    public String dayString;

    public Button dateButton;
    public TextView dateTextView;
    public TextView todayTextView;

    public boolean isActiveDate;



    public void setIndex(int index) {
        this.index = index;
    }
    public int getIndex() {
        return this.index;
    }



    public void setYear(int year) {
        this.year = year;
    }
    public int getYear() {
        return this.year;
    }

    public void setMonth(int index) {
        this.month = index;
    }
    public int getMonth() {
        return this.month;
    }

    public void setDate(int date) {
        this.date = date;
    }
    public int getDate() {
        return this.date;
    }

    public void setDay(int index) {
        this.day = index;
    }
    public int getDay() {
        return this.day;
    }

    public void setDayString(String day) {
        this.dayString = day;
    }
    public String getDayString() {
        return this.dayString;
    }




    public void setDateButton(Button button) {
        this.dateButton = button;
    }
    public Button getDateButton() {
        return this.dateButton;
    }

    public void setDateTextView(TextView textView) {
        this.dateTextView = textView;
    }
    public TextView getDateTextView() {
        return this.dateTextView;
    }

    public void setTodayTextView(TextView textView) {
        this.todayTextView = textView;
    }
    public TextView getTodayTextView() {
        return this.todayTextView;
    }





    public void setisActiveDate(boolean b) {
        this.isActiveDate = b;
    }
    public boolean getIsActiveDate() {
        return this.isActiveDate;
    }
}
