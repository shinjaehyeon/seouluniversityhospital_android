package org.snuh.www.app.departmentdetail;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.doctordetail.Mobile5200;
import org.snuh.www.app.doctoritemset.DoctorItem;
import org.snuh.www.app.doctoritemset.DoctorItemAdapter;

import java.util.ArrayList;

public class Mobile5140 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    int index;
    int view_num = 100;

    int department_pk;

    TextView result_none;

    NestedScrollView nestedScrollView;

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<DoctorItem> items = new ArrayList<DoctorItem>();
    DoctorItemAdapter itemAdapter;



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5042: // 서버에 의료진 리스트 요청 후처리
                    mobile5042(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    public Mobile5140() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5140(Context context) {
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mobile5140, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        setRecyclerView();

        mobile5041();

        return v;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        result_none = (TextView) v.findViewById(R.id.result_none);
    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    public void setDepartmentPk(int n) {
        this.department_pk = n;
    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new DoctorItemAdapter(recyclerView, items, activity, itemOnClickListener, nestedScrollView);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile5041();
            }
        });
    }

    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DoctorItem item = (DoctorItem) v.getTag();

            int pk = item.getPk(); // 의사 고유번호
            String doctorname = item.getDoctorName(); // 의사 이름

            Log.i("MyTagssss", "doctor pirmarykey = "+pk);
            // 의료진 상세 페이지로 이동
            Intent intent = new Intent(activity, Mobile5200.class);
            intent.putExtra("doctor", doctorname);
            intent.putExtra("doctor_pk", pk);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activity.startActivity(intent);
        }
    };

    // 서버에 의료진 리스트 요청
    public void mobile5041() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5041");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("currentSelectedDepartmentCode", department_pk);
        params.put("index", index);
        params.put("view_num", view_num);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5042);
        inLet.start();
    }

    // 서버에 의료진 리스트 요청 후처리
    public void mobile5042(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                result_none.setVisibility(View.GONE);
                String more_info = jsonObject.getString("more_info");

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int doctor_pk = jsonObject1.getInt("doctor_pk");
                    String doctor_name = jsonObject1.getString("doctor_name");
                    int doctor_department = jsonObject1.getInt("doctor_department");

                    DoctorItem item = new DoctorItem();
                    item.setPk(doctor_pk);
                    item.setDoctorName(doctor_name);

                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;

            } else {
                itemAdapter.isMoreInfo = false;
                result_none.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    public void setNestedScrollView(NestedScrollView nestedScrollView) {
        this.nestedScrollView = nestedScrollView;
    }
}
