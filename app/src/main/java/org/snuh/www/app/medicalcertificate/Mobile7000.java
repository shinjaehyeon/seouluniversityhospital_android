package org.snuh.www.app.medicalcertificate;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomSelectDialog;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.medicalcertificatedetail.Mobile7100;
import org.snuh.www.app.medicalcertificateitemset.MedicalCertificateItem;
import org.snuh.www.app.medicalcertificateitemset.MedicalCertificateItemAdapter;
import org.snuh.www.app.questionitemset.QuestionItem;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile7000 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    /**************************************************************/

    CustomSelectDialog customSelectDialog;

    int periodSizes[] = {0, 7, 30, 60, 90, 150};
    String periodSizeString[] = {"전체 기간", "최근 7일",  "최근 30일", "최근 60일", "최근 90일", "최근 150일"};
    int period_size = 30; // 최근 n일
    int period_index = 2;

    TextView recent_day_textview;
    Button period_setting_button;
    
    TextView result_none;
    Button moreViewButton;

    /**************************************************************/

    int index = 0;
    int view_num = 6;

    NestedScrollView nestedScrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<MedicalCertificateItem> items = new ArrayList<MedicalCertificateItem>();
    MedicalCertificateItemAdapter itemAdapter;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 7002: // 서버에 진단서 리스트 요청 후처리
                    mobile7002(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile7000);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기

        setRecyclerView(); // 리사이클러뷰 관련 환경 설정하기
        mobile7001(); // 서버에 진단서 리스트 요청
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }






    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);
        customSelectDialog = new CustomSelectDialog(activity);

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        recent_day_textview = (TextView) findViewById(R.id.recent_day_textview);
        period_setting_button = (Button) findViewById(R.id.period_setting_button);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        result_none = (TextView) findViewById(R.id.result_none);
        moreViewButton = (Button) findViewById(R.id.moreViewButton);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        customSelectDialog.setFirstValue(2);
        customSelectDialog.setSelectionList(periodSizeString, periodSizes);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        period_setting_button.setOnClickListener(periodSettingButtonEvent);
    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new MedicalCertificateItemAdapter(recyclerView, items, activity, itemOnClickListener, nestedScrollView);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile7001(); // 서버에 진단서 리스트 요청
            }
        });

        // 버튼으로 더보기 구현하고 싶으면
        moreViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter.isMoreInfo == true) {
                    if (!itemAdapter.isLoading) {
                        itemAdapter.isLoading = true;
                        if (itemAdapter.onLoadMoreListener != null) {
                            itemAdapter.onLoadMoreListener.onLoadMore();
                        }
                    }
                } else {
                    callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.");
                }
            }
        });
    }

    /**************************************************************/

    View.OnClickListener periodSettingButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCustomSelectDialog("기간 선택");
        }
    };

    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MedicalCertificateItem item = (MedicalCertificateItem) v.getTag();

            Log.i("MyTags", "선택한 진단서 pk = "+item.getPrimaryKey());

            Intent intent = new Intent(activity, Mobile7100.class);
            intent.putExtra("suhmc", item.getPrimaryKey());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    };

    /**************************************************************/






    // 서버에 진단서 리스트 요청
    public void mobile7001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile7001");
        params.put("member", new ShareInfo(this).getMember());
        params.put("index", index);
        params.put("view_num", view_num);
        params.put("period_size", period_size);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(7002);
        inLet.start();
    }

    public void mobile7002(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "mobile7002 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                result_none.setVisibility(View.GONE);
                String more_info = jsonObject.getString("more_info");

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int primarykey = jsonObject1.getInt("suhmc");
                    int doctor = jsonObject1.getInt("doctor");
                    String doctor_name = jsonObject1.getString("doctor_name");
                    int department = jsonObject1.getInt("department");
                    String department_string = jsonObject1.getString("department_string");
                    String medical_certificate_date = jsonObject1.getString("medical_certificate_date");
                    String medical_certificate_time = jsonObject1.getString("medical_certificate_time");
                    int passed_day = jsonObject1.getInt("passed_day");
                    int passed_hour = jsonObject1.getInt("passed_hour");
                    int passed_minute = jsonObject1.getInt("passed_minute");

                    MedicalCertificateItem item = new MedicalCertificateItem();
                    item.setPrimaryKey(primarykey);
                    item.setPassedDay(passed_day);
                    item.setPassedHour(passed_hour);
                    item.setPassedMinute(passed_minute);
                    item.setDateString(medical_certificate_date);
                    item.setTimeString(medical_certificate_time);
                    item.setDepartment(department);
                    item.setDepartmentString(department_string);
                    item.setDoctor(doctor);
                    item.setDoctorName(doctor_name);

                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;


            } else {
                result_none.setVisibility(View.VISIBLE);
                itemAdapter.isMoreInfo = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void allClear() {
        index = 0;
        items.clear();
        itemAdapter.isMoreInfo = true;
        itemAdapter.notifyDataSetChanged();
        recyclerView.removeAllViews();
    }


    /**************************************************************/

    public void callCustomSelectDialog(String title) {
        customSelectDialog.show();
        customSelectDialog.setTitle(title);
        customSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customSelectDialog.dismiss();
            }
        });
        customSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (customSelectDialog.isOnItemClicked) {
                    period_index = customSelectDialog.getCurrentIndex();
                    period_size = periodSizes[period_index];

                    recent_day_textview.setText(periodSizeString[period_index]+"기준 ");

                    allClear();
                    mobile7001();

                    customSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
