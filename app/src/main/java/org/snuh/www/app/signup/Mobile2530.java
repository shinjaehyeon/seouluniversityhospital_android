package org.snuh.www.app.signup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.snuh.www.app.R;

public class Mobile2530 extends Fragment {
    Activity activity;
    View rootView;

    /**************************************************************/

    public Mobile2530() {

    }

    @SuppressLint("ValidFragment")
    public Mobile2530(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile2530, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    /**************************************************************/
}
