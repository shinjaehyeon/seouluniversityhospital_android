package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;
import java.util.Calendar;

public class SuhCalendar extends LinearLayout {
    View v;
    Activity activity;
    Context context;
    UseFul useFul;

    /**************************************************************/

    View.OnClickListener dateOnClickListener;

    RelativeLayout top_area; // 이전달, 다음달 버튼 및 년도/월 표시되는 영역
    RelativeLayout prev_month_button; // 이전달 버튼
    RelativeLayout next_month_button; // 다음달 버튼
    TextView year_display; // 년도 표시 텍스트뷰
    TextView month_display; // 월 표시 텍스트뷰



    int[] dateButtonIdResources = {
        R.id.date_index_0_button, R.id.date_index_1_button, R.id.date_index_2_button, R.id.date_index_3_button, R.id.date_index_4_button, R.id.date_index_5_button, R.id.date_index_6_button,
        R.id.date_index_7_button, R.id.date_index_8_button, R.id.date_index_9_button, R.id.date_index_10_button, R.id.date_index_11_button, R.id.date_index_12_button, R.id.date_index_13_button,
        R.id.date_index_14_button, R.id.date_index_15_button, R.id.date_index_16_button, R.id.date_index_17_button, R.id.date_index_18_button, R.id.date_index_19_button, R.id.date_index_20_button,
        R.id.date_index_21_button, R.id.date_index_22_button, R.id.date_index_23_button, R.id.date_index_24_button, R.id.date_index_25_button, R.id.date_index_26_button, R.id.date_index_27_button,
        R.id.date_index_28_button, R.id.date_index_29_button, R.id.date_index_30_button, R.id.date_index_31_button, R.id.date_index_32_button, R.id.date_index_33_button, R.id.date_index_34_button,
        R.id.date_index_35_button, R.id.date_index_36_button, R.id.date_index_37_button, R.id.date_index_38_button, R.id.date_index_39_button, R.id.date_index_40_button, R.id.date_index_41_button
    };
    ArrayList<Button> dateButton = new ArrayList<Button>(); // 날짜 버튼



    int[] dateTextViewIdResources = {
        R.id.date_index_0_textview, R.id.date_index_1_textview, R.id.date_index_2_textview, R.id.date_index_3_textview, R.id.date_index_4_textview, R.id.date_index_5_textview, R.id.date_index_6_textview,
        R.id.date_index_7_textview, R.id.date_index_8_textview, R.id.date_index_9_textview, R.id.date_index_10_textview, R.id.date_index_11_textview, R.id.date_index_12_textview, R.id.date_index_13_textview,
        R.id.date_index_14_textview, R.id.date_index_15_textview, R.id.date_index_16_textview, R.id.date_index_17_textview, R.id.date_index_18_textview, R.id.date_index_19_textview, R.id.date_index_20_textview,
        R.id.date_index_21_textview, R.id.date_index_22_textview, R.id.date_index_23_textview, R.id.date_index_24_textview, R.id.date_index_25_textview, R.id.date_index_26_textview, R.id.date_index_27_textview,
        R.id.date_index_28_textview, R.id.date_index_29_textview, R.id.date_index_30_textview, R.id.date_index_31_textview, R.id.date_index_32_textview, R.id.date_index_33_textview, R.id.date_index_34_textview,
        R.id.date_index_35_textview, R.id.date_index_36_textview, R.id.date_index_37_textview, R.id.date_index_38_textview, R.id.date_index_39_textview, R.id.date_index_40_textview, R.id.date_index_41_textview
    };
    ArrayList<TextView> dateTextView = new ArrayList<TextView>(); // 날짜 텍스트뷰




    int[] todayTextViewIdResources = {
        R.id.today_index_0_display, R.id.today_index_1_display, R.id.today_index_2_display, R.id.today_index_3_display, R.id.today_index_4_display, R.id.today_index_5_display, R.id.today_index_6_display,
        R.id.today_index_7_display, R.id.today_index_8_display, R.id.today_index_9_display, R.id.today_index_10_display, R.id.today_index_11_display, R.id.today_index_12_display, R.id.today_index_13_display,
        R.id.today_index_14_display, R.id.today_index_15_display, R.id.today_index_16_display, R.id.today_index_17_display, R.id.today_index_18_display, R.id.today_index_19_display, R.id.today_index_20_display,
        R.id.today_index_21_display, R.id.today_index_22_display, R.id.today_index_23_display, R.id.today_index_24_display, R.id.today_index_25_display, R.id.today_index_26_display, R.id.today_index_27_display,
        R.id.today_index_28_display, R.id.today_index_29_display, R.id.today_index_30_display, R.id.today_index_31_display, R.id.today_index_32_display, R.id.today_index_33_display, R.id.today_index_34_display,
        R.id.today_index_35_display, R.id.today_index_36_display, R.id.today_index_37_display, R.id.today_index_38_display, R.id.today_index_39_display, R.id.today_index_40_display, R.id.today_index_41_display
    };
    ArrayList<TextView> todayTextView = new ArrayList<TextView>();




    ArrayList<SuhCalendarInfoSet> infoSets = new ArrayList<SuhCalendarInfoSet>();

    /**************************************************************/

    public SuhCalendar(Context context) {
        super(context);
        this.context = context;
        this.activity = (Activity) context;
        // initView();
    }

    public SuhCalendar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (Activity) context;
        // initView();
    }

    public SuhCalendar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (Activity) context;
        // initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.suh_calendar, this, false);
        addView(v);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectEvent(); // 객체에 이벤트 설정하기


        setTodayDate(); // 오늘 날짜 저장하기
    }

    /**************************************************************/

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        // 이전달, 다음달 버튼 연결
        prev_month_button = (RelativeLayout) v.findViewById(R.id.prev_month_button);
        next_month_button = (RelativeLayout) v.findViewById(R.id.next_month_button);

        // 년, 월 표시 텍스트뷰 연결
        year_display = v.findViewById(R.id.year_display);
        month_display = v.findViewById(R.id.month_display);

        // 날짜 버튼, 날짜 텍스트뷰, 날짜의 오늘 텍스트뷰 연결
        for (int i=0; i<dateButtonIdResources.length; i++) {
            dateButton.add((Button) v.findViewById(dateButtonIdResources[i]));
            dateTextView.add((TextView) v.findViewById(dateTextViewIdResources[i]));
            todayTextView.add((TextView) v.findViewById(todayTextViewIdResources[i]));
            infoSets.add(new SuhCalendarInfoSet());
        }
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

        for (int i=0; i<dateButton.size(); i++) {
            dateButton.get(i).setTag(infoSets.get(i));
            dateButton.get(i).setOnClickListener(dateOnClickListener);
            infoSets.get(i).setDateButton(dateButton.get(i));
            infoSets.get(i).setDateTextView(dateTextView.get(i));
            infoSets.get(i).setTodayTextView(todayTextView.get(i));
            infoSets.get(i).setIndex(i);
            int day = i;
            if (i > 6) {
                day = i % 7;
            }
            infoSets.get(i).setDay(day);
            infoSets.get(i).setDayString(dayString[day]);
        }

        // 이전달, 다음달 버튼에 이벤트 셋팅하기
        prev_month_button.setOnClickListener(changeMonthButtonEvent);
        next_month_button.setOnClickListener(changeMonthButtonEvent);
    }

    public void setDateOnClickListener(View.OnClickListener e) {
        this.dateOnClickListener = e;
    }

    /**************************************************************/

    // 이전달, 다음달 이벤트
    View.OnClickListener changeMonthButtonEvent = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int year  = currentCalendar.get(currentCalendar.YEAR); // 현재 년도
            int month = currentCalendar.get(currentCalendar.MONTH); // 현재 월(인덱스 값, 3월이면 2)

            switch (v.getId()) {
                case R.id.prev_month_button: // 이전 달 버튼 클릭했을 경우

                    if (month - 1 < 0) {
                        currentCalendar.set(currentCalendar.YEAR, year - 1);
                        currentCalendar.set(currentCalendar.MONTH, 11);
                    } else {
                        currentCalendar.set(currentCalendar.YEAR, year);
                        currentCalendar.set(currentCalendar.MONTH, month - 1);
                    }

                    setViewCalendar();

                    break;
                case R.id.next_month_button: // 다음 달 버튼 클릭했을 경우

                    if (month + 1 > 11) {
                        currentCalendar.set(currentCalendar.YEAR, year + 1);
                        currentCalendar.set(currentCalendar.MONTH, 0);
                    } else {
                        currentCalendar.set(currentCalendar.YEAR, year);
                        currentCalendar.set(currentCalendar.MONTH, month + 1);
                    }

                    setViewCalendar();

                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    int todayYear; // 오늘 년도
    int todayMonth; // 오늘 월
    int todayDate; // 오늘 일

    int selectedYear; // 선택된 년도
    int selectedMonth; // 선택된 월
    int selectedDate; // 선택된 일
    int selectedButtonIndex; // 선택된 날짜의 버튼 인덱스
    int prevSelectedButtonindex = 900900; // 전 선택된 날짜의 버튼 인덱스

    Calendar prevCalendar = Calendar.getInstance();
    Calendar currentCalendar = Calendar.getInstance();
    Calendar nextCalendar = Calendar.getInstance();
    String dayString[] = {"일", "월", "화", "수", "목", "금", "토"};

    // 달력 그리기 (currentCalendar 값으로)
    public void setViewCalendar() {
        int year  = currentCalendar.get(currentCalendar.YEAR); // 현재 년도
        int month = currentCalendar.get(currentCalendar.MONTH); // 현재 월(인덱스 값, 3월이면 2)
        currentCalendar.set(currentCalendar.DATE, 1); // 1일로 설정

        // 년, 월 표시 갱신하기
        year_display.setText(year+"년");
        month_display.setText((month+1)+"월");

        // 현재 월의 시작 요일 인덱스 값 구하기
        int startDateDayIndex = currentCalendar.get(Calendar.DAY_OF_WEEK) - 1;

        // 현재 월의 마지막 일 구하기
        int lastDate = currentCalendar.getActualMaximum(currentCalendar.DAY_OF_MONTH);
        Log.i("MyTags", "lastDate = "+lastDate);

        // 달력 그리기 (현재 달의 1일 부터 ~ n일 까지)
        int date = 1;
        int buttonindex = 0;
        for (int i=0; i<dateButton.size(); i++) {
            if (i % 2 == 0) {
                dateButton.get(i).setBackgroundResource(R.drawable.ripple_button_type_11);
            } else {
                dateButton.get(i).setBackgroundResource(R.drawable.ripple_button_type_12);
            }

            if (i >= startDateDayIndex) {
                // 현재 날짜의 정보 객체 생성
                infoSets.get(i).setYear(year);
                infoSets.get(i).setMonth(month);
                infoSets.get(i).setDate(date);
                infoSets.get(i).setisActiveDate(true);

                // 날짜 표시 텍스트뷰에 날짜 표시 및 색상 지정
                dateTextView.get(i).setText(date+"");
                dateTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_normal_color));

                // 오늘 날짜 텍스트뷰 숨김 처리
                todayTextView.get(i).setVisibility(View.GONE);

                // 오늘 날짜이면 오늘 날짜 텍스트뷰 표시하기
                if (year == todayYear && (month+1) == todayMonth && date == todayDate) {
                    todayTextView.get(i).setVisibility(View.VISIBLE);
                    todayTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_normal_color));
                }

                // 현재 날짜가 선택된 날짜라면 표시하기
                if (year == selectedYear && month == selectedMonth && date == selectedDate) {
                    dateButton.get(i).setBackgroundResource(R.drawable.ripple_button_type_2);
                    dateTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_on_color));
                    todayTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_on_color));
                }

                date++;
                if (date > lastDate) {
                    break;
                }
            }
            buttonindex++;
        }


        // 이전 달 그리기
        if (month - 1 < 0) {
            prevCalendar.set(prevCalendar.YEAR, year-1);
            prevCalendar.set(prevCalendar.MONTH, 11);
            prevCalendar.set(prevCalendar.DATE, 1);
        } else {
            prevCalendar.set(prevCalendar.YEAR, year);
            prevCalendar.set(prevCalendar.MONTH, month - 1);
            prevCalendar.set(prevCalendar.DATE, 1);
        }

        // 이전달 정보
        int prevYear = prevCalendar.get(prevCalendar.YEAR);
        int prevMonth = prevCalendar.get(prevCalendar.MONTH);

        // 이전 달의 마지막날 구하기
        int prevMonthLastDate = prevCalendar.getActualMaximum(prevCalendar.DAY_OF_MONTH);

        int prevDate = prevMonthLastDate;
        for (int i=startDateDayIndex-1; i>=0; i--) {
            // 현재 날짜의 정보 객체 생성
            infoSets.get(i).setYear(prevYear);
            infoSets.get(i).setMonth(prevMonth);
            infoSets.get(i).setDate(prevDate);
            infoSets.get(i).setisActiveDate(false);

            // 버튼 배경 흰색으로 지정
            dateButton.get(i).setBackgroundResource(R.drawable.ripple_button_type_6);

            // 날짜 표시 텍스트뷰에 날짜 표시 및 색상 지정
            dateTextView.get(i).setText(prevDate+"");
            dateTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_blur_color));

            // 오늘 날짜 텍스트뷰 숨김 처리
            todayTextView.get(i).setVisibility(View.GONE);

            // 오늘 날짜이면 오늘 날짜 텍스트뷰 표시하기
            if (prevYear == todayYear && (prevMonth+1) == todayMonth && prevDate == todayDate) {
                todayTextView.get(i).setVisibility(View.VISIBLE);
                todayTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_blur_color));
            }

            prevDate--;
        }



        // 다음 달 그리기
        if (month + 1 > 11) {
            nextCalendar.set(nextCalendar.YEAR, year+1);
            nextCalendar.set(nextCalendar.MONTH, 0);
            nextCalendar.set(nextCalendar.DATE, 1);
        } else {
            nextCalendar.set(nextCalendar.YEAR, year);
            nextCalendar.set(nextCalendar.MONTH, month + 1);
            nextCalendar.set(nextCalendar.DATE, 1);
        }

        // 다음달 정보
        int nextYear = nextCalendar.get(nextCalendar.YEAR);
        int nextMonth = nextCalendar.get(nextCalendar.MONTH);


        int nextDate = 1;
        for (int i=buttonindex+1; i<dateButton.size(); i++) {
            // 현재 날짜의 정보 객체 생성
            infoSets.get(i).setYear(nextYear);
            infoSets.get(i).setMonth(nextMonth);
            infoSets.get(i).setDate(nextDate);
            infoSets.get(i).setisActiveDate(false);

            // 버튼 배경 흰색으로 지정
            dateButton.get(i).setBackgroundResource(R.drawable.ripple_button_type_6);

            // 날짜 표시 텍스트뷰에 날짜 표시 및 색상 지정
            dateTextView.get(i).setText(nextDate+"");
            dateTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_blur_color));

            // 오늘 날짜 텍스트뷰 숨김 처리
            todayTextView.get(i).setVisibility(View.GONE);

            // 오늘 날짜이면 오늘 날짜 텍스트뷰 표시하기
            if (prevYear == todayYear && (prevMonth+1) == todayMonth && prevDate == todayDate) {
                todayTextView.get(i).setVisibility(View.VISIBLE);
                todayTextView.get(i).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_blur_color));
            }

            nextDate++;
        }
    }



    // 현재 선택된 년,월,일 값, 버튼 인덱스 값 셋팅
    public void setSelectedYear(int year) {
        this.selectedYear = year;
    }
    public void setSelectedMonth(int month) {
        this.selectedMonth = month;
    }
    public void setSelectedDate(int date) {
        this.selectedDate = date;
    }
    public void setSelectedButtonIndex(int index) {
        this.selectedButtonIndex = index;
    }
    public void setPrevSelectedButtonindex(int index) {
        this.prevSelectedButtonindex = index;
    }
    public void setPrevSelectedButtonIndexYear(int year) {
        this.prevSelectedButtonIndexYear = year;
    }
    public void setPrevSelectedButtonIndexMonth(int month) {
        this.prevSelectedButtonIndexMonth = month;
    }

    public int getCurrentFocusYear() {
        return currentCalendar.get(currentCalendar.YEAR);
    }
    public int getCurrentFocusMonth() {
        return currentCalendar.get(currentCalendar.MONTH);
    }

    int prevSelectedButtonIndexYear;
    int prevSelectedButtonIndexMonth;

    public boolean checkPrevSelectedValueCurrentValue() {
        if (currentCalendar.get(currentCalendar.YEAR) == prevSelectedButtonIndexYear && currentCalendar.get(currentCalendar.MONTH) == prevSelectedButtonIndexMonth) {
            return true;
        } else {
            return false;
        }
    }


    // 오늘 날짜 셋팅
    public void setTodayDate() {
        todayYear = useFul.getTodayYear();
        todayMonth = useFul.getTodayMonth();
        todayDate = useFul.getTodayDate();
    }

}
