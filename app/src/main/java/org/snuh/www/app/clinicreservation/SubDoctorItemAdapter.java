package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.ClassUtils;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;

import java.util.List;

public class SubDoctorItemAdapter extends RecyclerView.Adapter { // 제네릭 클래스, T1 = Item 클래스, B2 = ViewHolder 클래스
    private Activity activity;

    private List<SubDoctorItem> subDoctorItems;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public final LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    /**************************************************************/

    public SubDoctorItemAdapter(RecyclerView recyclerView, List<SubDoctorItem> subDoctorItems, final Activity activity, View.OnClickListener itemsOnClickListener) {
        this.subDoctorItems = subDoctorItems;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return subDoctorItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.sub_doctor_item, parent, false);
            return new SubDoctorItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SubDoctorItemViewHolder) {
            SubDoctorItem subDoctorItem = subDoctorItems.get(position);
            SubDoctorItemViewHolder userViewHolder = (SubDoctorItemViewHolder) holder;

            subDoctorItem.setViewHolder(userViewHolder);

            // userViewHolder.doctor_image ... 이미지는 나중에 (관리자 완성 되면)
            userViewHolder.doctor_department.setText(subDoctorItem.getDoctorDepartment());
            userViewHolder.doctor_name.setText(subDoctorItem.getDoctorName());
            userViewHolder.view.setTag(subDoctorItem);
            userViewHolder.view.setOnClickListener(itemsOnClickListener);

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return subDoctorItems == null ? 0 : subDoctorItems.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/

}
