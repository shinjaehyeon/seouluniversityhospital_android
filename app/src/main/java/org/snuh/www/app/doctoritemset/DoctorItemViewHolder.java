package org.snuh.www.app.doctoritemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class DoctorItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public Button button;
    public TextView doctorname_textview;

    /**************************************************************/

    public DoctorItemViewHolder(View view) {
        super(view);
        this.view = view;

        button = (Button) view.findViewById(R.id.button);
        doctorname_textview = (TextView) view.findViewById(R.id.doctorname_textview);
    }
}
