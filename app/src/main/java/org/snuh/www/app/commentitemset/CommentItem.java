package org.snuh.www.app.commentitemset;

public class CommentItem {
    public int commentPrimaryKey;
    public int memberPrimaryKey;
    public String memberName;
    public String datetimeString;
    public String comment;

    public CommentItemViewHolder viewHolder;

    public void setCommentPrimaryKey(int n) {
        this.commentPrimaryKey = n;
    }
    public int getCommentPrimaryKey() {
        return this.commentPrimaryKey;
    }

    public void setMemberPrimaryKey(int n) {
        this.memberPrimaryKey = n;
    }
    public int getMemberPrimaryKey() {
        return this.memberPrimaryKey;
    }

    public void setMemberName(String s) {
        this.memberName = s;
    }
    public String getMemberName() {
        return this.memberName;
    }

    public void setDatetimeString(String s) {
        this.datetimeString = s;
    }
    public String getDatetimeString() {
        return this.datetimeString;
    }

    public void setComment(String s) {
        this.comment = s;
    }
    public String getComment() {
        return this.comment;
    }


    public void setViewHolder(CommentItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public CommentItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
