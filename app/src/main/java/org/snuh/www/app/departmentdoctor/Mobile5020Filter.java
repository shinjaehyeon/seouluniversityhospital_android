package org.snuh.www.app.departmentdoctor;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentitemset.DepartmentItem;
import org.snuh.www.app.departmentitemset.DepartmentItemAdapter;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class Mobile5020Filter extends RelativeLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;
    DepartmentAlignSelectDialog departmentAlignSelectDialog;

    Mobile5020RecyclerView mobile5020RecyclerView;


    TextView filter_textview;
    Button period_setting_button;


    String periodSizeString[] = {"진료과 전체",
            "ㄱ 으로 시작하는 진료과",  "ㄴ 으로 시작하는 진료과", "ㄷ 으로 시작하는 진료과", "ㄹ 으로 시작하는 진료과",
            "ㅁ 으로 시작하는 진료과",  "ㅂ 으로 시작하는 진료과", "ㅅ 으로 시작하는 진료과", "ㅇ 으로 시작하는 진료과",
            "ㅈ 으로 시작하는 진료과",  "ㅊ 으로 시작하는 진료과", "ㅋ 으로 시작하는 진료과", "ㅌ 으로 시작하는 진료과",
            "ㅍ 으로 시작하는 진료과",  "ㅎ 으로 시작하는 진료과"
    };
    int align_index = 0;

    /**************************************************************/


    /**************************************************************/

    public Mobile5020Filter(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5020Filter(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5020Filter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5020_filter, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        filter_textview = (TextView) v.findViewById(R.id.filter_textview);
        period_setting_button = (Button) v.findViewById(R.id.period_setting_button);

        departmentAlignSelectDialog = new DepartmentAlignSelectDialog(activity);
    }

    public void setRealInItView() {
        setObjectEvent();
    }

    public void setMobile5020RecyclerView(Mobile5020RecyclerView r) {
        this.mobile5020RecyclerView = r;
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // 정렬설정 버튼 클릭 시 이벤트 연결
        period_setting_button.setOnClickListener(periodSettingButtonEvent);
    }

    /**************************************************************/

    // 정렬설정 버튼 클릭 시 이벤트
    View.OnClickListener periodSettingButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callDepartmentAlignSelectDialog("진료과 정렬 선택");
        }
    };

    /**************************************************************/

    // 진료과 선택 다이얼로그 창 호출 함수
    public void callDepartmentAlignSelectDialog(String title) {
        departmentAlignSelectDialog.show();
        departmentAlignSelectDialog.setTitle(title);
        departmentAlignSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                departmentAlignSelectDialog.dismiss();
            }
        });
        departmentAlignSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (departmentAlignSelectDialog.isOnItemClicked) {
                    align_index = departmentAlignSelectDialog.align_index;
                    filter_textview.setText(periodSizeString[align_index]);

                    mobile5020RecyclerView.align_index = align_index;
                    mobile5020RecyclerView.allClear();


                    mobile5020RecyclerView.mobile5021(); // 서버에 진료과 리스트 요청


                    departmentAlignSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

}
