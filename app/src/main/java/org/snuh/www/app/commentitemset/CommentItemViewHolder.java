package org.snuh.www.app.commentitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.snuh.www.app.R;

public class CommentItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public TextView name_textview;
    public TextView datetime_textview;
    public TextView content_textview;

    /**************************************************************/

    public CommentItemViewHolder(View view) {
        super(view);
        this.view = view;

        name_textview = (TextView) view.findViewById(R.id.name_textview);
        datetime_textview = (TextView) view.findViewById(R.id.datetime_textview);
        content_textview = (TextView) view.findViewById(R.id.content_textview);
    }
}
