package org.snuh.www.app.noticeitemset;

public class NoticeItem {
    public int primaryKey; // 푸쉬알림 pk
    public int passedDate; // 지금으로부터 알림온 날이 몇 일전인지
    public int passedHour; // 지금으로부터 알림온 날이 몇 시간전인지
    public int passedMinute; // 지금으로부터 알림온 날이 몇 분전인지
    public String getDate; // 받은 날짜 정보
    public String getTime; // 받은 시간 정보
    public String title; // 푸쉬알림 제목
    public String content; // 푸쉬알림 내용

    public int linkPage;
    public int lineType; // 링크 유형
    public String linkValue; // 링크 값

    public int status; // 푸쉬 상태

    public NoticeItemViewHolder viewHolder;


    public void setPrimaryKey(int n) {
        this.primaryKey = n;
    }
    public int getPrimaryKey() {
        return this.primaryKey;
    }

    public void setPassedDate(int n) {
        this.passedDate = n;
    }
    public int getPassedDate() {
        return this.passedDate;
    }

    public void setPassedHour(int n) {
        this.passedHour = n;
    }
    public int getPassedHour() {
        return this.passedHour;
    }

    public void setPassedMinute(int n) {
        this.passedMinute = n;
    }
    public int getPassedMinute() {
        return this.passedMinute;
    }

    public void setGetDate(String s) {
        this.getDate = s;
    }
    public String getGetDate() {
        return this.getDate;
    }

    public void setGetTime(String s) {
        this.getTime = s;
    }
    public String getGetTime() {
        return this.getTime;
    }

    public void setTitle(String s) {
        this.title = s;
    }
    public String getTitle() {
        return this.title;
    }

    public void setContent(String s) {
        this.content = s;
    }
    public String getContent() {
        return this.content;
    }

    public void setLineType(int n) {
        this.lineType = n;
    }
    public int getLineType() {
        return this.lineType;
    }

    public void setLinkPage(int n) {
        this.linkPage = n;
    }
    public int getLinkPage() {
        return this.linkPage;
    }

    public void setLinkValue(String value) {
        this.linkValue = value;
    }
    public String getLinkValue() {
        return this.linkValue;
    }

    public void setStatus(int n) {
        this.status = n;
    }
    public int getStatus() {
        return this.status;
    }


    public void setViewHolder(NoticeItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public NoticeItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
