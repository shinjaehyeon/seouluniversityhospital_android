package org.snuh.www.app.personalquestion;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4620;
import org.snuh.www.app.clinicreservation.Mobile4640;
import org.snuh.www.app.clinicreservation.Mobile4660;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

public class Mobile5520 extends Fragment {
    Activity activity;
    View v;
    UseFul useFul;

    Mobile5540 mobile5540;

    QuestionTypeSelectDialog questionTypeSelectDialog;
    int selected_question_type_code;

    TextView question_type_display_textview;

    Button question_type_select_button;

    int checkedQuestionTypeCode;
    String checkedQuestionTitle;
    String checkedQuestionContent;

    EditText question_title_edittext;
    EditText question_content_edittext;
    Button uploadButton;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5522: // 서버에 1:1 문의내용 DB 업로드 요청 후처리
                    mobile5522(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    public Mobile5520() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5520(Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mobile5520, container, false);

        setConnectViewId(); //  xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        return v;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);
        questionTypeSelectDialog = new QuestionTypeSelectDialog(activity);

        question_type_display_textview = (TextView) v.findViewById(R.id.question_type_display_textview);

        question_type_select_button = (Button) v.findViewById(R.id.question_type_select_button);

        question_title_edittext = (EditText) v.findViewById(R.id.question_title_edittext);
        question_content_edittext = (EditText) v.findViewById(R.id.question_content_edittext);
        uploadButton = (Button) v.findViewById(R.id.uploadButton);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        question_type_select_button.setOnClickListener(questionTypeSelectButtonEvent);
        uploadButton.setOnClickListener(uploadButtonEvent);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 문의유형 선택 버튼 클릭시 발생할 이벤트
    View.OnClickListener questionTypeSelectButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callQuestionTypeSelectDialog("문의유형 선택");
        }
    };

    // 문의하기 버튼 클릭시 발생할 이벤트
    View.OnClickListener uploadButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (formCheck()) {
                mobile5521(); // 서버에 1:1 문의내용 DB 업로드 요청
            }
        }
    };

    public boolean formCheck() {
        if (selected_question_type_code == 0) {
            callSUHDialogType1Dialog("안내", "문의 유형을 선택해주세요.");
            return false;
        }
        checkedQuestionTypeCode = selected_question_type_code;

        String questionTitle = question_title_edittext.getText().toString();
        if (questionTitle.length() < 4) {
            callSUHDialogType1Dialog("안내", "문의 제목은 4자 이상으로 입력해주세요.");
            return false;
        }
        checkedQuestionTitle = questionTitle;

        String questionContent = question_content_edittext.getText().toString();
        if (questionContent.length() < 10) {
            callSUHDialogType1Dialog("안내", "문의 내용은 10자 이상으로 입력해주세요.");
            return false;
        }
        checkedQuestionContent = questionContent;

        return true;
    }

    public void allClear() {
        question_type_display_textview.setText("문의유형을 선택해주세요.");
        question_title_edittext.setText("");
        question_content_edittext.setText("");

        selected_question_type_code = 0;
        checkedQuestionTypeCode = 0;
        checkedQuestionTitle = "";
        checkedQuestionContent = "";
    }

    public void setMobile5540(Mobile5540 m) {
        this.mobile5540 = m;
    }




    // 서버에 1:1 문의내용 DB 업로드 요청
    public void mobile5521() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5521");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("questionTypeCode", checkedQuestionTypeCode);
        params.put("questionTitle", checkedQuestionTitle);
        params.put("questionContent", checkedQuestionContent);


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5522);
        inLet.start();
    }

    // 서버에 1:1 문의내용 DB 업로드 요청 후처리
    public void mobile5522(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "mobile5522 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                callSUHDialogType1Dialog("안내", "문의가 성공적으로 등록되었습니다.");
                // mobile5540.allClear();
                // mobile5540.mobile5541();
                allClear();
            } else {
                callSUHDialogType1Dialog("안내", "문의 등록중 오류가 발생하였습니다.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callQuestionTypeSelectDialog(String title) {
        questionTypeSelectDialog.show();

        questionTypeSelectDialog.setTitle(title);
        questionTypeSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionTypeSelectDialog.dismiss();
            }
        });
        questionTypeSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (questionTypeSelectDialog.isOnItemClicked) {
                    selected_question_type_code = questionTypeSelectDialog.questionType[questionTypeSelectDialog.selected_index];
                    question_type_display_textview.setText(questionTypeSelectDialog.questionTypeString[questionTypeSelectDialog.selected_index]);


                    questionTypeSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }
}
