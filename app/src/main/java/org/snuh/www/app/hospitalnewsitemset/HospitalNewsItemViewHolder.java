package org.snuh.www.app.hospitalnewsitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class HospitalNewsItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public Button button;
    public TextView title;
    public TextView datetime;
    public TextView view_index;

    /**************************************************************/

    public HospitalNewsItemViewHolder(View view) {
        super(view);
        this.view = view;

        button = (Button) view.findViewById(R.id.button);
        title = (TextView) view.findViewById(R.id.title);
        datetime = (TextView) view.findViewById(R.id.datetime);
        view_index = (TextView) view.findViewById(R.id.view_index);
    }
}
