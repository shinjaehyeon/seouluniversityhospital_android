package org.snuh.www.app.signup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomSelectDialog;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.SUHDialogType2;
import org.snuh.www.app.common.SUHDialogType3;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.w3c.dom.Text;

public class Mobile2520 extends Fragment {
    Activity activity;
    View rootView;

    CustomSelectDialog customSelectDialog;

    /**************************************************************/

    EditText name;
    EditText phoneNumber;
    EditText authenticationNumber;
    EditText email;
    EditText password;
    EditText passwordCheck;
    EditText birthdayYear;
    EditText birthdayMonth;
    EditText birthdayDate;
    EditText addr;

    Button genderSelectButton;
    Button emailOverLapCheckButton;

    /**************************************************************/

    String[] genderStringArray = {"남자", "여자"};
    int[] genderIntegerArray = {800501, 800502};
    int currentSelectedGenderCode;
    TextView genderDisplayText;

    TextView mobileCompanyDisplayText;
    Button theMobileCompanySelectButton;
    int currentSelectMobileCompanyNumber; // 현재 선택된 통신사 번호
    String[] mobileCompany = {"LG U+", "SKT", "KT", "LG U+ 알뜰폰", "SKT 알뜰폰", "KT 알뜰폰"};
    int mobileCompanyCode[] = {700101, 700102, 700103, 700104, 700105, 700106};

    TextView theBloodTypeDisplayText;
    Button theBloodTypeSelectButton;
    int currentSelectBloodTypeNumber; // 현재 선택된 혈액형 번호
    String[] bloodType = {"O 형", "A 형", "B 형", "AB 형"};
    int bloodTypeCode[] = {600101, 600102, 600103, 600104};

    boolean isEmailOverLapChecked;

    /**************************************************************/

    String checkedName; // 체크된 이름
    int checkedGender; // 체크된 성별
    int checkedMobileCompanyNumber; // 체크된 통신사 번호
    String checkedPhoneNumber; // 체크된 휴대폰 번호
    String checkedEmail; // 체크된 이메일
    String checkedPassword; // 체크된 비밀번호
    String checkedBirthday; // 체크된 생년월일
    int checkedBloodTypeNumber; // 체크된 혈액형 번호
    String checkedAddress; // 체크된 주소

    /**************************************************************/

    public Mobile2520() {

    }

    @SuppressLint("ValidFragment")
    public Mobile2520(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 2527: // 이메일 중복체크 요청 후처리
                    mobile2527(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile2520, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        // 통신사 선택 버튼
        theMobileCompanySelectButton = (Button) rootView.findViewById(R.id.theMobileCompanySelectButton);
        theBloodTypeSelectButton = (Button) rootView.findViewById(R.id.theBloodTypeSelectButton);

        mobileCompanyDisplayText = (TextView) rootView.findViewById(R.id.mobileCompanyDisplayText);
        theBloodTypeDisplayText = (TextView) rootView.findViewById(R.id.theBloodTypeDisplayText);

        genderSelectButton = (Button) rootView.findViewById(R.id.genderSelectButton);
        emailOverLapCheckButton = (Button) rootView.findViewById(R.id.emailOverLapCheckButton);



        genderDisplayText = (TextView) rootView.findViewById(R.id.genderDisplayText);
        name = (EditText) rootView.findViewById(R.id.name);
        phoneNumber = (EditText) rootView.findViewById(R.id.phoneNumber);
        authenticationNumber = (EditText) rootView.findViewById(R.id.authenticationNumber);
        email = (EditText) rootView.findViewById(R.id.email);
        password = (EditText) rootView.findViewById(R.id.password);
        passwordCheck = (EditText) rootView.findViewById(R.id.passwordCheck);
        birthdayYear = (EditText) rootView.findViewById(R.id.birthdayYear);
        birthdayMonth = (EditText) rootView.findViewById(R.id.birthdayMonth);
        birthdayDate = (EditText) rootView.findViewById(R.id.birthdayDate);
        addr = (EditText) rootView.findViewById(R.id.addr);


        customSelectDialog = new CustomSelectDialog(activity);
    }



    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        theMobileCompanySelectButton.setOnClickListener(theMobileCompanySelectButtonClickEvent);
        theBloodTypeSelectButton.setOnClickListener(theBloodTypeSelectButtonClickEvent);

        genderSelectButton.setOnClickListener(genderSelectButtonClickEvent);
        emailOverLapCheckButton.setOnClickListener(emailOverLapCheckButtonClickEvent);

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        // customSelectDialog.setFirstValue(2);
        customSelectDialog.setSelectionList(genderStringArray, genderIntegerArray);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    /**************************************************************/

    // 일반 서울대학교병원 커스텀 다이얼로그
    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    // 통신사 선택 커스텀 다이얼로그
    public void callSUHDialogType2Dialog(String title) {
        final SUHDialogType2 suhDialogType2 = new SUHDialogType2(activity);
        suhDialogType2.show();
        suhDialogType2.setParentSelectedNumber(currentSelectMobileCompanyNumber);
        suhDialogType2.setTitle(title);
        suhDialogType2.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType2.dismiss();
            }
        });
        suhDialogType2.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                currentSelectMobileCompanyNumber = suhDialogType2.getCurrentSelectedMobileCompanyNumber();
                Log.i("FinalTags", "currentSelectMobileCompanyNumber = "+currentSelectMobileCompanyNumber);
                if (currentSelectMobileCompanyNumber - 1 >= 0) {
                    mobileCompanyDisplayText.setText(mobileCompany[currentSelectMobileCompanyNumber - 1]);
                    mobileCompanyDisplayText.setTextColor(Color.parseColor("#888888"));
                }
            }
        });
    }

    // 혈액형 선택 커스텀 다이얼로그
    public void callSUHDialogType3Dialog(String title) {
        final SUHDialogType3 suhDialogType3 = new SUHDialogType3(activity);
        suhDialogType3.show();
        suhDialogType3.setParentSelectedNumber(currentSelectBloodTypeNumber);
        suhDialogType3.setTitle(title);
        suhDialogType3.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType3.dismiss();
            }
        });
        suhDialogType3.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                currentSelectBloodTypeNumber = suhDialogType3.getCurrentSelectedMobileCompanyNumber();
                if (currentSelectBloodTypeNumber - 1 >= 0) {
                    theBloodTypeDisplayText.setText(bloodType[currentSelectBloodTypeNumber - 1]);
                    theBloodTypeDisplayText.setTextColor(Color.parseColor("#888888"));
                }
            }
        });
    }

    // 입력받은 값들 체크하기
    public boolean mobile2521() {
        UseFul useFul = new UseFul(activity);

        // 이름 체크하기
        String nameString = name.getText().toString();
        if (nameString.replaceAll("\\p{Z}", "").equals("")) {

            // 이름에 아무 값도 없으면
            callSUHDialogType1Dialog("안내", "이름을 입력해주세요.");
            return false; // 함수 종료

        } else if (useFul.isExistEmptySpace(nameString)) {

            // 이름에 공백이 들어가 있으면
            callSUHDialogType1Dialog("안내", "이름에는 공백이 들어갈 수 없습니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else {
            checkedName = nameString; // 체크된 이름에 저장
        }



        // 성별 체크하기
        if (currentSelectedGenderCode == 0) {
            callSUHDialogType1Dialog("안내", "성별을 선택해주세요.");
            return false; // 함수 종료
        } else {
            checkedGender = currentSelectedGenderCode;
        }


        // 통신사 체크하기
        if (currentSelectMobileCompanyNumber == 0) {

            // 통신사가 선택되어 있지 않으면
            callSUHDialogType1Dialog("안내", "휴대폰 통신사를 선택해주세요.");
            return false; // 함수 종료

        } else {
            checkedMobileCompanyNumber = mobileCompanyCode[currentSelectMobileCompanyNumber-1]; // 체크된 통신사에 저장
        }


        // 휴대폰 체크하기
        String phoneNumberString = phoneNumber.getText().toString().replaceAll("\\p{Z}", "");
        if (phoneNumberString.equals("")) {

            // 휴대폰에 입력된 번호가 없으면
            callSUHDialogType1Dialog("안내", "휴대폰 번호를 입력해주세요.");
            return false; // 함수 종료

        } else {
            checkedPhoneNumber = phoneNumberString; // 체크된 휴대폰 번호에 저장
        }


        // 이메일 체크하기
        String emailString = email.getText().toString();
        if (emailString.replaceAll("\\p{Z}", "").equals("")) {

            // 이메일에 아무 값도 입력되어 있지 않으면
            callSUHDialogType1Dialog("안내", "이메일을 입력해주세요.");
            return false; // 함수 종료

        } else if (useFul.isExistEmptySpace(emailString)) {

            // 이메일에 공백이 들어가 있을 경우
            callSUHDialogType1Dialog("안내", "이메일에는 공백이 들어갈 수 없습니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else if (!isEmailOverLapChecked) {

            // 이메일 중복체크가 되어 있지 않거나 중복된 이메일일 경우
            callSUHDialogType1Dialog("안내", "이메일 중복체크를 하지 않으셨거나 이미 가입된 이메일 입니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else {
            checkedEmail = emailString; // 체크된 이메일에 저장
        }


        // 비밀번호 체크
        String passwordString = password.getText().toString();
        String passwordCheckString = passwordCheck.getText().toString();
        if (passwordString.replaceAll("\\p{Z}", "").equals("")) {

            // 비밀번호에 아무 값도 입력되어 있지 않으면
            callSUHDialogType1Dialog("안내", "비밀번호를 입력해주세요.");
            return false; // 함수 종료

        } else if (useFul.isExistEmptySpace(passwordString)) {

            // 비밀번호에 공백이 포함되어 있으면
            callSUHDialogType1Dialog("안내", "비밀번호에는 공백이 들어갈 수 없습니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else if (passwordString.length() < 6) {

            // 비밀번호 문자열 길이가 6보다 작으면
            callSUHDialogType1Dialog("안내", "비밀번호는 최소 6자 이상으로 입력해주세요.");
            return false; // 함수 종료

        } else if (!passwordString.equals(passwordCheckString)) {

            // 비밀번호와 비밀번호확인이 일치하지 않으면
            callSUHDialogType1Dialog("안내", "비밀번호와 비밀번호확인이 일치하지 않습니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else {
            checkedPassword = passwordString; // 체크된 비밀번호에 저장
        }


        // 생년월일 체크
        String birthdayYearString = birthdayYear.getText().toString();
        String birthdayMonthString = birthdayMonth.getText().toString();
        String birthdayDateString = birthdayDate.getText().toString();

        if (birthdayYearString.replaceAll("\\p{Z}", "").equals("") || birthdayMonthString.replaceAll("\\p{Z}", "").equals("") || birthdayDateString.replaceAll("\\p{Z}", "").equals("")) {

            // 생년월일의 년, 월, 일 중에서 하나라도 아무 값도 입력하지 않았을 경우
            callSUHDialogType1Dialog("안내", "생년월일을 전부 입력해주세요.");
            return false; // 함수 종료

        } else {
            int birthdayYearInteger = Integer.parseInt(birthdayYearString);
            int birthdayMonthInteger = Integer.parseInt(birthdayMonthString);
            int birthdayDateInteger = Integer.parseInt(birthdayDateString);

            String birthdayYear2 = birthdayYearInteger+"";
            String birthdayMonth2 = ""+birthdayMonthInteger;
            if (birthdayMonthInteger < 10) {
                birthdayMonth2 = "0"+birthdayMonthInteger;
            }
            String birthdayDate2 = ""+birthdayDateInteger;
            if (birthdayDateInteger < 10) {
                birthdayDate2 = "0"+birthdayDateInteger;
            }

            String birthdayFull = birthdayYear2+""+birthdayMonth2+""+birthdayDate2;

            if (!useFul.dateCheck(birthdayFull, "yyyyMMdd")) {

                // 입력받은 생년월일 날짜가 올바른 날짜가 아니면
                callSUHDialogType1Dialog("안내", "생년월일의 날짜 형식이 올바르지 않습니다. 다시 확인해주세요.");
                return false; // 함수 종료

            } else {
                checkedBirthday = birthdayFull; // 체크된 생년월일에 저장
            }

        }


        // 혈액형 체크
        if (currentSelectBloodTypeNumber == 0) {

            // 현재 혈액형이 선택되어 있지 않다면
            callSUHDialogType1Dialog("안내", "혈액형을 선택해주세요.");
            return false; // 함수 종료

        } else {
            checkedBloodTypeNumber = bloodTypeCode[currentSelectBloodTypeNumber-1]; // 체크된 혈액형에 저장
        }


        // 주소 체크 (선택사항)
        String addrString = addr.getText().toString();
        checkedAddress = addrString; // 체크된 주소에 저장


        return true;
    }

    // 이메일 중복체크 요청
    public boolean mobile2526() {
        UseFul useFul = new UseFul(activity);
        String emailString = email.getText().toString();
        if (emailString.replaceAll("\\p{Z}", "").equals("")) {

            // 이메일에 아무 값도 입력되어 있지 않으면
            callSUHDialogType1Dialog("안내", "이메일을 입력해주세요.");
            return false; // 함수 종료

        } else if (useFul.isExistEmptySpace(emailString)) {

            // 이메일에 공백이 들어가 있을 경우
            callSUHDialogType1Dialog("안내", "이메일에는 공백이 들어갈 수 없습니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else if (!useFul.isValidEmail(emailString)) {

            // 이메일 형식이 올바르지 않은 경우
            callSUHDialogType1Dialog("안내", "이메일 형식이 올바르지 않습니다. 다시 확인해주세요.");
            return false; // 함수 종료

        } else {
            checkedEmail = emailString; // 체크된 이메일에 저장
        }

        ContentValues params = new ContentValues();
        params.put("act", "mobile2526");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("checkedEmail", checkedEmail);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2527);
        inLet.start();

        return true;
    }

    // 이메일 중복체크 요청 후처리
    public void mobile2527(Object obj) {
        String data = obj.toString();

        Log.i("FFFMMM", "data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);

            String emailUse = jsonObject.getString("emailUse");

            if (emailUse.equals("ok")) {
                isEmailOverLapChecked = true;
                callSUHDialogType1Dialog("안내", "이용 가능한 이메일 입니다.");
            } else {
                isEmailOverLapChecked = false;
                callSUHDialogType1Dialog("안내", "이미 가입되어 있는 이메일 입니다. 다른 이메일을 입력해주세요.");
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/

    public void callCustomSelectDialog(String title) {
        customSelectDialog.show();
        customSelectDialog.setTitle(title);
        customSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customSelectDialog.dismiss();
            }
        });
        customSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (customSelectDialog.isOnItemClicked) {
                    int selectIndex = customSelectDialog.getCurrentIndex();
                    currentSelectedGenderCode = genderIntegerArray[selectIndex];

                    genderDisplayText.setText(genderStringArray[selectIndex]);
                    genderDisplayText.setTextColor(Color.parseColor("#888888"));

                    customSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }






    /**************************************************************/

    View.OnClickListener theMobileCompanySelectButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callSUHDialogType2Dialog("통신사를 선택해주세요.");
        }
    };

    View.OnClickListener theBloodTypeSelectButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callSUHDialogType3Dialog("혈액형을 선택해주세요.");
        }
    };



    View.OnClickListener genderSelectButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCustomSelectDialog("성별을 선택해주세요.");
        }
    };

    View.OnClickListener emailOverLapCheckButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mobile2526(); // 이메일 중복체크 요청
        }
    };
}
