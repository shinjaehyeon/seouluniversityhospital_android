package org.snuh.www.app.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.departmentdoctor.Mobile5000;
import org.snuh.www.app.hospitalnews.Mobile8000;
import org.snuh.www.app.main.Mobile3000;
import org.snuh.www.app.medicalcertificate.Mobile7000;
import org.snuh.www.app.notice.Mobile7500;
import org.snuh.www.app.orderwaitingticket.Mobile6500;
import org.snuh.www.app.personalquestion.Mobile5500;
import org.snuh.www.app.profileinfoupdate.Mobile3500;
import org.snuh.www.app.setting.Mobile9000;
import org.snuh.www.app.signup.Mobile2500;

import java.util.ArrayList;

public class CustomDrawerLayout extends DrawerLayout {
    Context context;
    Activity activity;
    View rootView;

    /********************************************************************/

    int[] menuButtonIds = {
            R.id.menuButton1, R.id.menuButton2, R.id.menuButton3, R.id.menuButton4, R.id.menuButton5,
            R.id.menuButton6, R.id.menuButton7, R.id.menuButton8, R.id.menuButton9
    };


    ArrayList<Button> menuButtons = new ArrayList<Button>();

    /********************************************************************/

    public CustomDrawerLayout(Context context) {
        super(context);
        this.context = context;
        this.activity = (Activity) context;
        initView();
    }
    public CustomDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (Activity) context;
        initView();
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        rootView = li.inflate(R.layout.activity_drawer, this, false);
        addView(rootView);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        for (int i=0; i<menuButtonIds.length; i++) {
            menuButtons.add((Button) rootView.findViewById(menuButtonIds[i]));
            menuButtons.get(i).setOnClickListener(menuButtonEvent);
            menuButtons.get(i).setTag(i);
        }
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    View.OnClickListener menuButtonEvent = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (Integer.parseInt(v.getTag()+"")) {
                case 0: // 프로필 정보 수정
                    goToActivity(Mobile3500.class);
                    break;
                case 1: // 진료예약 신청
                    goToActivity(Mobile4500.class);
                    break;
                case 2: // 진료과 의료진
                    goToActivity(Mobile5000.class);
                    break;
                case 3: // 1:1 문의
                    goToActivity(Mobile5500.class);
                    break;
                case 4: // 순번대기표
                    goToActivity(Mobile6500.class);
                    break;
                case 5: // 진단서
                    goToActivity(Mobile7000.class);
                    break;
                case 6: // 알림 내역
                    goToActivity(Mobile7500.class);
                    break;
                case 7: // 병원뉴스
                    goToActivity(Mobile8000.class);
                    break;
                case 8: // 설정
                    goToActivity(Mobile9000.class);
                    break;
            }
        }
    };










    public void goToActivity(Class c) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // code..
                DrawerLayout drawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
                drawerLayout.closeDrawers();
            }
        }, 500);



        Intent intent = new Intent(activity, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
        //activity.finish();
    }
}
