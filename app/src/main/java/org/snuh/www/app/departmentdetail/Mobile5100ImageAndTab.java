package org.snuh.www.app.departmentdetail;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.io.ByteArrayOutputStream;

public class Mobile5100ImageAndTab extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;
    int department_pk;

    /**************************************************************/

    // String[] tabTitles = new String[]{"진료과 소개", "의료진 소개", "진료과 위치"};
    String[] tabTitles = new String[]{"진료과 소개", "의료진 소개"};
    TextView tab1;
    TextView tab2;
    // TextView tab3;

    Mobile5100ViewPager mobile5100ViewPager;

    ImageView imageView;
    TabLayout tabLayout;

    /**************************************************************/

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5102:
                    mobile5102(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    public Mobile5100ImageAndTab(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5100ImageAndTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5100ImageAndTab(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5100_image_and_tab, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        imageView = (ImageView) v.findViewById(R.id.imageView);
        tabLayout = (TabLayout) v.findViewById(R.id.tabLayout);


        tab1 = (TextView) LayoutInflater.from(activity).inflate(R.layout.tab, null);
        tab2 = (TextView) LayoutInflater.from(activity).inflate(R.layout.tab, null);
        // tab3 = (TextView) LayoutInflater.from(activity).inflate(R.layout.tab, null);

    }

    public void setRealInItView() {
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        tab1.setText(tabTitles[0]);
        tab2.setText(tabTitles[1]);
        // tab3.setText(tabTitles[2]);

        tabLayout.setupWithViewPager(mobile5100ViewPager.viewPager);
        tabLayout.addTab(tabLayout.newTab().setCustomView(tab1));
        tabLayout.addTab(tabLayout.newTab().setCustomView(tab2));
        // tabLayout.addTab(tabLayout.newTab().setCustomView(tab3));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(useFul.getRcolor(R.color.mobile5100_tab_button_text_normal_color), useFul.getRcolor(R.color.mobile5100_tab_button_text_on_color));
        tabLayout.setSelectedTabIndicatorHeight(useFul.getRdimen(R.dimen.mobile5100_tab_bottom_line_height));
        tabLayout.setSelectedTabIndicatorColor(useFul.getRcolor(R.color.mobile5100_tab_bottom_line_color));
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        tabLayout.addOnTabSelectedListener(tabSelectEventListener);
    }

    TabLayout.OnTabSelectedListener tabSelectEventListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            // viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    /**************************************************************/

    // 진료과 이미지 요청
    public void mobile5101() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5101");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("department_pk", department_pk);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        inLet.setHandler(handler);
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandlerRequestNumber(5102);
        inLet.start();
    }

    // 진료과 이미지 요청 후처리
    public void mobile5102(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                int note1 = jsonObject.getInt("note1");
                if (note1 == 1) {
                    String base64ImageString = jsonObject.getString("image");
                    String encodedString = base64ImageString;
                    String pureBase64Encoded = encodedString.substring(encodedString.indexOf(",")  + 1);
                    byte[] decodedBytes = Base64.decode(pureBase64Encoded, Base64.DEFAULT);

                    Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
                    imageView.setImageBitmap(decodedBitmap);
                }
            } else {

            }



        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void setMobile5100ViewPager(Mobile5100ViewPager mobile5100ViewPager) {
        this.mobile5100ViewPager = mobile5100ViewPager;
    }

    public void setDepartmentPk(int n) {
        this.department_pk = n;
    }
}
