package org.snuh.www.app.crhitemset;

import android.view.View;

public class CRHItem { // Clinic Reservation History Item
    public CRHItemViewHolder viewHolder;

    public int pk; // primarykey
    public String doctorName; // 의사 이름
    public String deparment; // 진료과 명칭
    public String clinicReservationDate; // 예약한 날짜
    public String clinicReservationTime; // 예약한 시간

    public int status; // 현 예약상태 (코드)
    public String statusString; // 현 예약상태 문자열

    public View.OnClickListener cancelButtonClickEvent; // 취소 버튼 클릭 시 이벤트

    public String year; // 예약한 년도
    public String month; // 예약한 월
    public String date; // 예약한 일

    /**************************************************************/

    public CRHItem() {

    }

    /**************************************************************/

    public void setPrimaryKey(int n) {
        this.pk = n;
    }
    public int getPk() {
        return this.pk;
    }

    public void setDoctorName(String s) {
        this.doctorName = s;
    }
    public String getDoctorName() {
        return this.doctorName;
    }

    public void setDeparment(String s) {
        this.deparment = s;
    }
    public String getDeparment() {
        return this.deparment;
    }

    public void setClinicReservationDate(String s) {
        this.clinicReservationDate = s;
    }
    public String getClinicReservationDate() {
        return this.clinicReservationDate;
    }

    public void setClinicReservationTime(String s) {
        this.clinicReservationTime = s;
    }
    public String getClinicReservationTime() {
        return this.clinicReservationTime;
    }

    public void setStatus(int n) {
        this.status = n;
    }
    public int getStatus() {
        return this.status;
    }

    public void setStatusString(String s) {
        this.statusString = s;
    }
    public String getStatusString() {
        return this.statusString;
    }

    public void setViewHolder(CRHItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public CRHItemViewHolder getViewHolder() {
        return this.viewHolder;
    }

    public void setCancelButtonClickEvent(View.OnClickListener e) {
        this.cancelButtonClickEvent = e;
    }
    public View.OnClickListener getCancelButtonClickEvent() {
        return this.cancelButtonClickEvent;
    }




    public void setYear(String year) {
        this.year = year;
    }
    public String getYear() {
        return this.year;
    }

    public void setMonth(String month) {
        this.month = month;
    }
    public String getMonth() {
        return this.month;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getDate() {
        return this.date;
    }



}
