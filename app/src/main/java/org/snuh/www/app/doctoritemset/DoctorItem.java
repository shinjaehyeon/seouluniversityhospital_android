package org.snuh.www.app.doctoritemset;

import org.snuh.www.app.departmentitemset.DepartmentItemViewHolder;

public class DoctorItem {
    public int pk;
    public String doctorName;
    public DoctorItemViewHolder viewHolder;

    public void setPk(int n) {
        this.pk = n;
    }
    public int getPk() {
        return this.pk;
    }

    public void setDoctorName(String s) {
        this.doctorName = s;
    }
    public String getDoctorName() {
        return this.doctorName;
    }

    public void setViewHolder(DoctorItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public DoctorItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
