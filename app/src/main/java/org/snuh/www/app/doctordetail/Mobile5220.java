package org.snuh.www.app.doctordetail;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.historyitemset.HistoryItem;
import org.snuh.www.app.historyitemset.HistoryItemAdapter;
import org.snuh.www.app.hospitalnewsitemset.HospitalNewsItem;
import org.snuh.www.app.prescriptiondrugitemset.PrescriptionDrugItem;
import org.snuh.www.app.prescriptiondrugitemset.PrescriptionDrugItemAdapter;
import org.snuh.www.app.questionitemset.QuestionItemAdapter;

import java.util.ArrayList;

public class Mobile5220 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    int doctor_pk;

    TextView content; // 의료진 한마디


    NestedScrollView nestedScrollView;


    LinearLayout school_area;
    LinearLayout career_area;


    RecyclerView recyclerView1;
    private RecyclerView.LayoutManager mLayoutManager1;
    ArrayList<HistoryItem> items1 = new ArrayList<HistoryItem>();
    HistoryItemAdapter itemAdapter1;


    RecyclerView recyclerView2;
    private RecyclerView.LayoutManager mLayoutManager2;
    ArrayList<HistoryItem> items2 = new ArrayList<HistoryItem>();
    HistoryItemAdapter itemAdapter2;




    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5222:
                    mobile5222(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };



    public Mobile5220() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5220(Context context) {
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }


    public void setDoctorPk(int n) {
        this.doctor_pk = n;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mobile5220, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        setRecyclerView();

        mobile5221();

        return v;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        content = (TextView) v.findViewById(R.id.content);

        school_area = (LinearLayout) v.findViewById(R.id.school_area);
        career_area = (LinearLayout) v.findViewById(R.id.career_area);

        recyclerView1 = (RecyclerView) v.findViewById(R.id.recyclerView1);
        recyclerView2 = (RecyclerView) v.findViewById(R.id.recyclerView2);
    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }


    public void setNestedScrollView(NestedScrollView scrollView) {
        nestedScrollView = scrollView;
    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView1.setNestedScrollingEnabled(false);
        recyclerView2.setNestedScrollingEnabled(false);

        mLayoutManager1 = new GridLayoutManager(activity,1);
        mLayoutManager1.setAutoMeasureEnabled(true);

        mLayoutManager2 = new GridLayoutManager(activity,1);
        mLayoutManager2.setAutoMeasureEnabled(true);

        recyclerView1.setLayoutManager(mLayoutManager1);
        recyclerView2.setLayoutManager(mLayoutManager2);

        itemAdapter1 = new HistoryItemAdapter(recyclerView1, items1, activity, null, nestedScrollView);
        recyclerView1.setAdapter(itemAdapter1);

        itemAdapter2 = new HistoryItemAdapter(recyclerView2, items2, activity, null, nestedScrollView);
        recyclerView2.setAdapter(itemAdapter2);

    }






    // 서버에 의료진 기본정보 요청
    public void mobile5221() {
        Log.i("MyTags", "mobile5221 실행됨");

        ContentValues params = new ContentValues();
        params.put("act", "mobile5221");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("doctor_pk", doctor_pk);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5222);
        inLet.start();
    }

    // 서버에 의료진 기본정보 요청 후처리
    public void mobile5222(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "mobile5222 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                String profile_info = jsonObject.getString("profile_info");
                String school_history = jsonObject.getString("school_history");
                String career = jsonObject.getString("career");

                content.setText(profile_info);


                if (school_history.length() >= 2) {
                    school_area.setVisibility(View.VISIBLE);

                    String[] lines = school_history.split(",");

                    for (int i=0; i<lines.length; i++) {
                        String[] set = lines[i].split(":");
                        String period = set[0];
                        String content = set[1];

                        HistoryItem item1 = new HistoryItem();
                        item1.setPeriod(period);
                        item1.setContent(content);

                        items1.add(item1);
                    }

                    itemAdapter1.setLoaded();
                    itemAdapter1.notifyDataSetChanged();

                } else {
                    school_area.setVisibility(View.GONE);
                }



                if (career.length() >= 2) {
                    career_area.setVisibility(View.VISIBLE);

                    String[] lines = career.split(",");

                    for (int i=0; i<lines.length; i++) {
                        String[] set = lines[i].split(":");
                        String period = set[0];
                        String content = set[1];

                        HistoryItem item2 = new HistoryItem();
                        item2.setPeriod(period);
                        item2.setContent(content);

                        items2.add(item2);
                    }

                    itemAdapter2.setLoaded();
                    itemAdapter2.notifyDataSetChanged();

                } else {
                    career_area.setVisibility(View.GONE);
                }



            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
