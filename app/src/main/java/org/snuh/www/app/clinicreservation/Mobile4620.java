package org.snuh.www.app.clinicreservation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;

import java.util.ArrayList;

public class Mobile4620 extends Fragment {
    Activity activity;
    View rootView;

    /**************************************************************/

    ArrayList<SubDepartmentItem> subDepartmentItems = new ArrayList<SubDepartmentItem>();
    Button department_select_button;
    TextView department_display_textview;
    EditText symptom_edittext;

    /**************************************************************/

    int currentSelectedDepartmentCode;
    String currentSelectedDepartmentString;

    SubDepartmentSelectDialog subDepartmentSelectDialog;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 4622: // 서버에 진료과 리스트 요청 후처리
                    mobile4622(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    Handler parentHandler;

    /**************************************************************/

    public Mobile4620() {

    }

    @SuppressLint("ValidFragment")
    public Mobile4620(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile4620, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        mobile4621(); // 서버에 진료과 리스트 요청

        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        subDepartmentSelectDialog = new SubDepartmentSelectDialog(activity);

        department_select_button = (Button) rootView.findViewById(R.id.department_select_button);
        department_display_textview = (TextView) rootView.findViewById(R.id.department_display_textview);
        symptom_edittext = (EditText) rootView.findViewById(R.id.symptom_edittext);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // 진료과 선택 박스 클릭 이벤트 연결하기
        department_select_button.setOnClickListener(departmentSelectButtonClickEvent);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    public void setHandler(Handler handler) {
        this.parentHandler = handler;
    }

    // 선택된 진료과 primaryKey 가져오기
    public int getCurrentSelectedDepartmentCode() {
        return currentSelectedDepartmentCode;
    }

    // 선택한 진료과 이름 문자열 가져오기
    public String getCurrentSelectedDepartmentString() {
        return this.currentSelectedDepartmentString;
    }

    // 증상 입력 박스에 입력된 값 가져오기
    public String getCurrentSymptomString() {
        String text = symptom_edittext.getText().toString();
        return text;
    }

    /**************************************************************/

    // 진료과 선택 박스 클릭 이벤트
    View.OnClickListener departmentSelectButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callSubDepartmentSelectDialog("진료과 선택");
        }
    };

    /**************************************************************/

    // 진료과 선택 다이얼로그 창 호출 함수
    public void callSubDepartmentSelectDialog(String title) {
        subDepartmentSelectDialog.show();
        subDepartmentSelectDialog.setTitle(title);
        subDepartmentSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subDepartmentSelectDialog.dismiss();
            }
        });
        subDepartmentSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (subDepartmentSelectDialog.isOnItemClicked) {
                    if (currentSelectedDepartmentCode != subDepartmentSelectDialog.currentSelectedDepartmentCode) {
                        currentSelectedDepartmentCode = subDepartmentSelectDialog.currentSelectedDepartmentCode;
                        currentSelectedDepartmentString = subDepartmentSelectDialog.currentSelectedDepartmentString;

                        Log.i("FinalTags", "mobile4623 실행");
                        mobile4623(); // 서버에 해당 진료과에 맞는 의료진 리스트 요청

                        department_display_textview.setText(currentSelectedDepartmentString + "");
                    }
                    subDepartmentSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    /**************************************************************/

    // 서버에 진료과 리스트 요청
    public void mobile4621() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile4621");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(4622);
        inLet.start();
    }

    // 서버에 진료과 리스트 요청 후처리
    public void mobile4622(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONArray ja = jsonObject.getJSONArray("data");
                for (int i=0; i<ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);

                    int suhd = jo.getInt("suhd");
                    String department = jo.getString("department");

                    SubDepartmentItem subDepartmentItem = new SubDepartmentItem();
                    subDepartmentItem.setCode(suhd);
                    subDepartmentItem.setDepartment(department);

                    subDepartmentItems.add(subDepartmentItem);
                }

                subDepartmentSelectDialog.setSubDepartmentItems(subDepartmentItems);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 서버에 해당 진료과에 맞는 의료진 리스트 요청
    public void mobile4623() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile4623");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("department", currentSelectedDepartmentCode);
        params.put("department_string", currentSelectedDepartmentString);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(parentHandler);
        inLet.setHandlerRequestNumber(4624);
        inLet.start();
    }
}
