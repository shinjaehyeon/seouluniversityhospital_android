package org.snuh.www.app.orderwaitingticket;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;

import java.util.Random;

public class ReceptionTicketService extends Service {
    int number;
    private Messenger mClient = null;   // Activity 에서 가져온 Messenger

    IBinder mBinder = new LocalBinder();
    class LocalBinder extends Binder {
        public ReceptionTicketService getService() { // 서비스 객체를 리턴
            return ReceptionTicketService.this;
        }
    }

    private final Random rand = new Random();

    private boolean isDestroy = false;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 6502: // 서버에 서울대학교 본원, 어린이병원, 암병원 각각 접수창구의 현재 번호 요청 후처리
                    mobile6502(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    // 컴포넌트에 난수를 반환해 줄 메소드
    public int getRandomNumber(){
        return rand.nextInt(1000);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        // 서비스에서 가장 먼저 호출됨(최초에 한번만)
        Log.d("test", "서비스의 onCreate");

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // 서비스가 호출될 때마다 실행
        Log.d("test", "서비스의 onStartCommand, number = "+number);
        number++;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 서비스가 종료될 때 실행
        Log.d("test", "서비스의 onDestroy");
        isDestroy = true;
    }


    /** activity로부터 binding 된 Messenger */
    private final Messenger mMessenger = new Messenger(new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.w("test","ControlService - message what : "+msg.what +" , msg.obj "+ msg.obj);
            switch (msg.what) {
                case 6500:
                    mClient = msg.replyTo;  // activity로부터 가져온
                    String data = msg.obj.toString();

                    mobile6501(); // 서버에 서울대학교 본원, 어린이병원, 암병원 각각 접수창구의 현재 번호 요청

                    break;
            }
            return false;
        }
    }));

    // 서버에 서울대학교 본원, 어린이병원, 암병원 각각 접수창구의 현재 번호 요청
    public void mobile6501() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile6501");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(getApplicationContext());
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(6502);
        inLet.start();
    }

    // 서버에 서울대학교 본원, 어린이병원, 암병원 각각 접수창구의 현재 번호 요청 후처리
    public void mobile6502(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "ReceptionTicketService data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);

            // 서울대학교 "본원" 현재 접수중인 번호
            int h_700201_counter_1_number = jsonObject.getInt("h_700201_counter_1_number");
            int h_700201_counter_2_number = jsonObject.getInt("h_700201_counter_2_number");
            int h_700201_counter_3_number = jsonObject.getInt("h_700201_counter_3_number");
            int h_700201_numbers[] = {h_700201_counter_1_number, h_700201_counter_2_number, h_700201_counter_3_number};

            // 서울대학교 "어린이병원" 현재 접수중인 번호
            int h_700202_counter_1_number = jsonObject.getInt("h_700202_counter_1_number");
            int h_700202_counter_2_number = jsonObject.getInt("h_700202_counter_2_number");
            int h_700202_counter_3_number = jsonObject.getInt("h_700202_counter_3_number");
            int h_700202_numbers[] = {h_700202_counter_1_number, h_700202_counter_2_number, h_700202_counter_3_number};

            // 서울대학교 "암병원" 현재 접수중인 번호
            int h_700203_counter_1_number = jsonObject.getInt("h_700203_counter_1_number");
            int h_700203_counter_2_number = jsonObject.getInt("h_700203_counter_2_number");
            int h_700203_counter_3_number = jsonObject.getInt("h_700203_counter_3_number");
            int h_700203_numbers[] = {h_700203_counter_1_number, h_700203_counter_2_number, h_700203_counter_3_number};

            // 내번호 정보
            int my_number_h_type = jsonObject.getInt("my_number_h_type"); // 현재 내 번호의 병원 종류
            int my_number = jsonObject.getInt("my_number"); // 현재 내 번호
            int my_number_status = jsonObject.getInt("my_number_status"); // 현재 내 번호 상태
            int my_number_infos[] = {my_number_h_type, my_number, my_number_status};

            sendMsgToActivity(h_700201_numbers, h_700202_numbers, h_700203_numbers, my_number_infos);

            if (isDestroy == false) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("MyTags", "mobile6501() 또 호출됨");
                        mobile6501();
                    }
                }, 2000);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendMsgToActivity(int[] h_700201_numbers, int[] h_700202_numbers, int[] h_700203_numbers, int my_number_infos[]) {
        try {
            Bundle bundle = new Bundle();
            bundle.putInt("h_700201_counter_1_number", h_700201_numbers[0]);
            bundle.putInt("h_700201_counter_2_number", h_700201_numbers[1]);
            bundle.putInt("h_700201_counter_3_number", h_700201_numbers[2]);

            bundle.putInt("h_700202_counter_1_number", h_700202_numbers[0]);
            bundle.putInt("h_700202_counter_2_number", h_700202_numbers[1]);
            bundle.putInt("h_700202_counter_3_number", h_700202_numbers[2]);

            bundle.putInt("h_700203_counter_1_number", h_700203_numbers[0]);
            bundle.putInt("h_700203_counter_2_number", h_700203_numbers[1]);
            bundle.putInt("h_700203_counter_3_number", h_700203_numbers[2]);

            bundle.putInt("my_number_h_type", my_number_infos[0]);
            bundle.putInt("my_number", my_number_infos[1]);
            bundle.putInt("my_number_status", my_number_infos[2]);
            Message msg = Message.obtain(null, 6500);
            msg.setData(bundle);

            mClient.send(msg);   // msg 보내기
        } catch (RemoteException e) {
        }
    }

}
