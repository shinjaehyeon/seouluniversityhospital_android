package org.snuh.www.app.departmentdetail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

public class Mobile5120 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    int department_pk;

    TextView content;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5122:
                    mobile5122(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    public Mobile5120() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5120(Context context) {
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }


    public void setDepartmentPk(int n) {
        this.department_pk = n;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mobile5120, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        return v;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        content = (TextView) v.findViewById(R.id.content);
    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 서버에 진료과 소개글/설명글 요청
    public void mobile5121() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5121");
        params.put("member", new ShareInfo(activity).getMember());
        Log.i("MyTags", "mobile5122 department_pk = "+department_pk);
        params.put("department_pk", department_pk);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5122);
        inLet.start();
    }

    // 서버에 진료과 소개글/설명글 요청 후처리
    public void mobile5122(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "mobile5122 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                final String description = jsonObject.getString("description");

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        content.setText(description);
                    }
                });
            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }





}
