package org.snuh.www.app.firebase;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.util.TypedValue;
import android.widget.RemoteViews;

import com.google.firebase.messaging.RemoteMessage;

import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.departmentdoctor.Mobile5000;
import org.snuh.www.app.hospitalnews.Mobile8000;
import org.snuh.www.app.passwordcheck.Mobile0500;
import org.snuh.www.app.personalquestion.Mobile5500;
import org.snuh.www.app.profileinfoupdate.Mobile3500;
import org.snuh.www.app.splash.Mobile1000;

import java.util.ArrayList;
import java.util.List;


public class FCMListenerService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FCM";

    int mLastId = 0;
    ArrayList<Integer> mActiveIdList = new ArrayList<Integer>();
    NotificationManager nm;

    private NotificationChannel mChannel;
    private NotificationManager notifManager;

    //SharedPreferences sharedpreferences = getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        // 메시지를 받았을 때 동작하는 메소드
        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("message");
        String push_group_pk = remoteMessage.getData().get("push_group_pk");
        String linkPage = remoteMessage.getData().get("linkPage");
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Title: " + title);
        Log.d(TAG, "Messpush_group_pkage: " + message);
        Log.d(TAG, "push_group_pk: " + push_group_pk);
        Log.d(TAG, "linkPage: " + linkPage);

        //int pushState = sharedpreferences.getInt("pushState", 0);
        sendPushNotification(title, message, push_group_pk, linkPage);
//        if (pushState == 900100) {
//            sendPushNotification(title, message, pk);
//        } else {
//
//        }
    }

    private void createNotificationId() {
        int id = ++mLastId;
        mActiveIdList.add(id);
    }

    public void sendPushNotification(String title, String message, String push_group_pk, String linkPage) {
        createNotificationId();


        // 큰 메세지 박스 설정하기 (푸쉬 내용 더보기 가능)
        RemoteViews bigView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification_layout_big);
        bigView.setTextViewText(R.id.push_title, title);
        bigView.setTextViewTextSize(R.id.push_title, TypedValue.COMPLEX_UNIT_DIP, 14f);
        bigView.setTextViewText(R.id.push_content, message);
        bigView.setTextViewTextSize(R.id.push_content, TypedValue.COMPLEX_UNIT_DIP, 14f);


        // 푸쉬 메세지 박스 셋팅
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder;
        notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // 안드로이드 버전이 8.0 이상이면 채널을 설정해줘야 한다.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            if (mChannel == null) {
                mChannel = new NotificationChannel("0", title, importance);
                mChannel.setLightColor(Color.rgb(221,93,94));
                mChannel.setShowBadge(true);
                mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                mChannel.setSound(defaultSoundUri, audioAttributes);
                mChannel.setDescription(message);
                mChannel.enableVibration(true);
                notifManager.createNotificationChannel(mChannel);
            }
        }

        builder = new NotificationCompat.Builder(this, "0");

        builder.setSmallIcon(getNotificationIcon()) // required
                .setContentTitle(title)
                .setContentText(message)  // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setLights(Color.argb(1,221,93,94), 500, 2000)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round))
                .setBadgeIconType(R.mipmap.ic_launcher_round)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomBigContentView(bigView);


        switch (Integer.parseInt(linkPage)) {
            case 600501: // 없음
                Intent intent = new Intent(getApplicationContext(), Mobile0500.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // intent.putExtra("cardnews_primaryKey", Integer.parseInt(pagevalue));

                PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(resultPendingIntent);
                break;
            case 600502: // 병원뉴스
                if (!isAppRunning(getApplicationContext())) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile0500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPageNumber", 8000);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setFullScreenIntent(resultPendingIntent2, true);


                    // builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile8000.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setFullScreenIntent(resultPendingIntent2, true);


                    // builder.setContentIntent(resultPendingIntent2);
                }
                break;
            case 600503: // 프로필정보업데이트
                if (!isAppRunning(getApplicationContext())) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile0500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPageNumber", 3500);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile3500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                }
                break;
            case 600504: // 진료예약신청
                if (!isAppRunning(getApplicationContext())) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile0500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPageNumber", 4600);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile4500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("pageIndex", 0);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                }
                break;
            case 600505: // 진료예약내역
                if (!isAppRunning(getApplicationContext())) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile0500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPageNumber", 4700);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile4500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("pageIndex", 1);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                }
                break;
            case 600506: // 진료과
                if (!isAppRunning(getApplicationContext())) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile0500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPageNumber", 5020);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile5000.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("pageIndex", 0);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                }
                break;
            case 600507: // 의료진
                if (!isAppRunning(getApplicationContext())) {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile0500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("linkPageNumber", 5040);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                } else {
                    Intent intent2 = new Intent(getApplicationContext(), Mobile5000.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent2.putExtra("pageIndex", 1);

                    PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);
                    builder.setContentIntent(resultPendingIntent2);
                }
                break;
            default:

                break;
        }


        Notification notification = builder.build();
        notifManager.notify(0, notification);

    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher_round : R.mipmap.ic_launcher_round;
    }

    private boolean isAppRunning(Context context){
//        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
//        for(int i = 0; i < procInfos.size(); i++){
//            if(procInfos.get(i).processName.equals(context.getPackageName())){
//                return true;
//            }
//        }
//        return false;

        if (new ShareInfo(context).getAppRunning() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void showNotification(Context context, String title, String body, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(notificationId, mBuilder.build());
    }
}
