package org.snuh.www.app.departmentitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.w3c.dom.Text;

public class DepartmentItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public Button button;
    public TextView department_textview;

    /**************************************************************/

    public DepartmentItemViewHolder(View view) {
        super(view);
        this.view = view;

        button = (Button) view.findViewById(R.id.button);
        department_textview = (TextView) view.findViewById(R.id.department_textview);
    }
}
