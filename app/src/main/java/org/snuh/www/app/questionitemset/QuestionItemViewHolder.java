package org.snuh.www.app.questionitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class QuestionItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public Button button;

    public TextView datetime_textview;
    public TextView question_type_textview;
    public TextView question_title_textview;

    public Button current_status;

    /**************************************************************/

    public QuestionItemViewHolder(View view) {
        super(view);
        this.view = view;

        button = (Button) view.findViewById(R.id.button);

        datetime_textview = (TextView) view.findViewById(R.id.datetime_textview);
        question_type_textview = (TextView) view.findViewById(R.id.question_type_textview);
        question_title_textview = (TextView) view.findViewById(R.id.question_title_textview);

        current_status = (Button) view.findViewById(R.id.current_status);
    }

}
