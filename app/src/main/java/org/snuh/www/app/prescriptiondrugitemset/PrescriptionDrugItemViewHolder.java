package org.snuh.www.app.prescriptiondrugitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class PrescriptionDrugItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public TextView drug_primarykey; // 약품 고유번호
    public TextView drug_name; // 약품 이름

    public TextView oneday_dose; // 하루 투여량
    public TextView oneday_number; // 하루 투여횟수
    public TextView dose_day_num; // 총 투약일수

    public TextView note; // 의사소견

    /**************************************************************/

    public PrescriptionDrugItemViewHolder(View view) {
        super(view);
        this.view = view;

        drug_primarykey = (TextView) view.findViewById(R.id.drug_primarykey);
        drug_name = (TextView) view.findViewById(R.id.drug_name);
        oneday_dose = (TextView) view.findViewById(R.id.oneday_dose);
        oneday_number = (TextView) view.findViewById(R.id.oneday_number);
        dose_day_num = (TextView) view.findViewById(R.id.dose_day_num);
        note = (TextView) view.findViewById(R.id.note);
    }
}
