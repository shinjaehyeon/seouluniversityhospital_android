package org.snuh.www.app.notice;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomSelectDialog;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdoctor.Mobile5000;
import org.snuh.www.app.hospitalnews.Mobile8000;
import org.snuh.www.app.main.Mobile3000;
import org.snuh.www.app.medicalcertificateitemset.MedicalCertificateItem;
import org.snuh.www.app.noticeitemset.NoticeItem;
import org.snuh.www.app.noticeitemset.NoticeItemAdapter;
import org.snuh.www.app.profileinfoupdate.Mobile3500;
import org.snuh.www.app.questionitemset.QuestionItem;
import org.snuh.www.app.questionitemset.QuestionItemAdapter;
import org.snuh.www.app.splash.Mobile1000;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile7500 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    CustomSelectDialog customSelectDialog;

    /**************************************************************/

    TextView recent_day_textview;
    Button period_setting_button;

    TextView result_none;

    int periodSizes[] = {0, 7, 30, 60, 90, 150};
    String periodSizeString[] = {"전체 기간", "최근 7일",  "최근 30일", "최근 60일", "최근 90일", "최근 150일"};
    int period_size = 30; // 최근 n일
    int period_index = 2;

    /**************************************************************/

    int index = 0;
    int view_num = 10;

    NestedScrollView nestedScrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<NoticeItem> items = new ArrayList<NoticeItem>();
    NoticeItemAdapter itemAdapter;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 7502: // 서버에 받은 푸쉬 목록 요청 후처리
                    mobile7502(msg.obj);
                    break;
                case 7504: // 서비에 푸쉬 읽음 처리 요청 후처리
                    mobile7504(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile7500);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기

        setRecyclerView();
        mobile7501();
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);

        customSelectDialog = new CustomSelectDialog(activity);

        recent_day_textview = (TextView) findViewById(R.id.recent_day_textview);
        period_setting_button = (Button) findViewById(R.id.period_setting_button);

        result_none = (TextView) findViewById(R.id.result_none);

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {
        customSelectDialog.setFirstValue(2);
        customSelectDialog.setSelectionList(periodSizeString, periodSizes);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        period_setting_button.setOnClickListener(periodSettingButtonEvent);
    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }


    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new NoticeItemAdapter(recyclerView, items, activity, itemOnClickListener, nestedScrollView);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile7501(); // 서버에 받은 푸쉬 목록 요청
            }
        });

        // 스크롤로 더보기 구현하고 싶으면
        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollViewPos = nestedScrollView.getScrollY();
                int TextView_lines = nestedScrollView.getChildAt(0).getBottom() - nestedScrollView.getHeight();

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();


                    if (TextView_lines == scrollViewPos) {
                        if (itemAdapter.isMoreInfo == true) {
                            itemAdapter.totalItemCount = linearLayoutManager.getItemCount();
                            itemAdapter.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                            if (!itemAdapter.isLoading && itemAdapter.totalItemCount <= (itemAdapter.lastVisibleItem + itemAdapter.visibleThreshold)) {
                                itemAdapter.isLoading = true;

                                if (itemAdapter.onLoadMoreListener != null) {
                                    itemAdapter.onLoadMoreListener.onLoadMore();
                                }

                            }
                        } else {
                            callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.");
                        }
                    }


            }
        });

    }

    // 서버에 받은 푸쉬 목록 요청
    public void mobile7501() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile7501");
        params.put("member", new ShareInfo(this).getMember());
        params.put("index", index);
        params.put("view_num", view_num);
        params.put("period_size", period_size);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(7502);
        inLet.start();
    }

    // 서버에 받은 푸쉬 목록 요청 후처리
    public void mobile7502(Object obj) {
        String data = obj.toString();

        Log.i("MyTags", "mobile7002 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                result_none.setVisibility(View.GONE);
                String more_info = jsonObject.getString("more_info");

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int primarykey = jsonObject1.getInt("suhpp");
                    int groupPrimaryKey = jsonObject1.getInt("suhpg");
                    String title = jsonObject1.getString("title");
                    String content = jsonObject1.getString("content");
                    int status = jsonObject1.getInt("status");
                    int link_page = jsonObject1.getInt("link_page");
                    String push_get_date = jsonObject1.getString("push_get_date");
                    String push_get_time = jsonObject1.getString("push_get_time");
                    int passed_day = jsonObject1.getInt("passed_day");
                    int passed_hour = jsonObject1.getInt("passed_hour");
                    int passed_minute = jsonObject1.getInt("passed_minute");

                    NoticeItem item = new NoticeItem();
                    item.setPrimaryKey(primarykey);
                    item.setTitle(title);
                    item.setContent(content);
                    item.setStatus(status);
                    item.setLinkPage(link_page);
                    item.setGetDate(push_get_date);
                    item.setGetTime(push_get_time);
                    item.setPassedDate(passed_day);
                    item.setPassedHour(passed_hour);
                    item.setPassedMinute(passed_minute);


                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;


            } else {
                result_none.setVisibility(View.VISIBLE);
                itemAdapter.isMoreInfo = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 서버에 푸쉬 읽음 처리 요청
    public void mobile7503(int primarykey) {
        ContentValues params = new ContentValues();
        params.put("act", "mobile7503");
        params.put("member", new ShareInfo(this).getMember());
        params.put("push_pk", primarykey);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(7504);
        inLet.start();
    }

    // 서비에 푸쉬 읽음 처리 요청 후처리
    public void mobile7504(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {

            } else {

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/

    View.OnClickListener periodSettingButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCustomSelectDialog("기간 선택");
        }
    };

    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            NoticeItem item = (NoticeItem) v.getTag();
            int push_personal_pk = item.getPrimaryKey();
            int status = item.getStatus();
            int linkType = item.getLineType();
            int linkPage = item.getLinkPage();

            Log.i("MyTags", "linkPage = "+linkPage);

            String linkValue = item.linkValue;


            if (status == 600202) {
                // 푸쉬가 전송된 상태(읽지 않음)일 경우
                // 푸쉬 읽음 쿼리 요청
                mobile7503(push_personal_pk); // 서버에 푸쉬 읽음 처리 요청
                item.getViewHolder().setUnActiveEffect(); // 비활성화 효과 주기 (읽었다는 표시)
            } else {
                // 푸쉬가 읽음 상태일 경우
                // 아무것도 안함
            }


            // 링크타입에 따라 링크 값에 따라 액티비티로 이동하게하는 코드 작성 필요
            switch (linkPage) {
                case 600501: // 없음

                    break;
                case 600502: // 병원뉴스
                    Intent intent1 = new Intent(getApplicationContext(), Mobile8000.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    break;
                case 600503: // 프로필정보업데이트
                    Intent intent2 = new Intent(getApplicationContext(), Mobile3500.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    break;
                case 600504: // 진료예약신청
                    Intent intent3 = new Intent(getApplicationContext(), Mobile4500.class);
                    intent3.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent3.putExtra("pageIndex", 0);
                    startActivity(intent3);
                    break;
                case 600505: // 진료예약내역
                    Intent intent4 = new Intent(getApplicationContext(), Mobile4500.class);
                    intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent4.putExtra("pageIndex", 1);
                    startActivity(intent4);
                    break;
                case 600506: // 진료과
                    Intent intent5 = new Intent(getApplicationContext(), Mobile5000.class);
                    intent5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent5.putExtra("pageIndex", 0);
                    startActivity(intent5);
                    break;
                case 600507: // 의료진
                    Intent intent6 = new Intent(getApplicationContext(), Mobile5000.class);
                    intent6.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent6.putExtra("pageIndex", 1);
                    startActivity(intent6);
                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callCustomSelectDialog(String title) {
        customSelectDialog.show();
        customSelectDialog.setTitle(title);
        customSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customSelectDialog.dismiss();
            }
        });
        customSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (customSelectDialog.isOnItemClicked) {
                    period_index = customSelectDialog.getCurrentIndex();
                    period_size = periodSizes[period_index];

                    recent_day_textview.setText(periodSizeString[period_index]+"기준 ");

                    allClear();
                    mobile7501();

                    customSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    /**************************************************************/

    public void allClear() {
        index = 0;
        items.clear();
        itemAdapter.isMoreInfo = true;
        itemAdapter.notifyDataSetChanged();
        recyclerView.removeAllViews();
    }























    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
