package org.snuh.www.app;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    EditText edittext;
    EditText edittext2;
    EditText edittext3;
    EditText edittext4;
    EditText edittext5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edittext = (EditText) findViewById(R.id.edittext);
        edittext2 = (EditText) findViewById(R.id.edittext2);
        edittext3 = (EditText) findViewById(R.id.edittext3);
        edittext4 = (EditText) findViewById(R.id.edittext4);
        edittext5 = (EditText) findViewById(R.id.edittext5);


        try {
            Log.i("MyTags", "키해시 시작");
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                edittext.setText(Base64.encodeToString(md.digest(), Base64.DEFAULT)+"");
                edittext2.setText(Base64.encodeToString(md.digest(), Base64.NO_CLOSE)+"");
                edittext3.setText(Base64.encodeToString(md.digest(), Base64.NO_PADDING)+"");
                edittext4.setText(Base64.encodeToString(md.digest(), Base64.NO_WRAP)+"");
                edittext5.setText(Base64.encodeToString(md.digest(), Base64.URL_SAFE)+"");
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
