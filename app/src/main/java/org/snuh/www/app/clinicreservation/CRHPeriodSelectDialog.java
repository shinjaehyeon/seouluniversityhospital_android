package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class CRHPeriodSelectDialog extends Dialog {
    Context context;
    UseFul useFul;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    int buttonIdResources[] = {R.id.button_index_0,R.id.button_index_1,R.id.button_index_2,R.id.button_index_3,R.id.button_index_4,R.id.button_index_5};
    public ArrayList<Button> buttons = new ArrayList<Button>();

    Button previousSelectedButton;

    /**************************************************************/

    int periodSizes[] = {0, 7, 30, 60, 90, 150};
    String periodSizeString[] = {"전체 기간", "최근 7일",  "최근 30일", "최근 60일", "최근 90일", "최근 150일"};
    int period_size = 30;
    public int period_index = 2;

    int first_period_index = -1;

    /**************************************************************/


    public CRHPeriodSelectDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.crh_period_select_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        if (first_period_index != -1) {
            buttons.get(first_period_index).setBackgroundResource(R.drawable.ripple_button_type_2);
            buttons.get(first_period_index).setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_on_color));

            previousSelectedButton = (Button) buttons.get(first_period_index);
        }
    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(context);


        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        for (int i=0; i<buttonIdResources.length; i++) {
            buttons.add((Button) findViewById(buttonIdResources[i]));
        }
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<buttons.size(); i++) {
            buttons.get(i).setTag(i);
            buttons.get(i).setOnClickListener(itemOnClickListener);
        }
    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public void setFirstValue(int n) {
        this.first_period_index = n;
    }

    /**************************************************************/


    /**************************************************************/

    public boolean isOnItemClicked = false;

    // 진료과 리스트중 하나를 클릭 했을 시 발생하는 이벤트 설정
    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isOnItemClicked = true;

            int index = (int) v.getTag();
            period_index = index;

            ((Button) v).setBackgroundResource(R.drawable.ripple_button_type_2);
            ((Button) v).setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_on_color));

            if (previousSelectedButton != null) {
                if ((int) previousSelectedButton.getTag() != (int) ((Button) v).getTag()) {
                    previousSelectedButton.setBackgroundResource(R.drawable.ripple_button_type_6);
                    previousSelectedButton.setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_color));
                }
            }

            previousSelectedButton = (Button) v;

            dismiss();
        }
    };


}
