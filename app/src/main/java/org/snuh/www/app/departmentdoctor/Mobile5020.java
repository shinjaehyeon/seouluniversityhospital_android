package org.snuh.www.app.departmentdoctor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4620;
import org.snuh.www.app.clinicreservation.Mobile4640;
import org.snuh.www.app.clinicreservation.Mobile4660;
import org.snuh.www.app.clinicreservation.SubDoctorItem;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class Mobile5020 extends Fragment {
    Activity activity;
    View rootView;
    UseFul useFul;

    /**************************************************************/

    LinearLayout filter_content_area;
    LinearLayout recyclerView_area;

    Mobile5020Filter mobile5020Filter;
    Mobile5020RecyclerView mobile5020RecyclerView;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 9999:

                    break;
                default:

                    break;
            }
        }
    };

    /**************************************************************/

    public Mobile5020() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5020(Activity activity) {
        this.activity = activity;
        useFul = new UseFul(activity);
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile5020, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        filter_content_area = (LinearLayout) rootView.findViewById(R.id.filter_content_area);
        recyclerView_area = (LinearLayout) rootView.findViewById(R.id.recyclerView_area);



        mobile5020Filter = new Mobile5020Filter(activity);
        mobile5020Filter.inflateView();
        mobile5020Filter.setConnectViewId();
        mobile5020RecyclerView = new Mobile5020RecyclerView(activity);
        mobile5020RecyclerView.inflateView();
        mobile5020RecyclerView.setConnectViewId();


        mobile5020Filter.setMobile5020RecyclerView(mobile5020RecyclerView);
        mobile5020RecyclerView.setMobile5020Filter(mobile5020Filter);


        mobile5020Filter.setRealInItView();
        mobile5020RecyclerView.setRealInItView();
        mobile5020RecyclerView.mobile5021();

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                filter_content_area.addView(mobile5020Filter);
            }
        });

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView_area.addView(mobile5020RecyclerView);
            }
        });


    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

}
