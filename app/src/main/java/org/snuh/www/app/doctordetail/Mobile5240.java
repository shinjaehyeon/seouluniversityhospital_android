package org.snuh.www.app.doctordetail;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.commentitemset.CommentItem;
import org.snuh.www.app.commentitemset.CommentItemAdapter;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class Mobile5240 extends Fragment {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    int index = 0;
    int view_num = 6;

    int doctor_pk;

    NestedScrollView nestedScrollView;
    EditText comment_edittext;
    Button comment_upload_button;


    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<CommentItem> items = new ArrayList<CommentItem>();
    CommentItemAdapter itemAdapter;

    TextView result_none;
    Button moreViewButton;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            super.handleMessage(msg);

            final Object obj = msg.obj;

            switch (msg.what) {
                case 5242: // 서버에 해당 의료진의 감사댓글 리스트 요청 후처리
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mobile5242(obj);
                        }
                    });

                    break;
                case 5244: // 서버에 해당 의료진 댓글 DB에 업로드 요청 후처리
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mobile5244(obj);
                        }
                    });
                    break;

                default:

                    break;
            }
        }
    };

    public Mobile5240() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5240(Context context) {
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }


    public void setDoctorPk(int n) {
        this.doctor_pk = n;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mobile5240, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        setRecyclerView();
        mobile5241();

        return v;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        comment_edittext = (EditText) v.findViewById(R.id.comment_edittext);
        comment_upload_button = (Button) v.findViewById(R.id.comment_upload_button);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        result_none = (TextView) v.findViewById(R.id.result_none);
        moreViewButton = (Button) v.findViewById(R.id.moreViewButton);
    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        comment_upload_button.setOnClickListener(uploadButtonEvent);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new CommentItemAdapter(recyclerView, items, activity, null, nestedScrollView);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile5241(); // 서버에 해당 의료진의 감사댓글 리스트 요청
            }
        });

        // 버튼으로 더보기 구현하고 싶으면
        moreViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter.isMoreInfo == true) {
                    if (!itemAdapter.isLoading) {
                        itemAdapter.isLoading = true;
                        if (itemAdapter.onLoadMoreListener != null) {
                            itemAdapter.onLoadMoreListener.onLoadMore();
                        }
                    }
                } else {
                    callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.");
                }
            }
        });
    }

    // 서버에 해당 의료진의 감사댓글 리스트 요청
    public void mobile5241() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5241");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("doctor_pk", doctor_pk);
        params.put("index", index);
        params.put("view_num", view_num);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5242);
        inLet.start();
    }

    // 서버에 해당 의료진의 감사댓글 리스트 요청 후처리
    public void mobile5242(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                result_none.setVisibility(View.GONE);

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                String more_info = jsonObject.getString("more_info");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int comment_pk = jsonObject1.getInt("comment_pk");
                    int member_pk = jsonObject1.getInt("member_pk");
                    String member_name = jsonObject1.getString("member_name");
                    String datetime = jsonObject1.getString("datetime");
                    String comment = jsonObject1.getString("comment");

                    // code..
                    CommentItem item = new CommentItem();
                    item.setCommentPrimaryKey(comment_pk);
                    item.setMemberPrimaryKey(member_pk);
                    item.setMemberName(member_name);
                    item.setDatetimeString(datetime);
                    item.setComment(comment);

                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;

            } else {
                itemAdapter.isMoreInfo = false;
                result_none.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void allClear() {
        index = 0;
        items.clear();
        itemAdapter.isMoreInfo = true;
        itemAdapter.notifyDataSetChanged();
        recyclerView.removeAllViews();
    }

    // 서버에 해당 의료진 댓글 DB에 업로드 요청
    public void mobile5243() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5243");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("doctor", doctor_pk);
        params.put("comment", comment_edittext.getText().toString());


        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5244);
        inLet.start();
    }

    // 서버에 해당 의료진 댓글 DB에 업로드 요청 후처리
    public void mobile5244(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                comment_edittext.setText("");
                useFul.hideKeypad();

                allClear();
                mobile5241();
            } else {
                callSUHDialogType1Dialog("안내", "댓글 업로드중에 오류가 발생하였습니다.");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }












    // 댓글달기 버튼 클릭 시 이벤트 내용
    View.OnClickListener uploadButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (comment_edittext.getText().toString().length() <= 2) {
                callSUHDialogType1Dialog("안내", "댓글 내용은 최소 3자 이상이어야 합니다.");
            } else {
                mobile5243(); // 서버에 해당 의료진 댓글 DB에 업로드 요청
            }
        }
    };


    public void setNestedScrollView(NestedScrollView nestedScrollView) {
        this.nestedScrollView = nestedScrollView;
    }


    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }
}
