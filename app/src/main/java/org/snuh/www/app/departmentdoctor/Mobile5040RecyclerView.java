package org.snuh.www.app.departmentdoctor;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdetail.Mobile5100;
import org.snuh.www.app.departmentitemset.DepartmentItem;
import org.snuh.www.app.departmentitemset.DepartmentItemAdapter;
import org.snuh.www.app.doctordetail.Mobile5200;
import org.snuh.www.app.doctoritemset.DoctorItem;
import org.snuh.www.app.doctoritemset.DoctorItemAdapter;

import java.util.ArrayList;

public class Mobile5040RecyclerView extends LinearLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;

    Mobile5040Filter mobile5040Filter;

    int currentSelectedDepartmentCode;
    String currentSelectedDepartmentString;

    int index = 0;
    int view_num = 6;

    NestedScrollView scrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<DoctorItem> items = new ArrayList<DoctorItem>();
    DoctorItemAdapter itemAdapter;


    TextView result_none;
    Button moreViewButton;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5042:
                    mobile5042(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    public Mobile5040RecyclerView(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5040RecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5040RecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5040_recyclerview, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        result_none = (TextView) v.findViewById(R.id.result_none);
        moreViewButton = (Button) v.findViewById(R.id.moreViewButton);


    }

    public void setRealInItView() {
        setRecyclerView();
        mobile5041();
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    public void setMobile5040Filter(Mobile5040Filter mobile5040Filter) {
        this.mobile5040Filter = mobile5040Filter;
    }

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new DoctorItemAdapter(recyclerView, items, activity, itemOnClickListener, scrollView);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile5041();
            }
        });

        // 스크롤로 더보기 구현하고 싶으면
//        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                int scrollViewPos = scrollView.getScrollY();
//                int TextView_lines = scrollView.getChildAt(0).getBottom() - scrollView.getHeight();
//
//                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//
//
//                    if (TextView_lines == scrollViewPos) {
//                        if (crhItemAdapter.isMoreInfo == true) {
//                            crhItemAdapter.totalItemCount = linearLayoutManager.getItemCount();
//                            crhItemAdapter.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//
//                            if (!crhItemAdapter.isLoading && crhItemAdapter.totalItemCount <= (crhItemAdapter.lastVisibleItem + crhItemAdapter.visibleThreshold)) {
//                                crhItemAdapter.isLoading = true;
//
//                                if (crhItemAdapter.onLoadMoreListener != null) {
//                                    crhItemAdapter.onLoadMoreListener.onLoadMore();
//                                }
//
//                            }
//                        } else {
//                            // new UseFul(activity).showToast("불러올 데이터가 없습니다. - 스크롤");
//                        }
//                    }
//
//
//            }
//        });

        // 버튼으로 더보기 구현하고 싶으면
        moreViewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter.isMoreInfo == true) {
                    if (!itemAdapter.isLoading) {
                        itemAdapter.isLoading = true;
                        if (itemAdapter.onLoadMoreListener != null) {
                            itemAdapter.onLoadMoreListener.onLoadMore();
                        }
                    }
                } else {
                    callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.");
                }
            }
        });
    }

    OnClickListener itemOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            DoctorItem item = (DoctorItem) v.getTag();

            int pk = item.getPk(); // 의사 고유번호
            String doctorname = item.getDoctorName(); // 의사 이름

            Log.i("MyTagssss", "doctor pirmarykey = "+pk);
            // 의료진 상세 페이지로 이동
            Intent intent = new Intent(activity, Mobile5200.class);
            intent.putExtra("doctor", doctorname);
            intent.putExtra("doctor_pk", pk);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activity.startActivity(intent);
        }
    };

    // 서버에 의료진 리스트 요청
    public void mobile5041() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5041");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("currentSelectedDepartmentCode", currentSelectedDepartmentCode);
        params.put("index", index);
        params.put("view_num", view_num);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5042);
        inLet.start();
    }

    // 서버에 의료진 리스트 요청 후처리
    public void mobile5042(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                result_none.setVisibility(View.GONE);
                String more_info = jsonObject.getString("more_info");

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int doctor_pk = jsonObject1.getInt("doctor_pk");
                    String doctor_name = jsonObject1.getString("doctor_name");
                    int doctor_department = jsonObject1.getInt("doctor_department");

                    DoctorItem item = new DoctorItem();
                    item.setPk(doctor_pk);
                    item.setDoctorName(doctor_name);

                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;

            } else {
                itemAdapter.isMoreInfo = false;
                result_none.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void allClear() {
        index = 0;
        items.clear();
        itemAdapter.isMoreInfo = true;
        itemAdapter.notifyDataSetChanged();
        recyclerView.removeAllViews();
    }
}
