package org.snuh.www.app.clinicreservation;

public class SubDoctorItem {
    public SubDoctorItemViewHolder subDoctorItemViewHolder;
    public int doctorPrimaryKey;
    public String doctorDepartment;
    public String doctorName;

    public void setViewHolder(SubDoctorItemViewHolder userViewholder) {
        this.subDoctorItemViewHolder = userViewholder;
    }
    public SubDoctorItemViewHolder getSubDoctorItemViewHolder() {
        return this.subDoctorItemViewHolder;
    }

    public void setDoctorPrimaryKey(int primaryKey) {
        this.doctorPrimaryKey = primaryKey;
    }
    public int getDoctorPrimaryKey() {
        return this.doctorPrimaryKey;
    }

    public void setDoctorName(String name) {
        this.doctorName = name;
    }
    public String getDoctorName() {
        return this.doctorName;
    }

    public void setDoctorDepartment(String department){
        this.doctorDepartment = department;
    }
    public String getDoctorDepartment() {
        return this.doctorDepartment;
    }
}
