package org.snuh.www.app.historyitemset;

import android.app.Activity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.UseFul;

import java.util.List;

public class HistoryItemAdapter extends RecyclerView.Adapter {
    private Activity activity;

    private List<HistoryItem> items;
    private NestedScrollView scrollviews;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;
    public String getMoreInfoType = "scroll"; // or button

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public HistoryItemAdapter(RecyclerView recyclerView, List<HistoryItem> items, final Activity activity, View.OnClickListener itemsOnClickListener, NestedScrollView scrollview) {
        this.items = items;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;
        this.scrollviews = scrollview;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.history_item, parent, false);
            return new HistoryItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HistoryItemViewHolder) {
            HistoryItem item = items.get(position);
            HistoryItemViewHolder userViewHolder = (HistoryItemViewHolder) holder;

            item.setViewHolder(userViewHolder);
            userViewHolder.period.setText(item.getPeriod());
            userViewHolder.content.setText(item.getContent());

//            userViewHolder.button.setTag(item);
//            userViewHolder.button.setOnClickListener(itemsOnClickListener);
//            userViewHolder.title.setText(new UseFul(activity).cutString(item.getTitle(), 18));
//            userViewHolder.datetime.setText(item.getDatetime());
//            userViewHolder.view_index.setText("조회수 : "+item.getViewIndex()+"");



        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/
}
