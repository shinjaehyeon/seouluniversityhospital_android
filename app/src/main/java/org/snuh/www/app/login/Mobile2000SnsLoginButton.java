package org.snuh.www.app.login;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class Mobile2000SnsLoginButton extends RelativeLayout {
    View v;
    Context context;

    /**************************************************************/

    Button sns_login_button;
    ImageView sns_icon;
    TextView sns_button_text;

    /**************************************************************/

    public Mobile2000SnsLoginButton(Context context) {
        super(context);
        this.context = context;
        initView();
    }
    public Mobile2000SnsLoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
        getAttrs(attrs);
    }
    public Mobile2000SnsLoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        initView();
        getAttrs(attrs, defStyle);
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile2000_sns_login_button, this, false);
        addView(v);

        setConnectViewId();
    }

    public void setConnectViewId() {
        sns_login_button = (Button) v.findViewById(R.id.sns_login_button);
        sns_icon = (ImageView) v.findViewById(R.id.sns_icon);
        sns_button_text = (TextView) v.findViewById(R.id.sns_button_text);
    }

    public void setIconImage(int resId) {
        sns_icon.setImageResource(resId);
    }

    public void setSnsIconWidth(float width) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) sns_icon.getLayoutParams();
        lp.width = (int) width;
    }

    public void setSnsIconHeight(float height) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) sns_icon.getLayoutParams();
        lp.height = (int) height;
    }

    public void setSnsIconMarginLeft(float margin) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) sns_icon.getLayoutParams();
        lp.leftMargin = (int) margin;
    }

    public void setButtonText(String text) {
        sns_button_text.setText(text);
    }

    public void setButtonEvent(View.OnClickListener e) {
        sns_login_button.setOnClickListener(e);
    }

    public void setLoginButtonTag(int tag) {
        sns_login_button.setTag(tag);
    }



    /************************************************************************************************/

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Mobile2000SnsLoginButton);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Mobile2000SnsLoginButton, defStyle, 0);
        setTypeArray(typedArray);

    }

    private void setTypeArray(TypedArray typedArray) {
        // SNS 로그인 아이콘 설정
        int icon = typedArray.getResourceId(R.styleable.Mobile2000SnsLoginButton_iconImage, R.drawable.facebook_icon);
        sns_icon.setImageResource(icon);

        // SNS 로그인 아이콘 (너비 & 높이 & 왼쪽여백) 설정
        int iconWidth = (int) typedArray.getDimension(R.styleable.Mobile2000SnsLoginButton_iconWidth, 0);
        int iconHeight = (int) typedArray.getDimension(R.styleable.Mobile2000SnsLoginButton_iconHeight, 0);
        int iconMarginLeft = (int) typedArray.getDimension(R.styleable.Mobile2000SnsLoginButton_iconMaringLeft, 0);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) sns_icon.getLayoutParams();
        lp.width = iconWidth;
        lp.height = iconHeight;
        lp.leftMargin = iconMarginLeft;
        sns_icon.setLayoutParams(lp);

        // SNS 로그인 텍스트 설정
        String text = typedArray.getString(R.styleable.Mobile2000SnsLoginButton_setText);
        sns_button_text.setText(text);

        // SNS 로그인 텍스트 크기 설정
        float textSize = (float) typedArray.getDimension(R.styleable.Mobile2000SnsLoginButton_setTextSize, 0);
        textSize = textSize / getResources().getDisplayMetrics().density;
        Log.i("MyTags", "textSize = "+textSize);
        sns_button_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        // SNS 로그인 텍스트 색상 설정
        int textColor = typedArray.getColor(R.styleable.Mobile2000SnsLoginButton_setTextColor, 0);
        sns_button_text.setTextColor(textColor);

        // SNS 로그인 배경 설정
        int bgRes = typedArray.getResourceId(R.styleable.Mobile2000SnsLoginButton_setBackground, R.drawable.ripple_button_type_3);
        sns_login_button.setBackgroundResource(bgRes);



        typedArray.recycle();
    }

}
