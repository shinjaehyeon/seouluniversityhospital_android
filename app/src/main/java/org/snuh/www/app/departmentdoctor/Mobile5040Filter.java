package org.snuh.www.app.departmentdoctor;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.SubDepartmentItem;
import org.snuh.www.app.clinicreservation.SubDepartmentSelectDialog;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class Mobile5040Filter extends RelativeLayout {
    AppCompatActivity activity;
    Context context;
    UseFul useFul;
    View v;
    SubDepartmentSelectDialog subDepartmentSelectDialog;

    Mobile5040RecyclerView mobile5040RecyclerView;

    boolean isDeparmentLoadingComplete = false;

    TextView filter_textview;
    Button period_setting_button;

    int currentSelectedDepartmentCode;
    String currentSelectedDepartmentString;

    ArrayList<SubDepartmentItem> subDepartmentItems = new ArrayList<SubDepartmentItem>();

    /**************************************************************/


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 4622: // 서버에 진료과 리스트 요청 후처리
                    mobile4622(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };
    /**************************************************************/

    public Mobile5040Filter(Context context) {
        super(context);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5040Filter(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public Mobile5040Filter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.activity = (AppCompatActivity) context;
        useFul = new UseFul(activity);
    }

    public void inflateView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.mobile5040_filter, this, false);
        addView(v);
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        filter_textview = (TextView) v.findViewById(R.id.filter_textview);
        period_setting_button = (Button) v.findViewById(R.id.period_setting_button);

        subDepartmentSelectDialog = new SubDepartmentSelectDialog(activity);
    }

    public void setRealInItView() {
        setObjectEvent(); // 객체에 이벤트 설정하기
        mobile4621(); // 서버에 진료과 리스트 요청
    }

    public void setMobile5040RecyclerView(Mobile5040RecyclerView r) {
        this.mobile5040RecyclerView = r;
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        // 정렬설정 버튼 클릭 시 이벤트 연결
        period_setting_button.setOnClickListener(periodSettingButtonEvent);
    }

    /**************************************************************/

    // 정렬설정 버튼 클릭 시 이벤트
    OnClickListener periodSettingButtonEvent = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isDeparmentLoadingComplete) {
                callSubDepartmentSelectDialog("의료진 소속 진료과 선택");
            }
        }
    };

    /**************************************************************/

    // 진료과 선택 다이얼로그 창 호출 함수
    public void callSubDepartmentSelectDialog(String title) {
        subDepartmentSelectDialog.show();
        subDepartmentSelectDialog.setTitle(title);
        subDepartmentSelectDialog.setCloseButtonOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                subDepartmentSelectDialog.dismiss();
            }
        });
        subDepartmentSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (subDepartmentSelectDialog.isOnItemClicked) {
                    currentSelectedDepartmentCode = subDepartmentSelectDialog.currentSelectedDepartmentCode;
                    currentSelectedDepartmentString = subDepartmentSelectDialog.currentSelectedDepartmentString;

                    filter_textview.setText(currentSelectedDepartmentString+" 의료진");

                    mobile5040RecyclerView.currentSelectedDepartmentCode = currentSelectedDepartmentCode;
                    mobile5040RecyclerView.currentSelectedDepartmentString = currentSelectedDepartmentString;
                    mobile5040RecyclerView.allClear();


                    mobile5040RecyclerView.mobile5041();
                }
            }
        });
    }

    // 서버에 진료과 리스트 요청
    public void mobile4621() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile4621");
        params.put("member", new ShareInfo(activity).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(4622);
        inLet.start();
    }

    // 서버에 진료과 리스트 요청 후처리
    public void mobile4622(Object obj) {
        String data = obj.toString();
        isDeparmentLoadingComplete = true;


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            SubDepartmentItem subDepartmentItem2 = new SubDepartmentItem();
            subDepartmentItem2.setCode(0);
            subDepartmentItem2.setDepartment("전체");
            subDepartmentItems.add(subDepartmentItem2);

            if (result.equals("ok")) {
                JSONArray ja = jsonObject.getJSONArray("data");
                for (int i=0; i<ja.length(); i++) {
                    JSONObject jo = ja.getJSONObject(i);

                    int suhd = jo.getInt("suhd");
                    String department = jo.getString("department");

                    SubDepartmentItem subDepartmentItem = new SubDepartmentItem();
                    subDepartmentItem.setCode(suhd);
                    subDepartmentItem.setDepartment(department);

                    subDepartmentItems.add(subDepartmentItem);
                }

                subDepartmentSelectDialog.setSubDepartmentItems(subDepartmentItems);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
