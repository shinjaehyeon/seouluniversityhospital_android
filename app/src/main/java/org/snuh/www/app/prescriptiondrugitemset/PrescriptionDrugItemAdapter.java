package org.snuh.www.app.prescriptiondrugitemset;

import android.app.Activity;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;

import java.util.List;

public class PrescriptionDrugItemAdapter extends RecyclerView.Adapter {
    private Activity activity;

    private List<PrescriptionDrugItem> items;
    private NestedScrollView scrollviews;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;
    public String getMoreInfoType = "scroll"; // or button

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public PrescriptionDrugItemAdapter(RecyclerView recyclerView, List<PrescriptionDrugItem> items, final Activity activity, View.OnClickListener itemsOnClickListener, NestedScrollView scrollview) {
        this.items = items;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;
        this.scrollviews = scrollview;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.prescription_drug_item, parent, false);
            return new PrescriptionDrugItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PrescriptionDrugItemViewHolder) {
            PrescriptionDrugItem item = items.get(position);
            PrescriptionDrugItemViewHolder userViewHolder = (PrescriptionDrugItemViewHolder) holder;

            item.setViewHolder(userViewHolder);

            userViewHolder.drug_primarykey.setText("No."+item.getDrugPrimaryKey());
            userViewHolder.drug_name.setText(item.getDrugName()+"");
            userViewHolder.oneday_dose.setText(item.getOnedayDose()+"");
            userViewHolder.oneday_number.setText(item.getOnedayNumber()+" 회");
            userViewHolder.dose_day_num.setText(item.getDoseDayNum()+" 일");
            userViewHolder.note.setText(item.getNote()+"");


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/
}
