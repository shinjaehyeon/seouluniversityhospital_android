package org.snuh.www.app.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

import java.util.ArrayList;

public class CustomSelectDialog extends Dialog {
    Context context;
    UseFul useFul;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;
    LinearLayout list_area;

    public ArrayList<CustomSelectDialogSelectionButton> buttons = new ArrayList<CustomSelectDialogSelectionButton>();
    CustomSelectDialogSelectionButton previousSelectedButton;

    /**************************************************************/


    // ArrayList<Integer> valueInteger = new ArrayList<Integer>();
    // ArrayList<String> valueString = new ArrayList<String>();
    int[] valueInteger;
    String[] valueString;

    int current_index;
    int first_index = -1;

    /**************************************************************/


    public CustomSelectDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.custom_select_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        if (first_index != -1) {
            buttons.get(first_index).setActiveEffect();
            previousSelectedButton = (CustomSelectDialogSelectionButton) buttons.get(first_index);
        }
    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(context);


        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);
        list_area = (LinearLayout) findViewById(R.id.list_area);

        for (int i=0; i<valueString.length; i++) {
            CustomSelectDialogSelectionButton button = new CustomSelectDialogSelectionButton(context);
            button.setText(valueString[i]);
            button.setIndex(i);

            buttons.add(button);
        }

        for (int i=0; i<buttons.size(); i++) {
            list_area.addView(buttons.get(i));
        }
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<buttons.size(); i++) {
            buttons.get(i).getButton().setOnClickListener(itemOnClickListener);
        }
    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public void setSelectionList(String[] s, int[] n) {
        this.valueInteger = n;
        this.valueString = s;
    }

    public void setFirstValue(int n) {
        this.first_index = n;
    }

    /**************************************************************/

    public int getCurrentIndex() {
        return this.current_index;
    }

    /**************************************************************/

    public boolean isOnItemClicked = false;

    // 진료과 리스트중 하나를 클릭 했을 시 발생하는 이벤트 설정
    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isOnItemClicked = true;

            CustomSelectDialogSelectionButton obj = (CustomSelectDialogSelectionButton) v.getTag();
            current_index = obj.getIndex();

            obj.setActiveEffect();

            if (previousSelectedButton != null) {
                if ((int) previousSelectedButton.getIndex() != (int) obj.getIndex()) {
                    previousSelectedButton.setNormalEffect();
                }
            }

            previousSelectedButton = (CustomSelectDialogSelectionButton) obj;

            dismiss();
        }
    };


}
