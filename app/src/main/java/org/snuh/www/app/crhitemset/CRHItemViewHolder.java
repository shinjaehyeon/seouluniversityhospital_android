package org.snuh.www.app.crhitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class CRHItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public TextView doctor;
    public TextView department;
    public TextView reservation_date;
    public TextView reservation_time;

    public Button current_status;
    public Button cancel_button;

    /**************************************************************/

    public CRHItemViewHolder(View view) {
        super(view);
        this.view = view;

        doctor = (TextView) view.findViewById(R.id.doctor);
        department = (TextView) view.findViewById(R.id.department);
        reservation_date = (TextView) view.findViewById(R.id.reservation_date);
        reservation_time = (TextView) view.findViewById(R.id.reservation_time);

        current_status = (Button) view.findViewById(R.id.current_status);
        cancel_button = (Button) view.findViewById(R.id.cancel_button);
    }
}
