package org.snuh.www.app.clinicreservation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;
import java.util.Calendar;

public class Mobile4660 extends Fragment {
    Activity activity;
    View rootView;
    UseFul useFul;
    NestedScrollView scrollView;

    /**************************************************************/

    SuhCalendar suhCalendar;

    /**************************************************************/

    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<TimeItem> timeItems = new ArrayList<TimeItem>();
    TimeItemAdapter timeItemAdapter;

    /**************************************************************/

    TimeItem prevTimeItem = null;
    int currentSelectedYear;
    int currentSelectedMonth;
    int currentSelectedDate;
    int currentSelectedHour;
    int currentSelectedMinute;

    /**************************************************************/


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 4662:
                    mobile4662(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    /**************************************************************/

    public Mobile4660() {

    }

    @SuppressLint("ValidFragment")
    public Mobile4660(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile4660, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        setTodayDate();
        setRecyclerView();

        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        suhCalendar = (SuhCalendar) rootView.findViewById(R.id.suhCalendar);
        suhCalendar.setDateOnClickListener(dateOnClickListener);
        suhCalendar.initView();
        suhCalendar.setViewCalendar();

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    /**************************************************************/

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,2);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        timeItemAdapter = new TimeItemAdapter(recyclerView, timeItems, activity, timeOnClickListener);
        recyclerView.setAdapter(timeItemAdapter);
    }

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content, final boolean isFinish) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
                if (isFinish) {
                    activity.finish();
                }
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/

    int department;
    int doctor;



    // 선택된 진료과, 의료진, 날짜를 토대로 서버에 시간 리스트 요청
    public void mobile4661(int year, int month, int date) {
        String monthString = month+"";
        if (month < 10) {
            monthString = "0"+month;
        }
        String dateString = date+"";
        if (date < 10) {
            dateString = "0"+date;
        }
        String datetimeHypon = year+"-"+monthString+"-"+dateString;
        String datetimeNoHypon = year+""+monthString+""+dateString;


        ContentValues params = new ContentValues();
        params.put("act", "mobile4661");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("department", department);
        params.put("doctor", doctor);
        params.put("datetimeHypon", datetimeHypon);
        params.put("datetimeNoHypon", datetimeNoHypon);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(4662);
        inLet.start();
    }

    // 선택된 진료과, 의료진, 날짜를 토대로 서버에 시간 리스트 요청 후처리
    public void mobile4662(Object obj) {
        String data = obj.toString();

        timeItems.clear();
        Log.i("MyTags", "mobile4662 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int year = jsonObject1.getInt("year");
                    int month = jsonObject1.getInt("month");
                    int date = jsonObject1.getInt("date");

                    int hour = jsonObject1.getInt("hour");
                    int minute = jsonObject1.getInt("minute");
                    int already_suhCRH_pk = jsonObject1.getInt("already_suhCRH_pk");

                    TimeItem timeItem = new TimeItem();
                    timeItem.setYear(year);
                    timeItem.setMonth(month);
                    timeItem.setDate(date);
                    timeItem.setHour(hour);
                    timeItem.setMinute(minute);
                    timeItem.setAlready(already_suhCRH_pk);
                    timeItems.add(timeItem);
                }

                timeItemAdapter.notifyDataSetChanged();




                scrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            } else {
                timeItemAdapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setNestedScrollView(NestedScrollView scrollView) {
        this.scrollView = scrollView;
    }

    /**************************************************************/

    View.OnClickListener timeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TimeItem timeItem = (TimeItem) v.getTag();

            TimeItemViewHolder userViewHolder = (TimeItemViewHolder) timeItem.getViewHolder();
            userViewHolder.time_button.setBackgroundResource(R.drawable.ripple_button_type_2);
            userViewHolder.time_textview.setTextColor(useFul.getRcolor(R.color.mobile4660_time_text_on_color));

            currentSelectedYear = timeItem.getYear();
            currentSelectedMonth = timeItem.getMonth();
            currentSelectedDate = timeItem.getDate();
            currentSelectedHour = timeItem.getHour();
            currentSelectedMinute = timeItem.getMinute();

            if (prevTimeItem != null) {
                if (prevTimeItem != timeItem) {
                    TimeItemViewHolder userViewHolder2 = (TimeItemViewHolder) prevTimeItem.getViewHolder();
                    userViewHolder2.time_button.setBackgroundResource(R.drawable.ripple_button_type_6);
                    userViewHolder2.time_textview.setTextColor(useFul.getRcolor(R.color.mobile4660_time_text_normal_color));
                }
            }
            prevTimeItem = timeItem;
        }
    };


    /**************************************************************/


    Calendar todayCalendar = Calendar.getInstance();
    int todayYear;
    int todayMonth;
    int todayDate;

    View.OnClickListener dateOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SuhCalendarInfoSet infoSet = (SuhCalendarInfoSet) v.getTag();

            if(infoSet.getIsActiveDate()) {
                int year = infoSet.getYear();
                int month = infoSet.getMonth();
                int date = infoSet.getDate();
                int day = infoSet.getDay();
                String dayString = infoSet.getDayString();

                if (checkDate(year, month, date, 0)) {
                    // 입력받은 값들 초기화하기
                    if (prevTimeItem != null) {
                        TimeItemViewHolder userViewHolder2 = (TimeItemViewHolder) prevTimeItem.getViewHolder();
                        userViewHolder2.time_button.setBackgroundResource(R.drawable.ripple_button_type_6);
                        userViewHolder2.time_textview.setTextColor(useFul.getRcolor(R.color.mobile4660_time_text_normal_color));
                        prevTimeItem = null;
                    }
                    currentSelectedYear = 0;
                    currentSelectedMonth = 0;
                    currentSelectedDate = 0;
                    currentSelectedHour = 0;
                    currentSelectedMinute = 0;




                    suhCalendar.setSelectedYear(year);
                    suhCalendar.setSelectedMonth(month);
                    suhCalendar.setSelectedDate(date);
                    suhCalendar.setSelectedButtonIndex(infoSet.getIndex());


                    Button dateButton = (Button) infoSet.getDateButton();
                    TextView dateTextView = (TextView) infoSet.getDateTextView();
                    TextView todayTextView = (TextView) infoSet.getTodayTextView();

                    dateButton.setBackgroundResource(R.drawable.ripple_button_type_2);
                    dateTextView.setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_on_color));
                    todayTextView.setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_on_color));

                    Log.i("MyTags", "선택한 날짜는 " + year + "년 " + (month + 1) + "월 " + date + "일 " + dayString + "요일 입니다.");
                    // 선택된 진료과, 의료진, 날짜를 토대로 서버에 시간 리스트 요청
                    mobile4661(year, month+1, date);


                    int prevSelectedButtonIndex = suhCalendar.prevSelectedButtonindex;

                    if (prevSelectedButtonIndex != 900900 && suhCalendar.checkPrevSelectedValueCurrentValue()) {
                        if (prevSelectedButtonIndex != infoSet.getIndex()) {
                            if (prevSelectedButtonIndex % 2 == 0) {
                                suhCalendar.dateButton.get(prevSelectedButtonIndex).setBackgroundResource(R.drawable.ripple_button_type_11);
                                suhCalendar.dateTextView.get(prevSelectedButtonIndex).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_normal_color));
                                suhCalendar.todayTextView.get(prevSelectedButtonIndex).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_normal_color));
                            } else {
                                suhCalendar.dateButton.get(prevSelectedButtonIndex).setBackgroundResource(R.drawable.ripple_button_type_12);
                                suhCalendar.dateTextView.get(prevSelectedButtonIndex).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_date_text_normal_color));
                                suhCalendar.todayTextView.get(prevSelectedButtonIndex).setTextColor(useFul.getRcolor(R.color.mobile4660_calendar_today_text_normal_color));
                            }
                        }
                    }
                    suhCalendar.setPrevSelectedButtonIndexYear(suhCalendar.getCurrentFocusYear());
                    suhCalendar.setPrevSelectedButtonIndexMonth(suhCalendar.getCurrentFocusMonth());
                    suhCalendar.setPrevSelectedButtonindex(infoSet.getIndex());
                } else {
                    callSUHDialogType1Dialog("안내", "진료예약은 오늘을 포함한 이후부터 가능합니다.", false);
                }
            }


        }
    };


    // 오늘 날짜 셋팅
    public void setTodayDate() {
        todayYear = useFul.getTodayYear();
        todayMonth = useFul.getTodayMonth() - 1;
        todayDate = useFul.getTodayDate();
    }


    // 받아온 날짜가 오늘날짜보다 n일 뒤인지 체크하기
    public boolean checkDate(int year, int month, int date, int after) {
        Log.i("MyTags", "todayYear = "+todayYear+", todayMonth = "+todayMonth+", todayDate = "+todayDate);
        Log.i("MyTags", "year = "+year+", month = "+month+", date = "+date);

        if (useFul.dateCompare(useFul.getTodayYear(), useFul.getTodayMonth() - 1, useFul.getTodayDate(), year, month, date) <= 0) {
            Log.i("MyTags", "여기 실행됨 1");
            return true;
        }

        if (useFul.getDday2(year, month, date) <= after) {
            Log.i("MyTags", "여기 실행됨 2");
            return false;
        }

        Log.i("MyTags", "여기 실행됨 3");
        return true;
    }


    public void allClear() {
        timeItems.clear();
        timeItemAdapter.notifyDataSetChanged();
        if (prevTimeItem != null) {
            TimeItemViewHolder userViewHolder2 = (TimeItemViewHolder) prevTimeItem.getViewHolder();
            userViewHolder2.time_button.setBackgroundResource(R.drawable.ripple_button_type_6);
            userViewHolder2.time_textview.setTextColor(useFul.getRcolor(R.color.mobile4660_time_text_normal_color));
            prevTimeItem = null;
        }

        currentSelectedYear = 0;
        currentSelectedMonth = 0;
        currentSelectedDate = 0;
        currentSelectedHour = 0;
        currentSelectedMinute = 0;
    }
}
