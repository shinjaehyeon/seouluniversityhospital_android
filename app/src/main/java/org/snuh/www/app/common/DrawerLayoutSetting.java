package org.snuh.www.app.common;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.view.MotionEvent;
import android.view.View;

import org.snuh.www.app.R;


public class DrawerLayoutSetting {
    public Activity activity;
    public Context context;
    public DrawerLayout drawerLayout;
    public CustomDrawerLayout drawerView;
    public CustomBottomMenubar bottom_fixed_menu_bar;

    public DrawerLayoutSetting(Activity activity) {
        this.activity = activity;
        this.context = (Context) activity;;
        init();
    }

    public void init() {
        drawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        drawerView = (CustomDrawerLayout) activity.findViewById(R.id.drawer);

        bottom_fixed_menu_bar = (CustomBottomMenubar) activity.findViewById(R.id.bottomMenubar);

        setOnClickEvent();

        bottom_fixed_menu_bar.setDrawerLayout(drawerLayout, drawerView);
    }

    public void setOnClickEvent() {
        drawerLayout.setDrawerListener(myDrawerListener);
        drawerView.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                return true;
            }
        });
    }

    public DrawerLayout.DrawerListener myDrawerListener = new DrawerLayout.DrawerListener() {
        public void onDrawerClosed(View drawerView) {

        }
        public void onDrawerOpened(View drawerView) {

        }

        public void onDrawerSlide(View drawerView, float slideOffset) {
            // txtPrompt.setText("onDrawerSlide: " + String.format("%.2f", slideOffset));
        }

        public void onDrawerStateChanged(int newState) {
            String state;
            switch (newState) {
                case DrawerLayout.STATE_IDLE:
                    state = "STATE_IDLE";
                    break;
                case DrawerLayout.STATE_DRAGGING:
                    state = "STATE_DRAGGING";
                    break;
                case DrawerLayout.STATE_SETTLING:
                    state = "STATE_SETTLING";
                    break;
                default:
                    state = "unknown!";
            }

            // txtPrompt2.setText(state);
        }
    };
}
