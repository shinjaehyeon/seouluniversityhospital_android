package org.snuh.www.app.setting;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.hospitalnews.HospitalDetailDialog;
import org.snuh.www.app.hospitalnewsitemset.HospitalNewsItem;
import org.snuh.www.app.hospitalnewsitemset.HospitalNewsItemAdapter;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile9000 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;



    NestedScrollView scrollView;



    int currentIsGetPush = 0;
    Button pushOnButton;
    Button pushOffButton;

    boolean isLoading = false;



    int currentIsPasswordActive = 0;
    boolean isPasswordWindowVisible = false;
    ConstraintLayout password_window;
    EditText password_edittext;
    Button pushOnButton2;
    Button pushOffButton2;
    Button password_commit_button;
    Button password_cancel_button;


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 9002:
                    mobile9002(msg.obj);
                    break;
                case 9004:
                    mobile9004(msg.obj);
                    break;

                default:

                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile9000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기


        mobile9001(); // 서버에 푸쉬 알림 수신 여부 요청


        mobile9005(); // 비밀번호 설정 여부 가져오기
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }






    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);


        scrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);


        pushOnButton = (Button) findViewById(R.id.pushOnButton);
        pushOffButton = (Button) findViewById(R.id.pushOffButton);


        password_window = (ConstraintLayout) findViewById(R.id.password_window);
        password_edittext = (EditText) findViewById(R.id.password_edittext);
        pushOnButton2 = (Button) findViewById(R.id.pushOnButton2);
        pushOffButton2 = (Button) findViewById(R.id.pushOffButton2);
        password_commit_button = (Button) findViewById(R.id.password_commit_button);
        password_cancel_button = (Button) findViewById(R.id.password_cancel_button);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        pushOnButton.setOnClickListener(pushWhetherButtonClickEvent);
        pushOffButton.setOnClickListener(pushWhetherButtonClickEvent);

        pushOnButton2.setOnClickListener(pushWhetherButtonClickEvent2);
        pushOffButton2.setOnClickListener(pushWhetherButtonClickEvent2);

        password_commit_button.setOnClickListener(passwordCommitButtonEvent);
        password_cancel_button.setOnClickListener(passwordCancelButtonEvent);
    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }


    // 서버에 푸쉬 알림 수신 여부 요청
    public void mobile9001() {
        isLoading = true;

        ContentValues params = new ContentValues();
        params.put("act", "mobile9001");
        params.put("member", new ShareInfo(this).getMember());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(9002);
        inLet.start();
    }

    // 서버에 푸쉬 알림 수신 여부 요청 후처리
    public void mobile9002(Object obj) {
        isLoading = false;
        String data = obj.toString();


        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                int is_get_push = jsonObject.getInt("is_get_push");

                currentIsGetPush = is_get_push;

                if (is_get_push == 1) {
                    pushOnButton.setBackgroundResource(R.drawable.ripple_button_type_2);
                    pushOffButton.setBackgroundResource(R.drawable.ripple_button_type_5);
                } else {
                    pushOnButton.setBackgroundResource(R.drawable.ripple_button_type_5);
                    pushOffButton.setBackgroundResource(R.drawable.ripple_button_type_2);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    // 비밀번호 설정 여부 가져오기
    public void mobile9005() {
        currentIsPasswordActive = new ShareInfo(activity).getPsswordSettingFlag();
        if (currentIsPasswordActive == 0) {
            pushOnButton2.setBackgroundResource(R.drawable.ripple_button_type_5);
            pushOffButton2.setBackgroundResource(R.drawable.ripple_button_type_2);
        } else {
            pushOnButton2.setBackgroundResource(R.drawable.ripple_button_type_2);
            pushOffButton2.setBackgroundResource(R.drawable.ripple_button_type_5);
        }
    }


    View.OnClickListener pushWhetherButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isLoading == false) {
                switch (v.getId()) {
                    case R.id.pushOnButton:
                        if (currentIsGetPush == 0) {
                            mobile9003(1); // 서버에 푸쉬 알림 수신 여부 업로드
                        }
                        break;
                    case R.id.pushOffButton:
                        if (currentIsGetPush == 1) {
                            mobile9003(0); // 서버에 푸쉬 알림 수신 여부 업로드
                        }
                        break;
                    default:

                        break;
                }
            } else {
                useFul.showToast("작업이 진행중입니다. 잠시 후 시도해주세요.");
            }
        }
    };

    View.OnClickListener pushWhetherButtonClickEvent2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isLoading == false) {
                switch (v.getId()) {
                    case R.id.pushOnButton2: // ON 버튼 클릭 시
                        if (currentIsPasswordActive == 0) {
                            // 비밀번호 설정이 OFF 인 경우
                            showPasswordWindow();
                        }
                        break;
                    case R.id.pushOffButton2: // OFF 버튼 클릭 시
                        if (currentIsPasswordActive == 1) {
                            // 비밀번호 설정이 ON 인 경우
                            showPasswordWindow();
                        }
                        break;
                    default:

                        break;
                }
            } else {
                useFul.showToast("작업이 진행중입니다. 잠시 후 시도해주세요.");
            }
        }
    };

    View.OnClickListener passwordCommitButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String password = password_edittext.getText().toString();

            if (!password.equals("")) {
                if (currentIsPasswordActive == 0) {
                    // 현재 패스워드 여부 상태가 OFF 이면
                    // 비밀번호 셋팅
                    new ShareInfo(activity).setInputIntValue("passwordSetting", 1);
                    new ShareInfo(activity).setInputIntValue("password", password);
                    currentIsPasswordActive = 1;
                    pushOnButton2.setBackgroundResource(R.drawable.ripple_button_type_2);
                    pushOffButton2.setBackgroundResource(R.drawable.ripple_button_type_5);

                    useFul.showToast("비밀번호가 설정되었습니다.");
                    password_edittext.setText("");
                    hidePasswordWindow();

                } else {
                    // 현재 패스워드 여부 상태가 ON 이면

                    String originPassword = new ShareInfo(activity).getPassword();

                    if (originPassword.equals(password)) {
                        // 비밀번호 해제
                        new ShareInfo(activity).setInputIntValue("passwordSetting", 0);
                        new ShareInfo(activity).setInputIntValue("password", "");
                        currentIsPasswordActive = 0;
                        pushOnButton2.setBackgroundResource(R.drawable.ripple_button_type_5);
                        pushOffButton2.setBackgroundResource(R.drawable.ripple_button_type_2);

                        useFul.showToast("비밀번호가 해제되었습니다.");
                        password_edittext.setText("");
                        hidePasswordWindow();
                    } else {
                        useFul.showToast("설정된 비밀번호와 일치하지 않습니다.");
                    }


                }
            } else {
                useFul.showToast("비밀번호를 입력해주세요.");
            }
        }
    };

    View.OnClickListener passwordCancelButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hidePasswordWindow();
        }
    };

    public void showPasswordWindow() {
        isPasswordWindowVisible = true;

        password_window.setVisibility(View.VISIBLE);
        password_window.setAlpha(0f);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(password_window, "alpha", 0f, 1f);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator2.setDuration(600);
        animator2.start();
    }

    public void hidePasswordWindow() {
        isPasswordWindowVisible = false;

        ObjectAnimator animator2 = ObjectAnimator.ofFloat(password_window, "alpha", 1f, 0f);
        animator2.setInterpolator(new EasingInterpolator(Ease.QUINT_OUT)); // github에서 라이브러리 가져와야 함.
        animator2.setDuration(600);
        animator2.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // code..
                password_window.setVisibility(View.GONE);
            }
        }, 600);
    }

    // 서버에 푸쉬 알림 수신 여부 업로드
    public void mobile9003(int flag) {
        isLoading = true;

        ContentValues params = new ContentValues();
        params.put("act", "mobile9003");
        params.put("member", new ShareInfo(this).getMember());
        params.put("value", flag);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(9004);
        inLet.start();
    }

    // 서버에 푸쉬 알림 수신 여부 업로드 후처리
    public void mobile9004(Object obj) {
        isLoading = false;
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                int success_value = jsonObject.getInt("success_value");
                currentIsGetPush = success_value;


                if (success_value == 1) {
                    useFul.showToast("푸쉬 알림을 받습니다.");
                    pushOnButton.setBackgroundResource(R.drawable.ripple_button_type_2);
                    pushOffButton.setBackgroundResource(R.drawable.ripple_button_type_5);
                } else {
                    useFul.showToast("푸쉬 알림을 받지 않습니다.");
                    pushOnButton.setBackgroundResource(R.drawable.ripple_button_type_5);
                    pushOffButton.setBackgroundResource(R.drawable.ripple_button_type_2);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }









    public void callHospitalDetailDialog(String title, String news_title, String news_datetime, int news_view_index, String news_content) {
        final HospitalDetailDialog hospitalDetailDialog = new HospitalDetailDialog(this);
        hospitalDetailDialog.show();

        hospitalDetailDialog.setNewsTitle(news_title);
        hospitalDetailDialog.setNewsDateTime(news_datetime);
        hospitalDetailDialog.setNewsViewIndex(news_view_index);
        hospitalDetailDialog.setNewsContent(news_content);


        hospitalDetailDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hospitalDetailDialog.dismiss();
            }
        });
        hospitalDetailDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }













    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                if (isPasswordWindowVisible){
                    hidePasswordWindow();
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
