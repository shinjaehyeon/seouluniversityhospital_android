package org.snuh.www.app.historyitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;

public class HistoryItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public TextView period;
    public TextView content;

    /**************************************************************/

    public HistoryItemViewHolder(View view) {
        super(view);
        this.view = view;


        period = (TextView) view.findViewById(R.id.period);
        content = (TextView) view.findViewById(R.id.content);
    }
}
