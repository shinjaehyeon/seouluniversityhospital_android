package org.snuh.www.app.questionitemset;

public class QuestionItem {
    public int questionPrimaryKey;
    public int memberPrimaryKey;

    public int questionTypePrimaryKey;
    public String questionTypeString;

    public String questionDatetime;
    public String questionTitle;
    public String questionContent;

    public String answerDatetime;
    public String answerTitle;
    public String answerContent;

    public QuestionItemViewHolder viewHolder;

    public void setQuestionPrimaryKey(int n) {
        this.questionPrimaryKey = n;
    }
    public int getQuestionPrimaryKey() {
        return this.questionPrimaryKey;
    }

    public void setMemberPrimaryKey(int n) {
        this.memberPrimaryKey = n;
    }
    public int getMemberPrimaryKey() {
        return this.memberPrimaryKey;
    }



    public void setQuestionTypePrimaryKey(int n) {
        this.questionTypePrimaryKey = n;
    }
    public int getQuestionTypePrimaryKey() {
        return this.questionTypePrimaryKey;
    }

    public void setQuestionTypeString(String s) {
        this.questionTypeString = s;
    }
    public String getQuestionTypeString() {
        return this.questionTypeString;
    }


    public void setQuestionDatetime(String s) {
        this.questionDatetime = s;
    }
    public String getQuestionDatetime() {
        return this.questionDatetime;
    }

    public void setQuestionTitle(String s) {
        this.questionTitle = s;
    }
    public String getQuestionTitle() {
        return this.questionTitle;
    }

    public void setQuestionContent(String s) {
        this.questionContent = s;
    }
    public String getQuestionContent() {
        return this.questionContent;
    }

    public void setAnswerDatetime(String s) {
        this.answerDatetime = s;
    }
    public String getAnswerDatetime() {
        return this.answerDatetime;
    }

    public void setAnswerTitle(String s) {
        this.answerTitle = s;
    }
    public String getAnswerTitle() {
        return this.answerTitle;
    }

    public void setAnswerContent(String s) {
        this.answerContent = s;
    }
    public String getAnswerContent() {
        return this.answerContent;
    }

    public void setViewHolder(QuestionItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public QuestionItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
