package org.snuh.www.app.questionitemset;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.snuh.www.app.R;
import org.snuh.www.app.common.LoadingViewHolder;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.UseFul;

import java.util.List;

public class QuestionItemAdapter extends RecyclerView.Adapter { // 제네릭 클래스, T1 = Item 클래스, B2 = ViewHolder 클래스
    private Activity activity;

    private List<QuestionItem> items;

    public boolean isLoading;
    public final int VIEW_TYPE_ITEM = 0;
    public final int VIEW_TYPE_LOADING = 1;
    public int visibleThreshold = 5;
    public int lastVisibleItem, totalItemCount;
    public final LinearLayoutManager linearLayoutManager;

    public OnLoadMoreListener onLoadMoreListener;
    private View.OnClickListener itemsOnClickListener;

    public boolean isMoreInfo;

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    /**************************************************************/

    public QuestionItemAdapter(RecyclerView recyclerView, List<QuestionItem> items, final Activity activity, View.OnClickListener itemsOnClickListener) {
        this.items = items;
        this.activity = activity;
        this.itemsOnClickListener = itemsOnClickListener;

        linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.question_item, parent, false);
            return new QuestionItemViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof QuestionItemViewHolder) {
            QuestionItem item = items.get(position);
            QuestionItemViewHolder userViewHolder = (QuestionItemViewHolder) holder;

            item.setViewHolder(userViewHolder);

            userViewHolder.datetime_textview.setText(item.getQuestionDatetime());
            userViewHolder.question_type_textview.setText("문의유형 : "+item.getQuestionTypeString());
            userViewHolder.question_title_textview.setText(item.getQuestionTitle());
            userViewHolder.button.setTag(item);
            userViewHolder.button.setOnClickListener(itemsOnClickListener);

            if (item.getAnswerContent().equals("")) {
                userViewHolder.current_status.setText("미답변");
                userViewHolder.current_status.setTextColor(new UseFul(activity).getRcolor(R.color.mobile5500_question_item_notanswered_text_color));
            } else {
                userViewHolder.current_status.setText("답변완료");
                userViewHolder.current_status.setTextColor(new UseFul(activity).getRcolor(R.color.mobile5500_question_item_answered_text_color));
            }

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    /**************************************************************/

}
