package org.snuh.www.app.personalquestion;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.Mobile4500;
import org.snuh.www.app.clinicreservation.Mobile4600;
import org.snuh.www.app.clinicreservation.Mobile4700;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.UseFul;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile5500 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;
    RelativeLayout progressBarLayout;

    /**************************************************************/

    Button tab_1_button;
    Button tab_2_button;

    TextView tab_1_button_text_on;
    TextView tab_1_button_text_off;
    TextView tab_2_button_text_on;
    TextView tab_2_button_text_off;

    /**************************************************************/

    Mobile5520 mobile5520;
    Mobile5540 mobile5540;

    ViewPager viewPager;

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile5500);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기

        setViewPager(); // viewPager 셋팅하기
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }





    /**************************************************************/

    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);
        progressBarLayout = (RelativeLayout) findViewById(R.id.progressBarLayout);

        /**************************************************************/

        tab_1_button = (Button) findViewById(R.id.tab_1_button);
        tab_2_button = (Button) findViewById(R.id.tab_2_button);

        tab_1_button_text_on = (TextView) findViewById(R.id.tab_1_button_text_on);
        tab_1_button_text_off = (TextView) findViewById(R.id.tab_1_button_text_off);
        tab_2_button_text_on = (TextView) findViewById(R.id.tab_2_button_text_on);
        tab_2_button_text_off = (TextView) findViewById(R.id.tab_2_button_text_off);

        /**************************************************************/

        mobile5520 = new Mobile5520(activity);
        mobile5520.setMobile5540(mobile5540);
        mobile5540 = new Mobile5540(activity);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        /**************************************************************/
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    /**************************************************************/

    // viewPager 셋팅하기
    public void setViewPager() {
        tab_1_button.setTag(0);
        tab_2_button.setTag(1);

        tab_1_button.setOnClickListener(movePageListener);
        tab_2_button.setOnClickListener(movePageListener);
        viewPager.setAdapter(new pagerAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
    }

    View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();

            tabButtonEffect(tag);

            viewPager.setCurrentItem(tag);
        }
    };

    public void tabButtonEffect(int n) {
        if (n == 0) {
            tab_1_button_text_on.setVisibility(View.VISIBLE);
            tab_1_button_text_off.setVisibility(View.GONE);

            tab_2_button_text_on.setVisibility(View.GONE);
            tab_2_button_text_off.setVisibility(View.VISIBLE);
        } else {
            tab_1_button_text_on.setVisibility(View.GONE);
            tab_1_button_text_off.setVisibility(View.VISIBLE);

            tab_2_button_text_on.setVisibility(View.VISIBLE);
            tab_2_button_text_off.setVisibility(View.GONE);
        }
    }

    private class pagerAdapter extends FragmentStatePagerAdapter {
        public pagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            // super.destroyItem(container, position, object);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);

            tabButtonEffect(position);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return mobile5520;
                case 1:
                    return mobile5540;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content, final boolean isFinish) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(this);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
                if (isFinish) {
                    finish();
                }
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/





    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                callSUHDialogType1Dialog("경고", "문의 내용을 작성하시던 도중이였다면 문의내용이 초기화 됩니다. 정말 뒤로가기 하시겠습니까?", true);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**************************************************************/



}
