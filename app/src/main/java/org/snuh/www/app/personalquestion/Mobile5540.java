package org.snuh.www.app.personalquestion;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.clinicreservation.CRHPeriodSelectDialog;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.questionitemset.QuestionAnswerDialog;
import org.snuh.www.app.questionitemset.QuestionItem;
import org.snuh.www.app.questionitemset.QuestionItemAdapter;

import java.util.ArrayList;

public class Mobile5540 extends Fragment {
    Activity activity;
    View v;
    UseFul useFul;

    CRHPeriodSelectDialog crhPeriodSelectDialog;
    QuestionAnswerDialog questionAnswerDialog;

    int index = 0;
    int view_num = 6;

    TextView recent_day_textview;
    Button period_setting_button;

    int periodSizes[] = {0, 7, 30, 60, 90, 150};
    String periodSizeString[] = {"전체 기간", "최근 7일",  "최근 30일", "최근 60일", "최근 90일", "최근 150일"};
    int period_size = 30; // 최근 n일
    int period_index = 2;


    NestedScrollView scrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<QuestionItem> items = new ArrayList<QuestionItem>();
    QuestionItemAdapter itemAdapter;

    TextView result_none;

    Button moreViewButton;



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 5542: // 서버에 1:1 문의내역 리스트 요청 후처리
                    mobile5542(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    public Mobile5540() {

    }

    @SuppressLint("ValidFragment")
    public Mobile5540(Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.mobile5540, container, false);

        setConnectViewId(); //  xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기


        setRecyclerView(); // 리사이클러뷰 관련 환경 설정하기
        mobile5541(); // 서버에 1:1 문의내역 리스트 요청

        return v;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);
        crhPeriodSelectDialog = new CRHPeriodSelectDialog(activity);
        crhPeriodSelectDialog.setFirstValue(2);

        questionAnswerDialog = new QuestionAnswerDialog(activity);


        recent_day_textview = (TextView) v.findViewById(R.id.recent_day_textview);
        period_setting_button = (Button) v.findViewById(R.id.period_setting_button);

        scrollView = (NestedScrollView) v.findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);

        result_none = (TextView) v.findViewById(R.id.result_none);

        moreViewButton = (Button) v.findViewById(R.id.moreViewButton);

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        period_setting_button.setOnClickListener(periodSettingButtonEvent);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new QuestionItemAdapter(recyclerView, items, activity, itemOnClickListener);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile5541(); // 서버에 1:1 문의내역 리스트 요청
            }
        });

        // 버튼으로 더보기 구현하고 싶으면
        moreViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAdapter.isMoreInfo == true) {
                    if (!itemAdapter.isLoading) {
                        itemAdapter.isLoading = true;
                        if (itemAdapter.onLoadMoreListener != null) {
                            itemAdapter.onLoadMoreListener.onLoadMore();
                        }
                    }
                } else {
                    callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.");
                }
            }
        });
    }

    View.OnClickListener periodSettingButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCRHPeriodSelectDialog("기간 선택");
        }
    };

    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            QuestionItem item = (QuestionItem) v.getTag();

            String questionType = "문의유형 : "+item.getQuestionTypeString();
            String questionTitle = item.getQuestionTitle();
            String questionDatetime = item.getQuestionDatetime();
            String questionContent = item.getQuestionContent();

            String answerTitle = item.getAnswerTitle();
            String answerDatetime = item.getAnswerDatetime();
            String answerContent = item.getAnswerContent();

            questionAnswerDialog.setQuestionType(questionType);
            questionAnswerDialog.setQuestionTitle(questionTitle);
            questionAnswerDialog.setQuestionDatetime(questionDatetime);
            questionAnswerDialog.setQuestionContent(questionContent);

            questionAnswerDialog.setAnswerTitle(answerTitle);
            questionAnswerDialog.setAnswerDatetime(answerDatetime);
            questionAnswerDialog.setAnswerContent(answerContent);

            callQuestionAnswerDialog("문의 & 답변");

        }
    };

    // 서버에 1:1 문의내역 리스트 요청
    public void mobile5541() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile5541");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("index", index);
        params.put("view_num", view_num);
        params.put("period_size", period_size);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(5542);
        inLet.start();
    }

    // 서버에 1:1 문의내역 리스트 요청 후처리
    public void mobile5542(Object obj) {
        String data = obj.toString();
        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                result_none.setVisibility(View.GONE);
                String more_info = jsonObject.getString("more_info");

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int questionPrimaryKey = jsonObject1.getInt("questionPrimaryKey");
                    int memberPrimaryKey = jsonObject1.getInt("memberPrimaryKey");

                    int questionTypePrimaryKey = jsonObject1.getInt("questionTypePrimaryKey");
                    String questionTypeString = jsonObject1.getString("questionTypeString");

                    String questionDatetime = jsonObject1.getString("questionDatetime");
                    String questionTitle = jsonObject1.getString("questionTitle");
                    String cleanQuestionTitle = questionTitle;
                    if (cleanQuestionTitle.length() >= 25) {
                        cleanQuestionTitle = cleanQuestionTitle.substring(0, 24)+"...";
                    }

                    String questionContent =  jsonObject1.getString("questionContent");

                    String answerDatetime = jsonObject1.getString("answerDatetime");
                    String answerTitle = jsonObject1.getString("answerTitle");
                    String answerContent = jsonObject1.getString("answerContent");

                    QuestionItem item = new QuestionItem();
                    item.setQuestionPrimaryKey(questionPrimaryKey);
                    item.setMemberPrimaryKey(memberPrimaryKey);
                    item.setQuestionTypePrimaryKey(questionTypePrimaryKey);
                    item.setQuestionTypeString(questionTypeString);
                    item.setQuestionDatetime(questionDatetime);
                    item.setQuestionTitle(cleanQuestionTitle);
                    item.setQuestionContent(questionContent);
                    item.setAnswerDatetime(answerDatetime);
                    item.setAnswerTitle(answerTitle);
                    item.setAnswerContent(answerContent);
                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;


            } else {
                result_none.setVisibility(View.VISIBLE);
                itemAdapter.isMoreInfo = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void allClear() {
        index = 0;
        items.clear();
        itemAdapter.isMoreInfo = true;
        itemAdapter.notifyDataSetChanged();
        recyclerView.removeAllViews();
    }














    public void callSUHDialogType1Dialog(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    public void callCRHPeriodSelectDialog(String title) {
        crhPeriodSelectDialog.show();
        crhPeriodSelectDialog.setTitle(title);
        crhPeriodSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crhPeriodSelectDialog.dismiss();
            }
        });
        crhPeriodSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (crhPeriodSelectDialog.isOnItemClicked) {
                    period_index = crhPeriodSelectDialog.period_index;
                    period_size = periodSizes[period_index];

                    recent_day_textview.setText(periodSizeString[period_index]+" 기준");

                    allClear();
                    mobile5541();



                    crhPeriodSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    public void callQuestionAnswerDialog(String title) {
        questionAnswerDialog.show();
         questionAnswerDialog.setApplyContents();

        questionAnswerDialog.setTitle(title);
        questionAnswerDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionAnswerDialog.dismiss();
            }
        });
        questionAnswerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }
}
