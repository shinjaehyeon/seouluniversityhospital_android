package org.snuh.www.app.clinicreservation;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class TimeItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public Button time_button;
    public TextView time_textview;

    /**************************************************************/

    public TimeItemViewHolder(View view) {
        super(view);
        this.view = view;

        time_button = (Button) view.findViewById(R.id.time_button);
        time_textview = (TextView) view.findViewById(R.id.time_textview);
    }

    public void setMargin(Activity activity) {
        android.support.v7.widget.GridLayoutManager.LayoutParams params = (android.support.v7.widget.GridLayoutManager.LayoutParams) view.getLayoutParams();
        params.rightMargin = (int) activity.getResources().getDimension(R.dimen.mobile4660_time_list_margin_right);
        view.setLayoutParams(params);
    }
}
