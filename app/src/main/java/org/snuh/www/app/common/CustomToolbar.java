package org.snuh.www.app.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class CustomToolbar extends RelativeLayout {
    View v;
    Context context;

    /************************************************************************************************/

    RelativeLayout backButton;
    RelativeLayout custom_toolbar;
    TextView title;
    RelativeLayout top_border;
    RelativeLayout bottom_border;

    /************************************************************************************************/

    public CustomToolbar(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        initView();
        getAttrs(attrs);
    }

    public CustomToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.custom_toolbar, this, false);
        addView(v);

        setConnectViewId();
        setObjectEvent();
    }

    public void setConnectViewId() {
        backButton = (RelativeLayout) v.findViewById(R.id.backButton);
        custom_toolbar = (RelativeLayout) v.findViewById(R.id.custom_toolbar);
        title = (TextView) v.findViewById(R.id.title);
        top_border = (RelativeLayout) v.findViewById(R.id.top_border);
        bottom_border = (RelativeLayout) v.findViewById(R.id.bottom_border);
    }

    public void setObjectEvent() {
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });
    }

    /************************************************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    /************************************************************************************************/

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Mobile0000_customtopbar);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Mobile0000_customtopbar, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {
        // 상단바 배경 설정
        int bgRes = typedArray.getResourceId(R.styleable.Mobile0000_customtopbar_setTopBarBackground, R.drawable.ripple_button_type_3);
        custom_toolbar.setBackgroundResource(bgRes);

        // 상단바 타이틀 텍스트 설정
        String title_str = typedArray.getString(R.styleable.Mobile0000_customtopbar_setTitle);
        title.setText(title_str);

        // 상단바 타이틀 텍스트 색상 설정
        int textColor = typedArray.getColor(R.styleable.Mobile0000_customtopbar_setTitleColor, 0);
        title.setTextColor(textColor);

        // 상단바 타이틀 텍스트 크기 설정
        float textSize = (float) typedArray.getDimension(R.styleable.Mobile0000_customtopbar_setTitleSize, 0);
        textSize = textSize / getResources().getDisplayMetrics().density;
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        // 상단바 상단,하단 선 높이 설정
        int topHeight = (int) typedArray.getDimension(R.styleable.Mobile0000_customtopbar_setBorderTopHeight, 0);
        int bottomHeight = (int) typedArray.getDimension(R.styleable.Mobile0000_customtopbar_setBorderBottomHeight, 0);
        RelativeLayout.LayoutParams lp1 = (RelativeLayout.LayoutParams) top_border.getLayoutParams();
        lp1.height = topHeight;
        top_border.setLayoutParams(lp1);
        RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) bottom_border.getLayoutParams();
        lp2.height = bottomHeight;
        bottom_border.setLayoutParams(lp2);

        // 상단바 상단,하단 선 색상 설정
        int topColor = typedArray.getColor(R.styleable.Mobile0000_customtopbar_setBorderTopColor, 0);
        top_border.setBackgroundColor(topColor);
        int bottomColor = typedArray.getColor(R.styleable.Mobile0000_customtopbar_setBorderBottomColor, 0);
        bottom_border.setBackgroundColor(bottomColor);

        // 뒤로가기 버튼을 보이게 할건지 말건지 설정
        boolean isVisible = typedArray.getBoolean(R.styleable.Mobile0000_customtopbar_setBackButtonVisible, true);
        if (isVisible) {
            backButton.setVisibility(View.VISIBLE);
        } else {
            backButton.setVisibility(View.GONE);
        }

        typedArray.recycle();
    }
}
