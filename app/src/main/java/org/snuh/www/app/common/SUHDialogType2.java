package org.snuh.www.app.common;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;

public class SUHDialogType2 extends Dialog {
    Context context;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    Button select1LG;
    Button select2SKT;
    Button select3KT;
    Button select4LGFrugal;
    Button select5SKTFrugal;
    Button select6KTFrugal;

    int currentSelectNumber;

    /**************************************************************/

    public SUHDialogType2(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.suh_dialog_type_2);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기


    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        select1LG = (Button) findViewById(R.id.select1LG);
        select2SKT = (Button) findViewById(R.id.select2SKT);
        select3KT = (Button) findViewById(R.id.select3KT);
        select4LGFrugal = (Button) findViewById(R.id.select4LGFrugal);
        select5SKTFrugal = (Button) findViewById(R.id.select5SKTFrugal);
        select6KTFrugal = (Button) findViewById(R.id.select6KTFrugal);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        select1LG.setOnClickListener(phoneCompanyButtonClickEvent);
        select2SKT.setOnClickListener(phoneCompanyButtonClickEvent);
        select3KT.setOnClickListener(phoneCompanyButtonClickEvent);
        select4LGFrugal.setOnClickListener(phoneCompanyButtonClickEvent);
        select5SKTFrugal.setOnClickListener(phoneCompanyButtonClickEvent);
        select6KTFrugal.setOnClickListener(phoneCompanyButtonClickEvent);
    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public int getCurrentSelectedMobileCompanyNumber() {
        return this.currentSelectNumber;
    }

    public void setParentSelectedNumber(int n) {
        currentSelectNumber = n;
        switch (n) {
            case 1:
                select1LG.setBackgroundResource(R.drawable.ripple_button_type_2);
                select1LG.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 2:
                select2SKT.setBackgroundResource(R.drawable.ripple_button_type_2);
                select2SKT.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 3:
                select3KT.setBackgroundResource(R.drawable.ripple_button_type_2);
                select3KT.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 4:
                select4LGFrugal.setBackgroundResource(R.drawable.ripple_button_type_2);
                select4LGFrugal.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 5:
                select5SKTFrugal.setBackgroundResource(R.drawable.ripple_button_type_2);
                select5SKTFrugal.setTextColor(Color.parseColor("#ffffff"));
                break;
            case 6:
                select6KTFrugal.setBackgroundResource(R.drawable.ripple_button_type_2);
                select6KTFrugal.setTextColor(Color.parseColor("#ffffff"));
                break;
            default:

                break;
        }
    }

    /**************************************************************/

    View.OnClickListener phoneCompanyButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.select1LG:
                    currentSelectNumber = 1;
                    dismiss();
                    break;
                case R.id.select2SKT:
                    currentSelectNumber = 2;
                    dismiss();
                    break;
                case R.id.select3KT:
                    currentSelectNumber = 3;
                    dismiss();
                    break;
                case R.id.select4LGFrugal:
                    currentSelectNumber = 4;
                    dismiss();
                    break;
                case R.id.select5SKTFrugal:
                    currentSelectNumber = 5;
                    dismiss();
                    break;
                case R.id.select6KTFrugal:
                    currentSelectNumber = 6;
                    dismiss();
                    break;
                default:

                    break;
            }
        }
    };
}
