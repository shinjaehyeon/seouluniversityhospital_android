package org.snuh.www.app.departmentdetail;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.UseFul;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile5100 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;

    /**************************************************************/

    NestedScrollView nestedScrollView;

    LinearLayout image_and_tab_area;
    LinearLayout viewpager_area;

    Mobile5100ImageAndTab mobile5100ImageAndTab;
    Mobile5100ViewPager mobile5100ViewPager;

    int department_pk;
    String department;

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile5100);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기


        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기


    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {
        department_pk = getIntent().getIntExtra("department_pk", 0);
        department = getIntent().getStringExtra("department")+"";
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        topbar.setTitle(department);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);

        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);

        image_and_tab_area = (LinearLayout) findViewById(R.id.image_and_tab_area);
        viewpager_area = (LinearLayout) findViewById(R.id.viewpager_area);

        mobile5100ImageAndTab = new Mobile5100ImageAndTab(activity);
        mobile5100ViewPager = new Mobile5100ViewPager(activity);

        mobile5100ImageAndTab.setDepartmentPk(department_pk);
        mobile5100ImageAndTab.inflateView();
        mobile5100ImageAndTab.setConnectViewId();
        mobile5100ViewPager.inflateView();
        mobile5100ViewPager.setConnectViewId();

        mobile5100ImageAndTab.setMobile5100ViewPager(mobile5100ViewPager);

        mobile5100ViewPager.setMobile5100ImageAndTab(mobile5100ImageAndTab);

        mobile5100ViewPager.setDepartmentPk(department_pk);
        mobile5100ViewPager.setNestedScrollView(nestedScrollView);

        mobile5100ImageAndTab.setRealInItView();
        mobile5100ImageAndTab.mobile5101();
        mobile5100ViewPager.setRealInItView();

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                image_and_tab_area.addView(mobile5100ImageAndTab);
            }
        });



        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewpager_area.addView(mobile5100ViewPager);
            }
        });
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    /**************************************************************/










    /**************************************************************/

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
