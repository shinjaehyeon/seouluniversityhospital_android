package org.snuh.www.app.departmentitemset;

public class DepartmentItem {
    public int pk;
    public String department;
    public DepartmentItemViewHolder viewHolder;

    public void setPk(int n) {
        this.pk = n;
    }
    public int getPk() {
        return this.pk;
    }

    public void setDepartment(String s) {
        this.department = s;
    }
    public String getDepartment() {
        return this.department;
    }

    public void setViewHolder(DepartmentItemViewHolder viewHolder) {
        this.viewHolder = viewHolder;
    }
    public DepartmentItemViewHolder getViewHolder() {
        return this.viewHolder;
    }
}
