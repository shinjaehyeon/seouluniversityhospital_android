package org.snuh.www.app.departmentdoctor;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class DepartmentAlignSelectDialog extends Dialog {
    Context context;
    UseFul useFul;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    int buttonIdResources[] = {
            R.id.button_index_0, R.id.button_index_1, R.id.button_index_2, R.id.button_index_3, R.id.button_index_4, R.id.button_index_5,
            R.id.button_index_6, R.id.button_index_7, R.id.button_index_8, R.id.button_index_9, R.id.button_index_10, R.id.button_index_11,
            R.id.button_index_12, R.id.button_index_13, R.id.button_index_14
    };
    ArrayList<Button> buttons = new ArrayList<Button>();

    Button previousSelectedButton;

    /**************************************************************/

    String periodSizeString[] = {"진료과 전체",
            "ㄱ 으로 시작하는 진료과",  "ㄴ 으로 시작하는 진료과", "ㄷ 으로 시작하는 진료과", "ㄹ 으로 시작하는 진료과",
            "ㅁ 으로 시작하는 진료과",  "ㅂ 으로 시작하는 진료과", "ㅅ 으로 시작하는 진료과", "ㅇ 으로 시작하는 진료과",
            "ㅈ 으로 시작하는 진료과",  "ㅊ 으로 시작하는 진료과", "ㅋ 으로 시작하는 진료과", "ㅌ 으로 시작하는 진료과",
            "ㅍ 으로 시작하는 진료과",  "ㅎ 으로 시작하는 진료과"
    };
    int align_index = 0;

    /**************************************************************/


    public DepartmentAlignSelectDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.department_align_select_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(context);


        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        for (int i=0; i<buttonIdResources.length; i++) {
            buttons.add((Button) findViewById(buttonIdResources[i]));
        }
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<buttons.size(); i++) {
            buttons.get(i).setTag(i);
            buttons.get(i).setOnClickListener(itemOnClickListener);
        }
    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    /**************************************************************/


    /**************************************************************/

    boolean isOnItemClicked = false;

    // 진료과 리스트중 하나를 클릭 했을 시 발생하는 이벤트 설정
    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isOnItemClicked = true;

            int index = (int) v.getTag();
            align_index = index;

            ((Button) v).setBackgroundResource(R.drawable.ripple_button_type_2);
            ((Button) v).setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_on_color));

            if (previousSelectedButton != null) {
                if ((int) previousSelectedButton.getTag() != (int) ((Button) v).getTag()) {
                    previousSelectedButton.setBackgroundResource(R.drawable.ripple_button_type_6);
                    previousSelectedButton.setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_color));
                }
            }

            previousSelectedButton = (Button) v;

            dismiss();
        }
    };


}
