package org.snuh.www.app.permission;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.login.Mobile2000;

public class Mobile1500 extends AppCompatActivity {
    Activity activity;
    Context context;

    /**************************************************************/

    private static final int REQUEST_PERMISSION_CODE = 1;
    private static String[] PERMISSION_ALL = {
        Manifest.permission.INTERNET,
        Manifest.permission.CHANGE_WIFI_MULTICAST_STATE,
        Manifest.permission.CHANGE_WIFI_STATE,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.VIBRATE,
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.WRITE_CALL_LOG,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static String[] PERMISSION_ALL_CHECK = {
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.WRITE_CALL_LOG,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    };

    /**************************************************************/

    Button acceptButton;

    /**************************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile1500);

        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }





    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        acceptButton = (Button) findViewById(R.id.acceptButton);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        acceptButton.setOnClickListener(acceptButtonClickEvent);
    }

    // 로그인페이지로 이동
    public void goToLogin() {
        Intent intent = new Intent(this, Mobile2000.class);
        startActivity(intent);
    }

    /**************************************************************/

    // acceptButton 클릭 이벤트
    View.OnClickListener acceptButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startPermission();
        }
    };

    public void startPermission() {
        try {
            if (!checkPermissions(activity, PERMISSION_ALL)) {
                // 하나라도 권한이 허용 되지 않은게 있다면
                requestAllPermissions(activity);
            } else {
                // 모든 권한이 허용되었다면

            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public boolean checkPermissions(Activity activity, String[] permission) throws IllegalAccessException {
        int[] grantlist = new int[permission.length];
        Log.i("MyTag", "permission.length = "+permission.length);
        for (int i=0; i<permission.length; i++) {
            grantlist[i] = ContextCompat.checkSelfPermission(activity, permission[i]);
        }
        for (int i=0; i<grantlist.length; i++) {
            Log.i("MyTag", "grantlist["+i+"] = "+grantlist[i]);
            if (grantlist[i] == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public static void requestAllPermissions(Activity activity) {
        Log.i("MyTag", "requestAllPermissions() 실행");
        ActivityCompat.requestPermissions(activity, PERMISSION_ALL, REQUEST_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            try {
                if (checkPermissions(activity, PERMISSION_ALL_CHECK)) {
                    // 모든 권한 허락 요청 받음
                    new UseFul(activity).showToast("제시된 권한을 모두 허용하셨습니다.");

                    // 로그인 페이지로 이동
                    goToLogin();

                    finish();
                } else {
                    showRequestAgainDialog();
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showRequestAgainDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("앱에 필요한 권한을 모두 허용해주셔야 이용 가능합니다. 설정에서 권한을 모두 허용해 주세요.");
        builder.setPositiveButton("설정바로가기", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(Uri.parse("package:"+getPackageName()));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                    startActivity(intent);
                }
            }
        });
        builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityCompat.finishAffinity(activity);
            }
        });
        builder.create();
        builder.show();
    }
}
