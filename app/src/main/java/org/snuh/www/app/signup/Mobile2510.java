package org.snuh.www.app.signup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;

public class Mobile2510 extends Fragment {
    Activity activity;
    View rootView;

    /**************************************************************/

    LinearLayout step01;
    LinearLayout step02;
    LinearLayout step03;

    /**************************************************************/

    LinearLayout allTermsAcceptButtonLabel;
    LinearLayout serviceTermsAcceptButtonLabel;
    LinearLayout personalTermsAcceptButtonLabel;

    CheckBox allTermsAcceptButton;
    CheckBox serviceTermsAcceptButton;
    CheckBox personalTermsAcceptButton;

    boolean isAllTermsAcceptChecked = false;
    boolean isServiceTermsAcceptChecked = false;
    boolean isPersonalTermsAcceptChecked = false;

    /**************************************************************/

    TextView serviceTermsText;
    TextView personalTermsText;

    /**************************************************************/

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 2516:
                    mobile2516(msg.obj); // 약관 동의 내용 서버에 요청하기 후처리
                    break;
            }
        }
    };





    /**************************************************************/

    public Mobile2510() {

    }

    @SuppressLint("ValidFragment")
    public Mobile2510(Activity activity) {
        this.activity = activity;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile2510, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        mobile2515(); // 약관 동의 내용 서버에 요청하기

        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        step01 = (LinearLayout) rootView.findViewById(R.id.step01);
        step02 = (LinearLayout) rootView.findViewById(R.id.step02);
        step03 = (LinearLayout) rootView.findViewById(R.id.step03);

        allTermsAcceptButtonLabel = (LinearLayout) rootView.findViewById(R.id.allTermsAcceptButtonLabel);
        serviceTermsAcceptButtonLabel = (LinearLayout) rootView.findViewById(R.id.serviceTermsAcceptButtonLabel);
        personalTermsAcceptButtonLabel = (LinearLayout) rootView.findViewById(R.id.personalTermsAcceptButtonLabel);

        allTermsAcceptButton = (CheckBox) rootView.findViewById(R.id.allTermsAcceptButton);
        serviceTermsAcceptButton = (CheckBox) rootView.findViewById(R.id.serviceTermsAcceptButton);
        personalTermsAcceptButton = (CheckBox) rootView.findViewById(R.id.personalTermsAcceptButton);


        serviceTermsText = (TextView) rootView.findViewById(R.id.serviceTermsText);
        personalTermsText = (TextView) rootView.findViewById(R.id.personalTermsText);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        allTermsAcceptButton.setOnCheckedChangeListener(allTermsAcceptButtonCheckboxEvent);
        serviceTermsAcceptButton.setOnCheckedChangeListener(serviceTermsAcceptButtonCheckboxEvent);
        personalTermsAcceptButton.setOnCheckedChangeListener(personalTermsAcceptButtonCheckboxEvent);

        allTermsAcceptButtonLabel.setOnClickListener(allTermsAcceptButtonLabelClickEvent);
        serviceTermsAcceptButtonLabel.setOnClickListener(serviceTermsAcceptButtonLabelClickEvent);
        personalTermsAcceptButtonLabel.setOnClickListener(personalTermsAcceptButtonLabelClickEvent);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {
        step01.post(new Runnable() {
            @Override
            public void run() {
                int width = step01.getWidth();
                int height = width;

                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) step01.getLayoutParams();
                lp.height = height;

                step01.setLayoutParams(lp);
            }
        });
        step02.post(new Runnable() {
            @Override
            public void run() {
                int width = step02.getWidth();
                int height = width;

                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) step02.getLayoutParams();
                lp.height = height;

                step02.setLayoutParams(lp);
            }
        });
        step03.post(new Runnable() {
            @Override
            public void run() {
                int width = step03.getWidth();
                int height = width;

                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) step03.getLayoutParams();
                lp.height = height;

                step03.setLayoutParams(lp);
            }
        });
    }

    /**************************************************************/

    // 약관 동의 내용 서버에 요청하기
    public void mobile2515() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile2515");
        params.put("member", new ShareInfo(activity).getMember());

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(2516);
        inLet.start();
    }
    // 약관 동의 내용 서버에 요청하기 후처리
    public void mobile2516(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                String service_terms = jsonObject.getString("service_terms").replaceAll("<br />", "\n");
                String personal_terms = jsonObject.getString("personal_terms").replaceAll("<br />", "\n");

                serviceTermsText.setText(new UseFul(activity).textClean(service_terms));
                personalTermsText.setText(new UseFul(activity).textClean(personal_terms));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    /**************************************************************/

    CompoundButton.OnCheckedChangeListener  allTermsAcceptButtonCheckboxEvent = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            isAllTermsAcceptChecked = isChecked;
            if (isChecked) {
                serviceTermsAcceptButton.setChecked(true);
                personalTermsAcceptButton.setChecked(true);
            } else {
                serviceTermsAcceptButton.setChecked(false);
                personalTermsAcceptButton.setChecked(false);
            }
        }
    };

    CompoundButton.OnCheckedChangeListener  serviceTermsAcceptButtonCheckboxEvent = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            isServiceTermsAcceptChecked = isChecked;
        }
    };

    CompoundButton.OnCheckedChangeListener  personalTermsAcceptButtonCheckboxEvent = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            isPersonalTermsAcceptChecked = isChecked;
        }
    };




    View.OnClickListener allTermsAcceptButtonLabelClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isAllTermsAcceptChecked) {
                allTermsAcceptButton.setChecked(false);
            } else {
                allTermsAcceptButton.setChecked(true);
            }
        }
    };

    View.OnClickListener serviceTermsAcceptButtonLabelClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isServiceTermsAcceptChecked) {
                serviceTermsAcceptButton.setChecked(false);
            } else {
                serviceTermsAcceptButton.setChecked(true);
            }
        }
    };

    View.OnClickListener personalTermsAcceptButtonLabelClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isPersonalTermsAcceptChecked) {
                personalTermsAcceptButton.setChecked(false);
            } else {
                personalTermsAcceptButton.setChecked(true);
            }
        }
    };
}
