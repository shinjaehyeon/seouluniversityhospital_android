package org.snuh.www.app.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.main.Mobile3000;
import org.snuh.www.app.notice.Mobile7500;
import org.snuh.www.app.setting.Mobile9000;

import java.util.ArrayList;

public class CustomBottomMenubar extends LinearLayout {
    View v;
    Context context;
    UseFul useFul;

    public static final int DURATION_VALUE = 600;
    public static final int INTERPOLATOR_VALUE = android.R.anim.decelerate_interpolator;

    /************************************************************************************************/

    ArrayList<Button> menus = new ArrayList<Button>();
    ArrayList<ImageView> menu_icons = new ArrayList<ImageView>();
    ArrayList<TextView> menu_texts = new ArrayList<TextView>();

    int[] icon_ons = {
            R.drawable.ic_mobile0000_bottom_menubar_icon_1_on,
            R.drawable.ic_mobile0000_bottom_menubar_icon_2_on,
            R.drawable.ic_mobile0000_bottom_menubar_icon_3_on,
            R.drawable.ic_mobile0000_bottom_menubar_icon_4_on
    };
    int[] icon_offs = {
            R.drawable.ic_mobile0000_bottom_menubar_icon_1_off,
            R.drawable.ic_mobile0000_bottom_menubar_icon_2_off,
            R.drawable.ic_mobile0000_bottom_menubar_icon_3_off,
            R.drawable.ic_mobile0000_bottom_menubar_icon_4_off
    };

    ImageView theDisplayAndHiddenButton;

    /************************************************************************************************/

    DrawerLayout drawerLayout;
    View drawerView;

    boolean isOpened = false;
    boolean isMoved = false;


    boolean isVisible = false;

    /************************************************************************************************/

    int measuredwidth;
    int measuredheight;

    public CustomBottomMenubar(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public CustomBottomMenubar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;
        initView();
        getAttrs(attrs);
    }

    public CustomBottomMenubar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
        getAttrs(attrs, defStyleAttr);
    }

    public void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        v = li.inflate(R.layout.custom_bottom_menubar, this, false);
        addView(v);

        getSizes();
        setConnectViewId();
        setObjectEvent();
    }

    public void getSizes() {
        DisplayMetrics metrics = new DisplayMetrics();   //for all android versions
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        measuredwidth  = metrics.widthPixels;
        measuredheight = metrics.heightPixels;
    }

    public void setConnectViewId() {
        useFul = new UseFul(context);

        menus.add((Button) v.findViewById(R.id.menu1));
        menus.add((Button) v.findViewById(R.id.menu2));
        menus.add((Button) v.findViewById(R.id.menu3));
        menus.add((Button) v.findViewById(R.id.menu4));

        menu_icons.add((ImageView) v.findViewById(R.id.menu1_icon));
        menu_icons.add((ImageView) v.findViewById(R.id.menu2_icon));
        menu_icons.add((ImageView) v.findViewById(R.id.menu3_icon));
        menu_icons.add((ImageView) v.findViewById(R.id.menu4_icon));

        menu_texts.add((TextView) v.findViewById(R.id.menu1_text));
        menu_texts.add((TextView) v.findViewById(R.id.menu2_text));
        menu_texts.add((TextView) v.findViewById(R.id.menu3_text));
        menu_texts.add((TextView) v.findViewById(R.id.menu4_text));

        theDisplayAndHiddenButton = (ImageView) v.findViewById(R.id.theDisplayAndHiddenButton);
    }

    public void setObjectEvent() {
        // 홈 버튼 눌렀을 때
        menus.get(0).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(Mobile3000.class);
            }
        });

        // 전체메뉴 버튼 눌렀을 때
        menus.get(1).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(drawerView);
            }
        });

        // 알림 버튼 눌렀을 때
        menus.get(2).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(Mobile7500.class);
            }
        });

        // 설정 버튼 눌렀을 때
        menus.get(3).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActivity(Mobile9000.class);
            }
        });

    }

    /************************************************************************************************/


    public void setTheDisplayAndHiddenButtonEvent(View.OnClickListener e) {
        theDisplayAndHiddenButton.setOnClickListener(e);
    }

    public void setIsVisible(boolean b) {
        this.isVisible = b;
    }

    public boolean getIsVisible() {
        return this.isVisible;
    }


    public void setVisibleButton() {
        theDisplayAndHiddenButton.setImageResource(R.drawable.ic_mobile0000_custom_bottom_menubar_visible_button_icon);
    }

    public void setHiddenButton() {
        theDisplayAndHiddenButton.setImageResource(R.drawable.ic_mobile0000_custom_bottom_menubar_hidden_button_icon);
    }



    public void setDrawerLayout(DrawerLayout drawerLayouts, View drawerViews) {
        this.drawerLayout = drawerLayouts;
        this.drawerView = drawerViews;
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                isOpened = true;
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                isOpened = false;
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                switch (newState) {
                    case 0: // 멈췄을 때
                        isMoved = false;
                        break;
                    case 2: // 움직일 때
                        isMoved = true;
                        break;
                    default:

                        break;
                }
            }
        });
    }

    public void closeDrawerLayout() {
        drawerLayout.closeDrawers();
    }

    public boolean isOpened() {
        return this.isOpened;
    }

    public boolean isMoved() {
        return this.isMoved;
    }

    public void setActiveMenu(int index) {
        for (int i=0; i<menu_icons.size(); i++) {
            menu_icons.get(i).setImageResource(icon_offs[i]);
            menu_texts.get(i).setTextColor(useFul.getRcolor(R.color.custombottommenubar_menu_text_off_color));
        }

        menu_icons.get(index).setImageResource(icon_ons[index]);
        menu_texts.get(index).setTextColor(useFul.getRcolor(R.color.custombottommenubar_menu_text_on_color));
    }

    /************************************************************************************************/

    public void goToActivity(Class c) {
        Intent intent = new Intent(context, c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        ((Activity) context).startActivity(intent);
    }

    /************************************************************************************************/

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Mobile0000_custombottommenubar);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Mobile0000_custombottommenubar, defStyle, 0);
        setTypeArray(typedArray);

    }

    private void setTypeArray(TypedArray typedArray) {
        // 활성화 이벤트 초기값 설정
        int number = typedArray.getInteger(R.styleable.Mobile0000_custombottommenubar_setActiveMenu, 0);

        if (number != 0) {
            int index = number - 1;

            menu_icons.get(index).setImageResource(icon_ons[index]);
            menu_texts.get(index).setTextColor(useFul.getRcolor(R.color.custombottommenubar_menu_text_on_color));
        }
        typedArray.recycle();
    }
}
