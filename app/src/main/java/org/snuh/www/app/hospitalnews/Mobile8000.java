package org.snuh.www.app.hospitalnews;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.CustomBottomMenubar;
import org.snuh.www.app.common.CustomBottomMenubarEventSet;
import org.snuh.www.app.common.CustomToolbar;
import org.snuh.www.app.common.DrawerLayoutSetting;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.departmentdetail.Mobile5100;
import org.snuh.www.app.departmentitemset.DepartmentItem;
import org.snuh.www.app.departmentitemset.DepartmentItemAdapter;
import org.snuh.www.app.hospitalnewsitemset.HospitalNewsItem;
import org.snuh.www.app.hospitalnewsitemset.HospitalNewsItemAdapter;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Mobile8000 extends AppCompatActivity {
    Activity activity;
    Context context;
    DrawerLayoutSetting drawerLayoutSetting;
    CustomToolbar topbar;
    CustomBottomMenubar bottomMenubar;
    CustomBottomMenubarEventSet bottomMenubarEventSet;
    UseFul useFul;



    NestedScrollView scrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<HospitalNewsItem> items = new ArrayList<HospitalNewsItem>();
    HospitalNewsItemAdapter itemAdapter;

    int index = 0;
    int view_num = 10;



    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 8002: // 병원뉴스 요청 후처리
                    mobile8002(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile8000);


        setSaveActivity(this); // 액티비티 값을 글로벌 변수에 저장하기
        getIntentInfo(); // 인텐트 정보 받아오기
        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기

        setViewSize(); // 필요한 뷰 사이즈 조정하기
        setObjectSetting(); // 객체에 여러가지 설정하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setDrawerLayout(); // DrawerLayout 설정하기

        setRecyclerView();


        mobile8001(); // 병원뉴스 리스트 요청
    }

    @Override
    protected void onStart() {
        super.onStart();
        new UseFul(activity).setAppRunning(1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        new UseFul(activity).setAppRunning(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new UseFul(activity).setAppRunning(1);
    }








    // 액티비티 값을 글로벌 변수에 저장하기
    public void setSaveActivity(Activity activity) {
        this.activity = activity;
        this.context = activity;
    }

    // 인테트 정보 받아오기
    public void getIntentInfo() {

    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        topbar = (CustomToolbar) findViewById(R.id.topbar);
        bottomMenubar = (CustomBottomMenubar) findViewById(R.id.bottomMenubar);
        bottomMenubarEventSet = new CustomBottomMenubarEventSet(activity, bottomMenubar);
        useFul = new UseFul(activity);


        scrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 객체에 여러가지 설정하기
    public void setObjectSetting() {

    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {

    }

    // DrawerLayout 설정하기
    public void setDrawerLayout() {
        drawerLayoutSetting = new DrawerLayoutSetting(activity);
    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        itemAdapter = new HospitalNewsItemAdapter(recyclerView, items, activity, itemOnClickListener, scrollView);
        recyclerView.setAdapter(itemAdapter);

        itemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                items.add(null);
                itemAdapter.notifyItemInserted(items.size() - 1);

                mobile8001();
            }
        });

        // 스크롤로 더보기 구현하고 싶으면
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollViewPos = scrollView.getScrollY();
                int TextView_lines = scrollView.getChildAt(0).getBottom() - scrollView.getHeight();

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();


                    if (TextView_lines == scrollViewPos) {
                        if (itemAdapter.isMoreInfo == true) {
                            itemAdapter.totalItemCount = linearLayoutManager.getItemCount();
                            itemAdapter.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                            if (!itemAdapter.isLoading && itemAdapter.totalItemCount <= (itemAdapter.lastVisibleItem + itemAdapter.visibleThreshold)) {
                                itemAdapter.isLoading = true;

                                if (itemAdapter.onLoadMoreListener != null) {
                                    itemAdapter.onLoadMoreListener.onLoadMore();
                                }

                            }
                        } else {
                            // new UseFul(activity).showToast("불러올 데이터가 없습니다. - 스크롤");
                        }
                    }


            }
        });

    }

    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            HospitalNewsItem item = (HospitalNewsItem) v.getTag();

            int pk = item.getPk(); // 병원뉴스 고유번호
            Log.i("MyTags", "병원뉴스 pk = "+pk);
            // Log.i("MyTags", "병원뉴스 pk 내용 = "+item.getContent());

            String title = item.getTitle();
            String datetime = item.getDatetime();
            int viewIndex = item.getViewIndex();
            String content = item.getContent();

            callHospitalDetailDialog("병원뉴스 상세보기", title, datetime, viewIndex, content);
        }
    };



    public void callHospitalDetailDialog(String title, String news_title, String news_datetime, int news_view_index, String news_content) {
        final HospitalDetailDialog hospitalDetailDialog = new HospitalDetailDialog(this);
        hospitalDetailDialog.show();

        hospitalDetailDialog.setNewsTitle(news_title);
        hospitalDetailDialog.setNewsDateTime(news_datetime);
        hospitalDetailDialog.setNewsViewIndex(news_view_index);
        hospitalDetailDialog.setNewsContent(news_content);


        hospitalDetailDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hospitalDetailDialog.dismiss();
            }
        });
        hospitalDetailDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }







    // 병원뉴스 리스트 요청
    public void mobile8001() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile8001");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("index", index);
        params.put("view_num", view_num);

        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(8002);
        inLet.start();
    }

    // 병원뉴스 리스트 요청 후처리
    public void mobile8002(Object obj) {
        String data = obj.toString();
        Log.i("MyTags", "mobile8002 data = "+data);

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                // result_none.setVisibility(View.GONE);
                String more_info = jsonObject.getString("more_info");

                if (items.size() >= 1) {
                    items.remove(items.size() - 1);
                    itemAdapter.notifyItemRemoved(items.size());
                }

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int suhnews = jsonObject1.getInt("suhnews");
                    String title = jsonObject1.getString("title");
                    String content = jsonObject1.getString("content");
                    String datetime = jsonObject1.getString("datetime");
                    int view_index = jsonObject1.getInt("view_index");

                    HospitalNewsItem item = new HospitalNewsItem();
                    item.setPk(suhnews);
                    item.setTitle(title);
                    item.setContent(content);
                    item.setDatetime(datetime);
                    item.setViewIndex(view_index);

                    items.add(item);
                }
                itemAdapter.setLoaded();
                itemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    itemAdapter.isMoreInfo = true;
                } else {
                    itemAdapter.isMoreInfo = false;
                }
                index += view_num;

            } else {
                // result_none.setVisibility(View.VISIBLE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }








    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!bottomMenubar.isMoved()) { // DrawerLayout이 움직이지 않을 때만 실행
            if (bottomMenubar.isOpened() == true) {
                bottomMenubar.closeDrawerLayout();
            } else {
                finish();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
