package org.snuh.www.app.clinicreservation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snuh.www.app.R;
import org.snuh.www.app.common.InLet;
import org.snuh.www.app.common.OnLoadMoreListener;
import org.snuh.www.app.common.SUHDialogType1;
import org.snuh.www.app.common.ShareInfo;
import org.snuh.www.app.common.UseFul;
import org.snuh.www.app.crhitemset.CRHItem;
import org.snuh.www.app.crhitemset.CRHItemAdapter;
import org.snuh.www.app.crhitemset.CRHItemViewHolder;

import java.util.ArrayList;
import java.util.Calendar;

public class Mobile4700 extends Fragment {
    Activity activity;
    View rootView;
    UseFul useFul;
    RelativeLayout progressLayout;

    /**************************************************************/

    int periodSizes[] = {0, 7, 30, 60, 90, 150};
    String periodSizeString[] = {"전체 기간", "최근 7일",  "최근 30일", "최근 60일", "최근 90일", "최근 150일"};
    int period_size = 30; // 최근 n일
    int period_index = 2;

    TextView recent_day_textview;
    Button period_setting_button;

    CRHPeriodSelectDialog crhPeriodSelectDialog;



    int index = 0;
    int view_num = 6;

    NestedScrollView scrollView;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<CRHItem> crhItems = new ArrayList<CRHItem>();
    CRHItemAdapter crhItemAdapter;

    CRHItem cancelWantCrhItem;

    Button moreViewButton;

    /**************************************************************/


    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 3011:
                    mobile3011(msg.obj);
                    break;
                case 3021:
                    mobile3021(msg.obj);
                    break;
                default:

                    break;
            }
        }
    };


    /**************************************************************/

    public Mobile4700() {

    }

    @SuppressLint("ValidFragment")
    public Mobile4700(Activity activity) {
        this.activity = activity;
    }

    @SuppressLint("ValidFragment")
    public Mobile4700(Activity activity, RelativeLayout progressLayout) {
        this.activity = activity;
        this.progressLayout = progressLayout;
    }

    /**************************************************************/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mobile4700, parentViewGroup, false);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기
        setViewSize(); // 필요한 뷰 사이즈 조정하기

        setRecyclerView();
        mobile3010(); // 최근 진료예약 내역 요청 보내기

        return rootView;
    }

    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(activity);

        recent_day_textview = (TextView) rootView.findViewById(R.id.recent_day_textview);
        period_setting_button = (Button) rootView.findViewById(R.id.period_setting_button);

        crhPeriodSelectDialog = new CRHPeriodSelectDialog(activity);
        crhPeriodSelectDialog.setFirstValue(2);

        scrollView = (NestedScrollView) rootView.findViewById(R.id.nestedScrollView);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        moreViewButton = (Button) rootView.findViewById(R.id.moreViewButton);
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        period_setting_button.setOnClickListener(periodSettingButtonEvent);
    }

    // 필요한 뷰 사이즈 조정하기
    public void setViewSize() {

    }

    // 리사이클러뷰 관련 환경 설정하기
    public void setRecyclerView() {
        recyclerView.setNestedScrollingEnabled(false);

        mLayoutManager = new GridLayoutManager(activity,1);
        mLayoutManager.setAutoMeasureEnabled(true);

        recyclerView.setLayoutManager(mLayoutManager);

        crhItemAdapter = new CRHItemAdapter(recyclerView, crhItems, activity, null, scrollView);
        recyclerView.setAdapter(crhItemAdapter);

        crhItemAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                crhItems.add(null);
                crhItemAdapter.notifyItemInserted(crhItems.size() - 1);

                mobile3010(); // 서버에 최근 진료예약 내역 요청
            }
        });

        // 스크롤로 더보기 구현하고 싶으면
//        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                int scrollViewPos = scrollView.getScrollY();
//                int TextView_lines = scrollView.getChildAt(0).getBottom() - scrollView.getHeight();
//
//                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//
//
//                    if (TextView_lines == scrollViewPos) {
//                        if (crhItemAdapter.isMoreInfo == true) {
//                            crhItemAdapter.totalItemCount = linearLayoutManager.getItemCount();
//                            crhItemAdapter.lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//
//                            if (!crhItemAdapter.isLoading && crhItemAdapter.totalItemCount <= (crhItemAdapter.lastVisibleItem + crhItemAdapter.visibleThreshold)) {
//                                crhItemAdapter.isLoading = true;
//
//                                if (crhItemAdapter.onLoadMoreListener != null) {
//                                    crhItemAdapter.onLoadMoreListener.onLoadMore();
//                                }
//
//                            }
//                        } else {
//                            // new UseFul(activity).showToast("불러올 데이터가 없습니다. - 스크롤");
//                        }
//                    }
//
//
//            }
//        });

        // 버튼으로 더보기 구현하고 싶으면
        moreViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (crhItemAdapter.isMoreInfo == true) {
                    if (!crhItemAdapter.isLoading) {
                        crhItemAdapter.isLoading = true;
                        if (crhItemAdapter.onLoadMoreListener != null) {
                            crhItemAdapter.onLoadMoreListener.onLoadMore();
                        }
                    }
                } else {
                    callSUHDialogType1Dialog("안내", "더 이상 불러올 데이터가 없습니다.", false);
                }
            }
        });
    }

    // 진료과 선택 다이얼로그 창 호출 함수
    public void callCRHPeriodSelectDialog(String title) {
        crhPeriodSelectDialog.show();
        crhPeriodSelectDialog.setTitle(title);
        crhPeriodSelectDialog.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crhPeriodSelectDialog.dismiss();
            }
        });
        crhPeriodSelectDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (crhPeriodSelectDialog.isOnItemClicked) {
                    period_index = crhPeriodSelectDialog.period_index;
                    period_size = periodSizes[period_index];

                    recent_day_textview.setText(periodSizeString[period_index]+" 기준");

                    synchronizationCRH(); // 진료예약내역 동기화



                    crhPeriodSelectDialog.isOnItemClicked = false;
                }
            }
        });
    }

    // 진료예약내역 동기화
    public void synchronizationCRH() {
        index = 0;
        int size = crhItems.size();
        crhItems.clear();
        crhItemAdapter.notifyDataSetChanged();
        recyclerView.clearDisappearingChildren();

        cancelWantCrhItem = null;


        mobile3010();
    }

    // 서버에 최근 진료예약 내역 요청
    public void mobile3010() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3010");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("index", index);
        params.put("view_num", view_num);
        params.put("period_size", period_size);
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3011);
        inLet.start();
    }

    // 서버에 최근 진료예약 내역 요청 후처리
    public void mobile3011(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                String more_info = jsonObject.getString("more_info");

                if (crhItems.size() >= 1) {
                    crhItems.remove(crhItems.size() - 1);
                    crhItemAdapter.notifyItemRemoved(crhItems.size());
                }

                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                    int pk = jsonObject1.getInt("suhcrh");
                    String doctor_name = jsonObject1.getString("doctor_name");
                    String department_string = jsonObject1.getString("department_string");
                    String clinic_reservation_date = jsonObject1.getString("clinic_reservation_date");
                    String clinic_reservation_time = jsonObject1.getString("clinic_reservation_time");
                    int status = jsonObject1.getInt("status");
                    String status_string = jsonObject1.getString("status_string");

                    int year = jsonObject1.getInt("year");
                    String month = useFul.attachZero(jsonObject1.getInt("month"));
                    String date = useFul.attachZero(jsonObject1.getInt("date"));


                    CRHItem crhItem = new CRHItem();
                    crhItem.setPrimaryKey(pk);
                    crhItem.setDoctorName(doctor_name+" 의료진");
                    crhItem.setDeparment(department_string+" 진료");
                    crhItem.setClinicReservationDate(clinic_reservation_date);
                    crhItem.setClinicReservationTime(clinic_reservation_time);
                    crhItem.setStatus(status);
                    crhItem.setStatusString(status_string);
                    crhItem.setCancelButtonClickEvent(cancelButtonClickEvent);
                    crhItem.setYear(year+"");
                    crhItem.setMonth(month);
                    crhItem.setDate(date);

                    crhItems.add(crhItem);
                }
                crhItemAdapter.setLoaded();
                crhItemAdapter.notifyItemChanged(index);
                if (more_info.equals("ok")) {
                    crhItemAdapter.isMoreInfo = true;
                } else {
                    crhItemAdapter.isMoreInfo = false;
                }
                index = index + view_num;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener cancelButtonClickEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cancelWantCrhItem = (CRHItem) v.getTag();

            int year = Integer.parseInt(cancelWantCrhItem.getYear());
            int month = Integer.parseInt(cancelWantCrhItem.getMonth());
            int date = Integer.parseInt(cancelWantCrhItem.getDate());

            Log.i("MyTags", "year = "+year);
            Log.i("MyTags", "month = "+month);
            Log.i("MyTags", "date = "+date);

            Calendar big = Calendar.getInstance();
            Calendar small = Calendar.getInstance();
            small.set(small.YEAR, year);
            small.set(small.MONTH, month-1);
            small.set(small.DATE, date);



            int dday = useFul.differDatetime(big, small);
            Log.i("MyTags", "날짜 차이 = "+dday);
            if (dday > -2) {
                callSUHDialogType1Dialog("안내", "진료예약일이 내일인 경우에는 취소할 수 없습니다.", false);
            } else {
                // new UseFul(activity).showToast("취소 버튼 눌른 진료예약 pk는 "+crhItem.getPk());
                callSUHDialogType1Dialog2("안내", "정말 해당 진료예약을 취소하시겠습니까?");
            }
        }
    };

    View.OnClickListener periodSettingButtonEvent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            callCRHPeriodSelectDialog("기간 선택");
        }
    };

    /**************************************************************/

    public void callSUHDialogType1Dialog(String title, String content, final boolean isFinish) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (isFinish) {
                    activity.finish();
                }
            }
        });
    }

    public void callSUHDialogType1Dialog2(String title, String content) {
        final SUHDialogType1 suhDialogType1 = new SUHDialogType1(activity);
        suhDialogType1.show();
        suhDialogType1.setTitle(title);
        suhDialogType1.setContent(content);
        suhDialogType1.setCloseButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setNegativeButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss();
            }
        });
        suhDialogType1.setPositiveButtonOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                suhDialogType1.dismiss(); // 팝업창 닫고
                progressLayout.setVisibility(View.VISIBLE); // 로딩 레이아웃 띄우고

                // 서버에 진료예약 취소 요청
                mobile3020();
            }
        });
        suhDialogType1.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    /**************************************************************/

    // 서버에 진료예약 취소 요청
    public void mobile3020() {
        ContentValues params = new ContentValues();
        params.put("act", "mobile3020");
        params.put("member", new ShareInfo(activity).getMember());
        params.put("crh_pk", cancelWantCrhItem.getPk());
        InLet inLet = new InLet(activity);
        inLet.setInLetAct("basic");
        inLet.setProtocolType("http");
        // inLet.setUrl("");
        inLet.setParams(params);
        inLet.setHandler(handler);
        inLet.setHandlerRequestNumber(3021);
        inLet.start();
    }
    // 서버에 진료예약 취소 요청 후처리
    public void mobile3021(Object obj) {
        String data = obj.toString();

        try {
            JSONObject jsonObject = new JSONObject(data);
            String result = jsonObject.getString("result");

            if (result.equals("ok")) {
                // 취소 성공했으면
                CRHItemViewHolder crhItemViewHolder = cancelWantCrhItem.getViewHolder();

                // 글자 취소처리됨 으로 문자열 바꾸고 색 회색으로 바꾸기
                crhItemViewHolder.current_status.setText("취소됨");
                crhItemViewHolder.current_status.setTextColor(new UseFul(activity).getRcolor(R.color.clinic_reservation_list_current_status_text_off_color));
                // 취소 버튼 없애기
                crhItemViewHolder.cancel_button.setVisibility(View.GONE);
            } else {
                // 취소 실패했으면
                callSUHDialogType1Dialog("안내", "취소 과정에 오류가 발생하였습니다. 다시 시도해 주세요. \n\n(같은 오류 메세지가 계속 보이면 아래 메일로 문의를 보내주시면 답변 드리겠습니다.)\njwisedom@naver.com", false);
            }

            progressLayout.setVisibility(View.GONE); // 로딩 레이아웃 숨기기
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**************************************************************/
}
