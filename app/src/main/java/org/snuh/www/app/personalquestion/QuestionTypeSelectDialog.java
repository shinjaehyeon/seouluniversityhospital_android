package org.snuh.www.app.personalquestion;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.snuh.www.app.common.UseFul;

import java.util.ArrayList;

public class QuestionTypeSelectDialog extends Dialog {
    Context context;
    UseFul useFul;

    /**************************************************************/

    TextView title;
    RelativeLayout closeButton;

    int buttonIdResources[] = {R.id.button_index_0,R.id.button_index_1,R.id.button_index_2,R.id.button_index_3,R.id.button_index_4,R.id.button_index_5,R.id.button_index_6};
    public ArrayList<Button> buttons = new ArrayList<Button>();

    Button previousSelectedButton;

    /**************************************************************/

    int questionType[] = {800601,800602,800603,800604,800605,800606,800607};
    String questionTypeString[] = {"병원위치","진료예약","약 처방/복용","진료과","의료진","운영시간","기타"};
    public int selected_index = -1;

    int first_period_index = -1;

    /**************************************************************/


    public QuestionTypeSelectDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀 바 삭제
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.question_type_select_dialog);

        setConnectViewId(); // xml 레이아웃에 있는 객체와 코드 객체 연결하기
        setObjectEvent(); // 객체에 이벤트 설정하기

        if (first_period_index != -1) {
            buttons.get(first_period_index).setBackgroundResource(R.drawable.ripple_button_type_2);
            buttons.get(first_period_index).setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_on_color));
        }
    }


    // xml 레이아웃에 있는 객체와 코드 객체 연결하기
    public void setConnectViewId() {
        useFul = new UseFul(context);


        title = (TextView) findViewById(R.id.title);
        closeButton = (RelativeLayout) findViewById(R.id.closeButton);

        for (int i=0; i<buttonIdResources.length; i++) {
            buttons.add((Button) findViewById(buttonIdResources[i]));
        }
    }

    // 객체에 이벤트 설정하기
    public void setObjectEvent() {
        for (int i=0; i<buttons.size(); i++) {
            buttons.get(i).setTag(i);
            buttons.get(i).setOnClickListener(itemOnClickListener);
        }
    }

    /**************************************************************/

    public void setTitle(String s) {
        title.setText(s);
    }

    public void setCloseButtonOnClickListener(View.OnClickListener e) {
        closeButton.setOnClickListener(e);
    }

    public void setFirstValue(int n) {
        this.first_period_index = n;
    }

    /**************************************************************/


    /**************************************************************/

    public boolean isOnItemClicked = false;

    // 진료과 리스트중 하나를 클릭 했을 시 발생하는 이벤트 설정
    View.OnClickListener itemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isOnItemClicked = true;

            int index = (int) v.getTag();
            selected_index = index;

            ((Button) v).setBackgroundResource(R.drawable.ripple_button_type_2);
            ((Button) v).setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_on_color));

            if (previousSelectedButton != null) {
                if ((int) previousSelectedButton.getTag() != (int) ((Button) v).getTag()) {
                    previousSelectedButton.setBackgroundResource(R.drawable.ripple_button_type_6);
                    previousSelectedButton.setTextColor(useFul.getRcolor(R.color.mobile4700_align_dialog_list_text_color));
                }
            }

            previousSelectedButton = (Button) v;

            dismiss();
        }
    };


}
