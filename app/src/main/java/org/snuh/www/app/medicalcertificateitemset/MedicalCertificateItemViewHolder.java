package org.snuh.www.app.medicalcertificateitemset;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.snuh.www.app.R;
import org.w3c.dom.Text;

public class MedicalCertificateItemViewHolder extends RecyclerView.ViewHolder {
    View view;

    public Button button;
    public TextView passed_day_textview;
    public TextView date_textview;
    public TextView time_textview;
    public TextView department_textview;
    public TextView doctor_name_textview;


    /**************************************************************/

    public MedicalCertificateItemViewHolder(View view) {
        super(view);
        this.view = view;

        button = (Button) view.findViewById(R.id.button);
        passed_day_textview = (TextView) view.findViewById(R.id.passed_day_textview);
        date_textview = (TextView) view.findViewById(R.id.date_textview);
        time_textview = (TextView) view.findViewById(R.id.time_textview);
        department_textview = (TextView) view.findViewById(R.id.department_textview);
        doctor_name_textview = (TextView) view.findViewById(R.id.doctor_name_textview);

    }
}
